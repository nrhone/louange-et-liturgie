import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditeursPage } from './editeurs';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    EditeursPage,
  ],
  imports: [
    IonicPageModule.forChild(EditeursPage),
    PipesModule
  ],
})
export class EditeursPageModule {}
