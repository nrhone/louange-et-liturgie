import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { first } from 'rxjs/operators';

/*
  Generated class for the MessagesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessagesProvider {

	db:any;db2:any;
	unread:number=0;
	public getUnread = new Subject<number>();
	constructor(public storage: Storage, private afs: AngularFirestore) { 
		this.db = this.afs.collection("messages");
		this.db2 = this.afs.collection("home_messages");
	}
  	async getMessages(){
		let messages = await this.storage.get('sb_messages');
		if(messages) return messages;
		else return {lastUpdateTime:0, messages:[]}
  	}
  	async getMessage(id:number){
		let messages = await this.getMessages();
		return messages.messages[id];
	}
	async getLastMessages(){
		let messages = await this.getMessages();
		let date = messages.lastUpdateTime ? messages.lastUpdateTime:0;
		this.afs.collection('messages', ref=>ref.orderBy('date').where('date',">=",date)).valueChanges().pipe(first()).toPromise().then(res=>{
			res.forEach(message =>{
				let newMessage = message;
				newMessage['read']=false;
				messages.messages.push(newMessage);
			})
			messages.lastUpdateTime = Date.now();
			this.storage.set('sb_messages', messages);
			let unreadNb=0;
			messages.messages.forEach(message=>{
				if(!message.read) unreadNb++;
			});
			this.getUnread.next(unreadNb);
		})
	}
	async changeReadStatus(id:number){
		let messages = await this.storage.get('sb_messages');
		messages.messages[id].read=true;
		let unreadNb=0;
		messages.messages.forEach(message=>{
			if(!message.read) unreadNb++;
		});
		this.getUnread.next(unreadNb);
		return this.storage.set('sb_messages', messages);
	}
	async deleteMessage(id:number){
		let messages = await this.storage.get('sb_messages');
		messages.messages.splice(id,1);
		return this.storage.set('sb_messages', messages);
	}

	getHomeMessage(){
		//let messages = await this.getMessages();
		let date = Date.now();
		return this.afs.collection('home_messages', ref=>ref.orderBy('endDate','desc').where('endDate',">=",date).limit(1)).valueChanges();
	}

}
