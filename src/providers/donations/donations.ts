import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from 'angularfire2/firestore';

/*
  Generated class for the ProvidersDonationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DonationsProvider {

  constructor(public storage: Storage, private afs: AngularFirestore) {
  }
  async getDonations(){
	  return this.storage.get("sb_donations");
  }
  async addDonation(donation:any){
	let donations = await this.getDonations();
	if(!donations) donations=[];
	donations.push(donation);
	this.afs.collection("donations").add(donation);
	return this.storage.set("sb_donations", donations);
  }

}
