import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private iab: InAppBrowser,
	private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider) {
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("About");
  }
  openUrl(url:string){
    this.iab.create(url, "_system", "location=true");
  }
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
}
