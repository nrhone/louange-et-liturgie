import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the EditeursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editeurs',
  templateUrl: 'editeurs.html',
})
export class EditeursPage {
  public editeurs:any[]=[];
  public carnet:string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private songsProvider:SongsProvider,
	private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
	) {
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Editeurs");
    this.carnet = this.songsProvider.getCarnet();
    this.songsProvider.getCopyrightsInSongbook().then(res=>this.editeurs=res)
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
  
  

}
