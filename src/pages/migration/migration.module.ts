import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MigrationPage } from './migration';

@NgModule({
  declarations: [
    MigrationPage,
  ],
  imports: [
    IonicPageModule.forChild(MigrationPage),
  ],
})
export class MigrationPageModule {}
