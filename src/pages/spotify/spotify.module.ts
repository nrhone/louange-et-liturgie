import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpotifyPage } from './spotify';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SpotifyPage,
  ],
  imports: [
    IonicPageModule.forChild(SpotifyPage),
    PipesModule
  ],
})
export class SpotifyPageModule {}
