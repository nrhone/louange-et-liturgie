import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , Gesture} from 'ionic-angular';

/**
 * Generated class for the ImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-image',
  templateUrl: 'image.html',
})
export class ImagePage {
  @ViewChild('pinchElement') element;
  scale:number=1;minScale:number=1;
  private gesture: Gesture;
  images:any;
  verseOrder:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //for(var key in this.navParams.data.images) console.log(key);
    this.verseOrder=[];this.images={};
    this.images = this.navParams.data.images;
    for(let id of this.navParams.data.order) {
      if(id.substring(0,3)=='IMG' && this.images[id]) this.verseOrder.push(id);
    }
    //this.verseOrder=this.navParams.data.order;
  }

  
  ionViewDidLoad() {

    // create a new Gesture instance hooked to the DOM element
    this.gesture = new Gesture(this.element.nativeElement);

    // start listening for ...
    this.gesture.listen();

    // ... the pinchstart event
/*    this.gesture.on('pinchstart', (e) => {
    });*/

    // ... for the pinch event
    this.gesture.on('pinch', (e) => {
      if(e.scale>=this.minScale) this.scale=e.scale;
    });

    // ... for the pinchend event
    /*this.gesture.on('pinchend', (e) => {
    });*/

  }

  ngOnDestroy() {
    
    // stop listening
    if(this.gesture) this.gesture.destroy();
  }

}
