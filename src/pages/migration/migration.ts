import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';

/**
 * Generated class for the MigrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-migration',
  templateUrl: 'migration.html',
})
export class MigrationPage {
  public db: any;
  private songsCollection: AngularFirestoreCollection<any>;
  private cpCollection: AngularFirestoreCollection<any>;
  private rubriqueCollection: AngularFirestoreCollection<any>;
  private carnetsCollection: AngularFirestoreCollection<any>;
  private themesCollection: AngularFirestoreCollection<any>;
  private usersCollection: any;
  private usersInfoCollection: any;
  private songsCol:any;
  private songbooksCol:any;
  items: Observable<any[]>;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    //fireDB: AngularFireDatabase,
    private afs: AngularFirestore
  ) {
    this.db = firebase.database().ref('/');
    this.songsCollection = afs.collection('songs');
    this.cpCollection = afs.collection("copyrights");
    this.rubriqueCollection = afs.collection("rubriques");
    this.carnetsCollection = afs.collection("songbooks");
    this.themesCollection = afs.collection("themes");
    this.usersCollection = firebase.firestore().collection('users');
    this.usersInfoCollection = firebase.firestore().collection('usersInfo');
    //this.songsCollection.valueChanges().subscribe(res=>console.log(res));
    this.songsCol = firebase.firestore().collection('songs');
    this.songbooksCol = firebase.firestore().collection('songbooks');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MigrationPage');
  }
  changeStructure(){
    let counter=0;console.log("beginning");
    //let storage=firebase.storage();
    this.songsCol.get().then(
      querySnapshot => {
        querySnapshot.forEach(doc =>{
          let meta = doc.data();
          let key = doc.id;
          let doUpdate = false;
		  if(meta.ref && !Array.isArray(meta.ref)) {
			meta.ref = [meta.ref];
            doUpdate=true;
            //console.log(sbInfo);
          }
          if(doUpdate){
            var updateTime = new Date();    
            meta.lastUpdateTime = updateTime.getTime(); 
            counter++;
            if(counter%20==0) console.log(counter+' : ' +meta.title);
            console.log(meta);
            this.songsCol.doc(key).set(meta).then(console.log(counter));
            //if(counter==952) subscription.unsubscribe();
            
          }
          
          
          
        })
      }
    )
  }
  majChant(){
    this.afs.collection('songs').doc('605').valueChanges().subscribe(song=>{
      console.log(song);
      if(song["text_parts"]["LN"]) {
        song["text_parts"]["LN"] = song["text_parts"]["LI"];
        delete(song["text_parts"]["LI"]);
        song["songbooks"]["2017_FR"]["LN"]=song["songbooks"]["2017_FR"]["LI"];
        delete(song["songbooks"]["2017_FR"]["LI"]);
        //song["songbooks"]["2018_HU"]["LN"]=song["songbooks"]["2018_HU"]["LI"];
        //delete(song["songbooks"]["2018_HU"]["LI"]);
        this.afs.collection('songs').doc('605').set(song);
      }
    })

  }
  async migrate(){
	//GET SONGBOOKS
	let songbooks={};
	let snap = await this.songbooksCol.get();
	snap.forEach(doc=>{songbooks[doc.id] = doc.data()})
	for(let book in songbooks){
		songbooks[book].songs={};
		let snap = await this.songbooksCol.doc(book).collection("songs").get();
		snap.forEach(doc=>{
			songbooks[book].songs[doc.id] = doc.data();
		})
	}
	console.log(songbooks);
	let songsSnap = await this.songsCol.get();
	let index = 0;
	songsSnap.forEach(doc=>{
		let id = doc.id; let isInSb=[];
		for(let book in songbooks){
			if(songbooks[book].songs[doc.id]) isInSb.push(book);
		}
		
		this.songsCol.doc(id).set({isInSb:isInSb},{merge:true}).then(()=>{
			index++; if(index%50==0) console.log(index, doc.data().title, isInSb)
		});
	})



   
    
  }

  async createSongbook(){
	//let storage=firebase.storage();
	var carnetACopier = '2018_TEST';
	var nouveauCarnet = 'TAIZE_FR';
	let date = (new Date()).getTime();
	let i = 0;
	let carnet1 = (await this.songbooksCol.doc(carnetACopier).get()).data();
	this.songbooksCol.doc(nouveauCarnet).set(carnet1);
    let querySnapshot = await this.songbooksCol.doc(carnetACopier).collection("songs").get();
	querySnapshot.forEach(async song =>{
		i++;
		if(i%10==0) console.log(i);
		let data = song.data();
		data.lastUpdateTime = date;
		this.songbooksCol.doc(nouveauCarnet).collection("songs").doc(song.id).set(data);
		let songData = (await this.songsCol.doc(song.id).get()).data();
		if(songData.isInSb.indexOf(nouveauCarnet) ==-1) {
			songData.isInSb.push(nouveauCarnet);
			songData.lastUpdateTime = (new Date()).getTime();    
			this.songsCol.doc(song.id).set(songData);
		}
	})
        
    
  }
  createUsersColl(){
    this.usersCollection.get().then(
      querySnapshot => {
        querySnapshot.forEach(doc =>{
		  let user = doc.data();
		  let id = doc.id;
          if(!user.userId) {
			  if(user.username) console.log(user.username);
			  else console.log(user.name);
			  firebase.firestore().collection('users').doc(id).update({userId:id})
		  }
        })
      })
    
  }

  async publishBook(){
	var carnet = "2018_HU";
	let index = 0;
	let snap = await this.songbooksCol.doc(carnet).collection("songs").get();
	snap.forEach(doc=>{
		var id = doc.id;
		this.songsCol.doc(id).update({"songbooks.2018_HU":doc.data()}).then(()=>{
			index++;
			if(index%50==0) console.log(index,doc.data().title);
		})
	})

  }
  async checkBook(){
	var carnet = "2018_TEST";
	let index = 0;
	let snap = await this.songbooksCol.doc(carnet).collection("songs").get();
	snap.forEach(doc=>{
		var id = doc.id;
		this.songsCol.doc(id).get().then(snap=>{
			if(snap.exists) {}
			else {
				console.log(id,doc.data().title);
				this.songbooksCol.doc(carnet).collection("songs").doc(id).delete()
			}
		})
		
	})

  }
  
  
}
