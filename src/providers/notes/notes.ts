import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

/*
  Generated class for the NotesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotesProvider {
  constructor(public storage: Storage) {
  }
  getNotes(){
    return this.storage.get('notes');
  }
  getNoteById(id:string){
    return this.getNotes().then(val=>{
	  if(val && val[id] && typeof(val[id])==='string') {
		  val[id] = {notes:val[id], duration:180};
		  return this.storage.set('notes',val).then(res=>{return val[id]});
		}
	  else if(val && val[id] && typeof(val[id]==='object')){
		  return val[id];
	  }
      else if(val) {
		val[id]={notes:"", duration:180};
		return this.storage.set('notes',val).then(res=>{return val[id]});
	  }
	  else{
		  val = {[id] : {notes:"",duration:180}};
		  return this.storage.set('notes',val).then(res=>{return val[id]});
		};
    })
  }
  updateNoteById(id:string, note){
    return this.getNotes().then(val=>{
      if(val && val[id]) {
		val[id].notes = note;
        this.storage.set('notes',val);
	  }
	  else if (val){
		val[id] = {notes:note, duration:180};
        this.storage.set('notes',val);
	  } 
      else this.storage.set('notes', {[id]:{notes:note, duration:180}});
    })
  }
  updateDurationById(id:string, duration:number){
    return this.getNotes().then(val=>{
      if(val && val[id]) {
		val[id].duration = duration;
        this.storage.set('notes',val);
	  }
	  else if (val && !val[id]){
		val[id] = {notes:"", duration:duration};
        this.storage.set('notes',val);
	  } 
      else this.storage.set('notes', {[id]:{notes:"", duration:duration}});
    })
  }
  updateCapoById(id:string, capo:number){
	return this.getNotes().then(val=>{
		if(val && val[id]) {
			val[id].capo = capo;
			this.storage.set('notes',val);
		}
		else if (val && !val[id]){
			val[id] = {notes:"", capo:capo, duration:180};
			this.storage.set('notes',val);
		} 
		else this.storage.set('notes', {[id]:{notes:"", duration:180, capo:capo}});
	})
	}
	updateTranspositionById(id:string, transposition:number){
		return this.getNotes().then(val=>{
			if(val && val[id]) {
				val[id].transposition = transposition;
				this.storage.set('notes',val);
			}
			else if (val && !val[id]){
				val[id] = {notes:"", transposition:transposition, duration:180};
				this.storage.set('notes',val);
			} 
			else this.storage.set('notes', {[id]:{notes:"", duration:180, transposition:transposition}});
		})
	}

}
