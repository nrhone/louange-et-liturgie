import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , PopoverController , ToastController, ActionSheetController, AlertController } from 'ionic-angular';
import { FavorsProvider } from '../../providers/favors/favors';
import firebase from 'firebase';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { AngularFirestore } from 'angularfire2/firestore';
import { TranslateProvider } from '../../providers/translate/translate'
import { ParamsProvider } from '../../providers/params/params'

/**
 * Generated class for the PlaylistsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playlists',
  templateUrl: 'playlists.html',
})
export class PlaylistsPage {
  playlists:any[];
  sharedPlaylists:any[];
  newPlaylist = {title:''};
  public fireDb: any;
  user:any;
  expirationTime:number;
  isConnected:boolean=false;
  params:any;
  type:string="list";//select the view : lists or folders
  folders:any[]=[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public favorsProvider:FavorsProvider,
    public popoverCtrl: PopoverController,
    private auth: AuthServiceProvider,
    private afs: AngularFirestore,
    public toastCtrl: ToastController,
	private translateProvider:TranslateProvider,
	private paramsProvider:ParamsProvider,
	private actionSheetCtrl:ActionSheetController,
	private alertCtrl:AlertController
	) 
  {
    this.expirationTime = 2 * 24 * 3600 * 1000;
    this.fireDb = firebase.firestore();
    
    this.sharedPlaylists = [];
    this.auth.getUserInfo().then(res=>{
      this.user=res;
    });
    firebase.database().ref('.info/connected').on('value', snapshot => {
      // If we're not currently connected, don't do anything.
      if (snapshot.val() == false) {
        this.isConnected=false;
      }
      else {
        this.isConnected=true;
      }
    });
	this.paramsProvider.getParams().then(res=>this.params=res)
}
  ionViewDidEnter(){
    let time = Date.now();
    let playlistsTemp = []; 
    this.favorsProvider.getLocalPlaylists().then(result => {
      if(result) result.forEach((playlist,index) =>{
        if(playlist.isShared) {
          //playlist locale partagée
          if( (playlist.sharedTime + this.expirationTime) < time){
            //si la playlist a été partagée il y a plus d'1 jour -> revenir en local
            //get updated songs
            this.fireDb.collection("playlists").doc(playlist.sharedKey).get().then(snap=>{
              if(snap.exists) {
                playlist.songs = snap.data().songs;
                //remove details from Firebase
                this.fireDb.collection("playlists").doc(playlist.sharedKey).delete();
                playlist.isShared=false;
                delete(playlist.sharedKey);
                delete(playlist.sharedTime);
                delete(playlist.users);
                delete(playlist.shareCode);
                this.favorsProvider.updatePlaylist(index, playlist);
              }
              else {
                playlist.isShared=false;
                delete(playlist.sharedKey);
                delete(playlist.sharedTime);
                delete(playlist.users);
				delete(playlist.shareCode);
				this.favorsProvider.updatePlaylist(index, playlist);
              }
            })            
          }
        }
        playlistsTemp.push(playlist);
      })
      this.playlists = playlistsTemp;
    });
    this.getSharedPlaylists();
    this.favorsProvider.getFolders().then(res=>{
		this.folders=res;
	});
  }
  async getSharedPlaylists(){
    let time = Date.now();
    let sharedPlaylistsTemp =[];
    this.favorsProvider.getSharedPlaylists().then(async result=>{
		if(this.user && this.user.userId){}
		else this.user = await this.auth.getUserInfo();
		if(result) result.forEach((playlist,index) =>{
			//playlist partagée
			if( (playlist.sharedTime + this.expirationTime) < time){
				//remove playlist from users' playlists list
				this.fireDb.collection("users").doc(this.user.userId).collection("playlists").doc(playlist.id).delete();
			}
			else if(playlist.createUserId!=this.user.userId){
				sharedPlaylistsTemp.push(playlist) ;
			}
			else {
				this.favorsProvider.isLocalPlaylist(playlist.id).then(res=>{
					if(!res) sharedPlaylistsTemp.push(playlist);//playlist is shared by same user from an other device
				})
			}
      })
	  this.sharedPlaylists = sharedPlaylistsTemp;
	  return sharedPlaylistsTemp;
    })
  }
  doRefresh(refresher) {
    this.getSharedPlaylists().then(res=>{
		refresher.complete();
	})
  }
  presentPopoverPlaylist(myEvent) {
    let popover = this.popoverCtrl.create("NewPlaylistPage",{},{cssClass:this.params.darkMode?'dark':''});
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((data)=>{
      if(data) {
        this.playlists=data;
        
      }
    })
  }
  presentPopoverFolder(myEvent) {
    let popover = this.popoverCtrl.create("NewFolderPage",{},{cssClass:this.params.darkMode?'dark':''});
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((data)=>{
      if(data) {
        this.folders=data;
        
      }
    })
  }
  
  deletePlaylist(index){
    this.favorsProvider.deletePlaylistById(index).then(result=>{
      this.playlists=result;
      let toast = this.toastCtrl.create({
        message: 'La playlist a été supprimée',
        duration: 3000
      });
      toast.present();
    });
  }
  copyPlaylist(playlist){
    this.favorsProvider.createPlaylist(playlist.title+" - copie",playlist.songs,0).then(
      result=>{
        this.playlists = result;
      }
    ); 
  }
  async copySharedPlaylist(playlistSummary){
	  //get playlist TODO add loader
	  this.fireDb.collection("playlists").doc(playlistSummary.id).get().then(snap=>{
			let playlist = snap.data();
			let songs=[];
			if(playlist.songs) {
				playlist.songs.forEach(song=>{
					songs.push({title:song.title, orgKey:song.orgKey, i18n:song.i18n?song.i18n:{}, songbooks:song.songbooks?song.songbooks:{}});
				})
			}
			this.favorsProvider.createPlaylist(playlist.title, songs, 0).then(res=>this.playlists=res);
	  });
  }
  archive(playlist, index){
    this.favorsProvider.archivePlaylist(playlist).then(result=>{
      this.favorsProvider.deletePlaylistById(index).then(result=>{
        this.playlists=result;
        let toast = this.toastCtrl.create({
          message: 'La playlist a été archivée',
          duration: 3000
        });
        toast.present();
      });
    })
  }
  goToPlaylist(event, playlistId, playlist, canEdit) {
    if(playlist.sharedTime) {
      this.navCtrl.push("PlaylistPage", {
        id: playlist.sharedKey?playlist.sharedKey:playlist.id,
        isShared:true,
        canEdit:canEdit
      });
    }
    else{
      this.navCtrl.push("PlaylistPage", {
        id: playlistId,
        isShared:false,
        canEdit:true
      });
    }
    
  }
  
	
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
	addPlaylist(ev){
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Créer',
			buttons: [
				{
					text: 'Nouveau dossier',
					icon:'folder-open',
					handler: () => {
						this.presentPopoverFolder(ev);
					}
				},
				{
					text: 'Playlist personnelle',
					icon:'person',
					handler: () => {
						this.presentPopoverPlaylist(ev);
					}
				},
				{
					text: 'Playlist partagée par code',
					icon:'code-working',
					handler: () => {
						this.addSharedPlaylist();
					}
				},
				{
					text: 'Annuler',
					role: 'cancel',
					handler: () => {
					}
				  }
			]
		});
		actionSheet.present();
	}

	addSharedPlaylist(){
		let alert = this.alertCtrl.create({
			title:"Ajouter une playlist partagée",
			message:"Entrer le code de partage",
			inputs: [
				{
				  name: 'code',
				  placeholder: 'Code'
				}
			],
			buttons: [
				{
				  text: 'Annuler',
				  role: 'cancel',
				  handler: data => {
				  }
				},
				{
				  text: 'Télécharger',
				  handler: data => {
					if (data.code) {
					  // logged in!
					  this.getSharedPlaylist(data.code);
					} else {
					  // invalid login
					  return false;
					}
				  }
				}
			  ]
		})
		alert.present();
	}
	async getSharedPlaylist(code:string){
		let user = await this.auth.getUserInfo();
		if(user && user != null) {
			this.afs.collection("playlists", ref=>ref.where('shareCode','==',code)).valueChanges().subscribe(playlists=>{
				if(playlists && playlists.length>0){
					let playlist:any = playlists[0];
					var userSummary =  {
						"title":playlist.title,
						"createdTime":playlist.createdTime,
						"createUser":playlist.createUser ? playlist.createUser : "",
						"createUserId":playlist.createUserId ? playlist.createUserId : "",
						"sharedTime":playlist.sharedTime,
						"id":playlist.sharedKey,
					};
					this.fireDb.collection("users").doc(user.userId).collection("playlists").doc(playlist.sharedKey).set(userSummary);
					this.getSharedPlaylists();
				}
			});


		}

	}
  
	//**FOLDERS */
	goToFolder(id){
		this.navCtrl.push("FolderPage", {
			id: id
		});
	}
	selectFolder(playlist:any, index:number){
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Créer',
			buttons:[]
		});
		this.folders.forEach((folder,i)=>{
			actionSheet.addButton({
				text: folder.title,
				//icon:'code-working',
				handler: () => {
					this.favorsProvider.addPlaylistToFolder(i,playlist,index).then(res=>{
						this.folders=res;
						let toast = this.toastCtrl.create({
							message: 'Playlist ajoutée au dossier',
							duration: 3000
						  });
						  toast.present();
					});
				}
			})
		})
		actionSheet.addButton(
			{
				text: 'Annuler',
				role: 'cancel',
				handler: () => {
				}
			}
		);
		return actionSheet.present();
	  }
}
