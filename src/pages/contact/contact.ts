import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AppVersion } from '@ionic-native/app-version';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  isAvailable:boolean=false;
  email:any;
  versionNumber:any;
  constructor(
	  public navCtrl: NavController, 
	  public navParams: NavParams,
	  private emailComposer: EmailComposer, 
	  public toastCtrl: ToastController, 
	  private appVersion: AppVersion,
	  private translateProvider:TranslateProvider
	  ) {
    this.emailComposer.isAvailable().then((available: boolean) =>{
      if(available) {
        this.isAvailable=true;
        this.emailComposer.hasPermission().then(isPermitted=>{
          if(!isPermitted) this.emailComposer.requestPermission();
        })

      }
     });
     this.email={};
     this.appVersion.getVersionNumber().then(version => this.versionNumber = version);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
  send(){
    let email = {
      to: 'appli.louange@gmail.com',
      //body: this.email.body,
      subject: (this.email.type).toUpperCase()+" - "+this.versionNumber,
      isHtml: false
    };
    this.emailComposer.open(email).then(res=>{
      this.email.type="";
      let toast = this.toastCtrl.create({
        message: 'Le message a bien été envoyé.',
        duration: 3000
      });
      toast.present();
    }, error =>{
      let toast = this.toastCtrl.create({
        message: 'Erreur',
        duration: 3000
      });
      toast.present();
    });   
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}

}
