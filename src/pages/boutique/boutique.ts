import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController, ToastController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ProductsProvider } from '../../providers/products/products';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the BoutiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-boutique',
  templateUrl: 'boutique.html',
})
export class BoutiquePage {
  availableProducts:any;
  shopProducts:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public auth:AuthServiceProvider,
    private productsProvider: ProductsProvider,
    private iab: InAppBrowser,
    private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
  ) {
    this.productsProvider.getProducts().then(res=> this.shopProducts=res);
    console.log("page constr");
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Boutique");
   
  }
  /*buy(productId){
    this.iap.buy(productId).then((data)=> {
      this.auth.updateRights({"open":1}).then(()=>{
        this.navCtrl.setRoot('HomePage',{initialisation:true});
      });
      
    })
    .catch((err)=> {
      console.log(err);
      let toast = this.toastCtrl.create({
        message: err.errorMessage,
        duration: 3000
      });
      toast.present();
    });
  }
  restore(){
    this.iap.restorePurchases().then((products)=>{
      let toast = this.toastCtrl.create({
        message: 'Achats restaurés',
        duration: 3000
      });
      toast.present();

    })
  }*/
  openUrl(product:any){
    this.analyticsProvider.clickEvent("Produit boutique", product.text)
    let url = 'https://www.laboutique-chemin-neuf.com/'+product.detailPageUrl;
    this.iab.create(url, "_system", "location=true");
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}

}
