import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalSongsPage } from './local-songs';

@NgModule({
  declarations: [
    LocalSongsPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalSongsPage),
  ],
})
export class LocalSongsPageModule {}