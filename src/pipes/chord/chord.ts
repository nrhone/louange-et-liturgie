import { Pipe, PipeTransform } from '@angular/core';
/**
 * Generated class for the ChordPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'chord',
  pure: false
})
export class ChordPipe implements PipeTransform {
  /**
   * Takes a song line and transforms to chords
   * position : in the text or on the right
   * showCord : true to display the chords
   * capo : if chords are written with a capo
   * transposition : if chords are transposed 
   * => ex : chant écrit avec les accords en capo 3 -> capo=3 && transposition=-3
   * => si chant écrit avec les accords normaux mais à jouer capo 3 -> capo=3 && transposition=0
   * capoMode : for pianists, always display without capo
   */
  public format : string = "INT";
	chords = {
	  "FR":{
		"A":["La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib"],
		"A#":["La#","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si"],
		"Bb":["Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si"],
		"B":["Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do"],
		"C":["Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#"],
		"C#":["Do#","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré"],
		"Db":["Réb","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib","Ré"],
		"D":["Ré","Do#","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi","Mib"],
		"D#":["Ré#", "Ré","Do#","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi"],
		"Eb":["Mib","Ré","Do#","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa","Mi"],
		"E":["Mi","Mib","Ré","Do#","Do","Si","Sib","La","Sol#","Sol","Fa#","Fa"],
		"F":["Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La","Sol#","Sol","Fa#"],
		"F#":["Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La","Sol#","Sol"],
		"Gb":["Solb","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La","Sol#","Sol"],
		"G":["Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La","Sol#"],
		"G#":["Sol#","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La"],
		"Ab":["Lab","Sol","Fa#","Fa","Mi","Mib","Ré","Do#","Do","Si","Sib","La"],
		"X":["","","","","","","","","","","",""],
		"Z":["","","","","","","","","","","",""]  
	  },
	  "INT":{
		"A":["A","G#","G","F#","F","E","Eb","D","C#","C","B","Bb"],
		"A#":["A#","A","G#","G","F#","F","E","Eb","D","C#","C","B"],
		"Bb":["Bb","A","G#","G","F#","F","E","Eb","D","C#","C","B"],
		"B":["B","Bb","A","G#","G","F#","F","E","Eb","D","C#","C"],
		"C":["C","B","Bb","A","G#","G","F#","F","E","Eb","D","C#"],
		"C#":["C#","C","B","Bb","A","G#","G","F#","F","E","Eb","D"],
		"Db":["Db","C","B","Bb","A","G#","G","F#","F","E","Eb","D"],
		"D":["D","C#","C","B","Bb","A","G#","G","F#","F","E","Eb"],
		"D#":["D#", "D","C#","C","B","Bb","A","G#","G","F#","F","E"],
		"Eb":["Eb","D","C#","C","B","Bb","A","G#","G","F#","F","E"],
		"E":["E","Eb","D","C#","C","B","Bb","A","G#","G","F#","F"],
		"F":["F","E","Eb","D","C#","C","B","Bb","A","G#","G","F#"],
		"F#":["F#","F","E","Eb","D","C#","C","B","Bb","A","G#","G"],
		"Gb":["Gb","F","E","Eb","D","C#","C","B","Bb","A","G#","G"],
		"G":["G","F#","F","E","Eb","D","C#","C","B","Bb","A","G#"],
		"G#":["G#","G","F#","F","E","Eb","D","C#","C","B","Bb","A"],
		"Ab":["Ab","G","F#","F","E","Eb","D","C#","C","B","Bb","A"],
		"X":["","","","","","","","","","","",""],
		"Z":["","","","","","","","","","","",""]
	  },
	"DE":{
		"A":["A","G#","G","F#","F","E","Eb","D","C#","C","H","A#"],
		"A#":["A#","A","G#","G","F#","F","E","Eb","D","C#","C","H"],
		"Bb":["A#","A","G#","G","F#","F","E","Eb","D","C#","C","H"],
		"B":["H","A#","A","G#","G","F#","F","E","Eb","D","C#","C"],
		"C":["C","H","A#","A","G#","G","F#","F","E","Eb","D","C#"],
		"C#":["C#","C","H","A#","A","G#","G","F#","F","E","Eb","D"],
		"Db":["Db","C","H","A#","A","G#","G","F#","F","E","Eb","D"],
		"D":["D","C#","C","H","A#","A","G#","G","F#","F","E","Eb"],
		"D#":["D#", "D","C#","C","H","A#","A","G#","G","F#","F","E"],
		"Eb":["Eb","D","C#","C","H","A#","A","G#","G","F#","F","E"],
		"E":["E","Eb","D","C#","C","H","A#","A","G#","G","F#","F"],
		"F":["F","E","Eb","D","C#","C","H","A#","A","G#","G","F#"],
		"F#":["F#","F","E","Eb","D","C#","C","H","A#","A","G#","G"],
		"Gb":["Gb","F","E","Eb","D","C#","C","H","A#","A","G#","G"],
		"G":["G","F#","F","E","Eb","D","C#","C","H","A#","A","G#"],
		"G#":["G#","G","F#","F","E","Eb","D","C#","C","H","A#","A"],
		"Ab":["Ab","G","F#","F","E","Eb","D","C#","C","H","A#","A"],
		"X":["","","","","","","","","","","",""],
		"Z":["","","","","","","","","","","",""]
	  }
	};
	

	//For chords transposed with music theory
// A A#/Bb B C C#/Db E F F#/Gb G G#/Ab 
// I II III IV V VI VII
	keys = {
		"Abm":["Ab","Bb","Cb","Db","Eb","Fb","Gb"],	
		"Cb":["Cb","Db","Eb","Fb","Gb","Ab","Bb"],	
		"Ebm":["Eb","F","Gb","Ab","Bb","Cb","Db"],	
		"Gb":["Gb","Ab","Bb","Cb","Db","Eb","F"],	
		"Bbm":["Bb","C","Db","Eb","F","Gb","Ab"],	
		"Db":["Db","Eb","F","Gb","Ab","Bb","C"],	
		"Fm":["F","G","Ab","Bb","C","Db","Eb"],	
		"Ab":["Ab","Bb","C","Db","Eb","F","G"],	
		"Cm":["C","D","Eb","F","G","Ab","Bb"],	
		"Eb":["Eb","F","G","Ab","Bb","C","D"],	
		"Gm":["G","A","Bb","C","D","Eb","F"],	
		"Bb":["Bb","C","D","Eb","F","G","A"],	
		"Dm":["D","E","F","G","A","Bb","C"],	
		"F":["F","G","A","Bb","C","D","E"],	
		"C":["C","D","E","F","G","A","B"],	
		"Am":["A","B","C","D","E","F","G"],	
		"G":["G","A","B","C","D","E","F#"],	
		"Em":["E","F#","G","A","B","C","D"],	
		"D":["D","E","F#","G","A","B","C#",],	
		"Bm":["B","C#","D","E","F#","G","A"],	
		"A":["A","B","C#","D","E","F#","G#"],	
		"F#m":["F#","G#","A","B","C#","D","E"],	
		"E":["E","F#","G#","A","B","C#","D#"],	
		"C#m":["C#","D#","E","F#","G#","A","B"],	
		"B":["B","C#","D#","E","F#","G#","A#"],	
		"G#m":["G#","A#","B","C#","D#","E","F#"],	
		"F#":["F#","G#","A#","B","C#","D#","E#"],	
		"D#m":["D#","E#","F#","G#","A#","B","C#"],	
		"C#":["C#","D#","E#","F#","G#","A#","B#"],	
		"A#m":["A#","B#","C#","D#","E#","F#","G#"],	
	}
	formats = {
		"FR": {
			"A":"La",
			"A#":"La#",
			"Bb":"Sib",
			"B":"Si",
			"B#":"Si#",
			"Cb":"Dob",
			"C":"Do",
			"C#":"Do#",
			"Db":"Réb",
			"D":"Ré",
			"D#":"Ré#",
			"Eb":"Mib",
			"E":"Mi",
			"E#":"Mi#",
			"Fb":"Fab",
			"F":"Fa",
			"F#":"Fa#",
			"Gb":"Solb",
			"G":"Sol",
			"G#":"Sol#",
			"Ab":"Lab",
		},
		"INT": {
			"A":"A",
			"A#":"A#",
			"Bb":"Bb",
			"B":"B",
			"B#":"B#",
			"Cb":"Cb",
			"C":"C",
			"C#":"C#",
			"Db":"Db",
			"D":"D",
			"D#":"D#",
			"Eb":"Eb",
			"E":"E",
			"E#":"E#",
			"Fb":"Fb",
			"F":"F",
			"F#":"F#",
			"Gb":"Gb",
			"G":"G",
			"G#":"G#",
			"Ab":"Ab",		
		},
		"DE": {
			"A":"A",
			"A#":"A#",
			"Bb":"B",
			"B":"H",
			"B#":"H#",
			"Cb":"Cb",
			"C":"C",
			"C#":"C#",
			"Db":"Db",
			"D":"D",
			"D#":"D#",
			"Eb":"Eb",
			"E":"E",
			"E#":"E#",
			"Fb":"Fb",
			"F":"F",
			"F#":"F#",
			"Gb":"Gb",
			"G":"G",
			"G#":"G#",
			"Ab":"Ab",
		}
	}
	

	transform(value: string, showChords:boolean, position: string, options:any, song:any, songCapo:number, tonality:any) {
		let capo = 0, transposition = 0;
		let format = "INT"; if(options.chordFormat) format = options.chordFormat;
		if(song && song.songbooks[options.carnet] && (song.songbooks[options.carnet].capo || song.songbooks[options.carnet].capo==0 )) capo = song.songbooks[options.carnet].capo;
		else if(songCapo || songCapo==0) capo = song.capo;
		if(song && song.songbooks[options.carnet] && song.songbooks[options.carnet].transposition) transposition = song.songbooks[options.carnet].transposition;
		else if(song && (song.transposition || song.transposition==0)) transposition = song.transposition;
		if (options.capoMode) {
			capo=0;  
		}
		if(!options.chordFormat || options.chordFormat=="undefined") format="INT";
		if(song.tonality && song.tonality !="" && tonality) return this.getLineOfChords(value, position, showChords, format, song.tonality, tonality, transposition-capo);
		else return this.getLineOfChordsOld(value,capo, transposition, position, showChords, format);
	}
	getLineOfChordsOld(value,capo, transposition, position, showChord, format){
		var chordsString = "";
		var output ="";
		var hasChords = false;
		if(showChord && position=="right") {
			output = value.replace(/\[(\(?)([a-zA-Z]{1}[b#]?|[^\]\)]+)([^\/\]\)]*)(\/?)([a-zA-Z]?[b#]?)([^\/\]\)]*)(\)?)\]/g,(match,openPar,chord,alt,isBass,bassChord,bassAlt,closePar)=>{
			//parfois accord entre ()
			if(chord) {
				if(chord=="Z"){
				chordsString+="</td><td class='border-left'>";
				return "";
				}
				if(chord && this.chords[format][chord]) {
				if(openPar) chordsString+="(";
				chordsString += "<span class='cc'>"+this.chords[format][chord][(capo-transposition+12)%12]+alt;
				if (isBass=="/") {
					if(bassChord) chordsString += "<sub>/"+this.chords[format][bassChord][(capo-transposition+12)%12]+bassAlt;
					chordsString+="</sub>";
				}
				chordsString+="</span>";
				if(closePar) chordsString+=")";
				chordsString += " ";
				return "$";
				}
				else {
				chordsString+=chord;
				return "$";
				}
			}
			else{
				chordsString+="<small>"+match.replace(/[\[\]]/g,"")+"</small>";
				return "";
			}
			
			});
			output = output.replace(/\$/g, "");
			//soulignement des lieux des accords
			//output = output.replace(/\$([^$]){0,3}/g, function(match){
			//  if(match.length==1) return "_ ";//deux accords côte à côte
			//  else {
			//    if(match.charAt(1)==" ") return "_"+match.slice(1,4);
			//    else return "<u>"+match.charAt(1)+"</u>"+match.slice(2,4);
			//  }
			//})
			return "<td>"+output+ "</td><td width='30'></td><td>" + chordsString+"</td>";
		
		}
		
		else if(showChord && position =="text") {
			output = value.replace(/\] /g,"]_ ").replace(/ /g," [X]").replace(/ \[/g," [").replace(/\[(\(?)([a-zA-Z]{1}[b#]?|[^\]\)]+)([^\/\]\)]*)(\/?)([a-zA-Z]?[b#]?)([^\/\]\)]*)(\)?)\]/g,(match,openPar,chord,alt,isBass,bassChord,bassAlt,closePar)=>{
			if(chord){
				if(this.chords[format][chord]) {
				var chordString="";
				if(chord=='X') chordString = "</li></ul></li><li><ul class='list'><li>";
				else {
					chordString = "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>";
					hasChords=true;
				}
				if(openPar) chordsString+="(";
				chordString+= this.chords[format][chord][(capo-transposition+12)%12]+alt;
				if (isBass=="/") {
					if(this.chords[format][bassChord]) chordString += "<sub>/"+this.chords[format][bassChord][(capo-transposition+12)%12]+bassAlt;
					chordString+="</sub>";
				}
				if(closePar) chordsString+=")";
				chordString +="</li><li>";
				return chordString;
				}
				else if(["1.","2.","3.","4."].indexOf(chord)>-1){
				return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord number'>"+chord+"</li><li>";
				}
				else return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>"+chord+"</li><li>";
	
			}
	
			else{
				return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>"+match+"</li><li>";
			}
			
			});
			
			if(hasChords) return "<td><ul class='line'><li class='withChords'><ul class='list'><li class='chord'></li><li>"+output+"</li></ul></td>";
			else return "<td><ul class='line'><li><ul class='list'><li></li><li>"+output+"</li></ul></td>";
		}
		
		else {
			if(position=="right") return "<td>"+value.replace(/\[([^\[\]]+)\]/g,"")+"</td><td></td>";
			else return "<span><span class='chord'><span> </span></span></span>"+value.replace(/\[([^\[\]]+)\]/g,"");
		}
	}
	//with music theory
	getLineOfChords(value, position:string, showChord:boolean, format:string, oldTonality:string, newTonality:string, diff:number){
		var chordsString = "";
		var output ="";
		var hasChords = false;
		if(showChord && position=="right") {
			output = value.replace(/\[(\(?)([a-zA-Z]{1}[b#]?|[^\]\)]+)([^\/\]\)]*)(\/?)([a-zA-Z]?[b#]?)([^\/\]\)]*)(\)?)\]/g,(match,openPar,chord,alt,isBass,bassChord,bassAlt,closePar)=>{
			//parfois accord entre ()
			if(chord) {
				if(chord=="Z"){
					chordsString+="</td><td class='border-left'>";
					return "";
				}
				if(chord && (this.formats[format][chord])) {
					if(openPar) chordsString+="(";
					chordsString +="<span class='cc'>"+ this.getChord(chord,oldTonality, newTonality, diff, format)+alt;
					if (isBass=="/") {
						if(bassChord) chordsString += "<sub>/"+this.getChord(bassChord,oldTonality, newTonality, diff, format)+bassAlt;
						chordsString+="</sub>";
					}
					chordsString+="</span>";
					if(closePar) chordsString+=")";
					chordsString += " ";
					return "$";
				}
				else {
					chordsString+=chord;
					return "$";
				}
			}
			else{
				chordsString+="<small>"+match.replace(/[\[\]]/g,"")+"</small>";
				return "";
			}
			
			});
			output = output.replace(/\$/g, "");
			return "<td>"+output+ "</td><td width='30'></td><td>" + chordsString + "</td>";
		}
		else if(showChord && position =="text") {
			output = value.replace(/\] /g,"]_ ").replace(/ /g," [X]").replace(/ \[/g," [").replace(/\[(\(?)([a-zA-Z]{1}[b#]?|[^\]\)]+)([^\/\]\)]*)(\/?)([a-zA-Z]?[b#]?)([^\/\]\)]*)(\)?)\]/g,(match,openPar,chord,alt,isBass,bassChord,bassAlt,closePar)=>{
				if(chord){
					if(this.formats[format][chord] || chord=='X') {
						var chordString="";
						if(chord=='X') {
							chordString = "</li></ul></li><li><ul class='list'><li>";
						}
						else {
							chordString = "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>";
							hasChords=true;
							if(openPar) chordsString+="(";
							chordString+= this.getChord(chord,oldTonality, newTonality, diff, format)+alt;
							if (isBass=="/") {
								if(this.chords[format][bassChord]) chordString += "<sub>/"+this.getChord(bassChord,oldTonality, newTonality, diff, format)+bassAlt;
								chordString+="</sub>";
							}
							if(closePar) chordsString+=")";
						}
						chordString +="</li><li>";
						return chordString;
					}
					else if(["1.","2.","3.","4."].indexOf(chord)>-1){
						return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord number'>"+chord+"</li><li>";
					}
					else return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>"+chord+"</li><li>";
		
				}
	
				else{
					return "</li></ul></li><li class='chord-line'><ul class='list'><li class='chord'>"+match+"</li><li>";
				}
				
			});
			
			if(hasChords) return "<td><ul class='line'><li class='withChords'><ul class='list'><li class='chord'></li><li>"+output+"</li></ul></td>";
			else return "<td><ul class='line'><li><ul class='list'><li></li><li>"+output+"</li></ul></td>";
		}
		
		else {
			if(position=="right") return "<td>"+value.replace(/\[([^\[\]]+)\]/g,"")+"</td><td></td>";
			else return "<span><span class='chord'><span> </span></span></span>"+value.replace(/\[([^\[\]]+)\]/g,"");
		}
	}
  
	getChord(chord,oldTonality,newTonality, diff:number, format:string) {
		//get chord position in key
		let chordPosition = this.keys[oldTonality].indexOf(chord);
		if(chordPosition==-1) {
			return this.chords[format][chord][(12-diff)%12];
		}
		else {
			let chordToReturn = this.keys[newTonality][chordPosition];
			if(!this.formats[format][chordToReturn]) {}
			return this.formats[format][chordToReturn];
		}
	}
}
