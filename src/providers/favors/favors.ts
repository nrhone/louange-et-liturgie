import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthServiceProvider } from '../auth-service/auth-service';
import { ParamsProvider } from '../params/params';
//import { registerLocaleData } from '@angular/common';

/*
  Generated class for the FavorsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FavorsProvider {
  public db: any;
  public fireDb: any;
  public user:any;
  public carnet:string = "APPLI_FR";
  constructor(public storage: Storage,afs: AngularFirestore,private auth: AuthServiceProvider, private paramsProvider:ParamsProvider) {
    //console.log('Hello FavorsProvider Provider');
    this.fireDb = firebase.firestore();
    this.auth.getUserInfo().then(res=>{
      if(res && res != null) {this.user=res;}
	});
	this.paramsProvider.getParams().then(params=>{
		if(params) {
		  this.carnet=params.carnet;
	  }
  });
	
  }
  
  async getSharedPlaylists(){
	let availablePlaylists=[];
	let res = await this.auth.getUserInfo();
	if(res && res.userId){
		return this.fireDb.collection("users").doc(res.userId).collection("playlists").get().then(sharedPlaylists=>{
			sharedPlaylists.forEach(sharedPlaylist => {
				availablePlaylists.push(sharedPlaylist.data());
			});
			return availablePlaylists;
		})
		.catch(err=>{
			return availablePlaylists;
		})
	}
	else return;
  }
  getLocalPlaylists(){
    return this.storage.get("playlists").then(playlists =>{
      if(playlists) return playlists;
      else return [];
    });   
  }
  getPlaylist(id){
    return this.getLocalPlaylists().then(playlists => {
      if (playlists[id] ) return playlists[id];
      else return false;
    });
  }
  isLocalPlaylist(sharedId:string){
		return this.getLocalPlaylists().then(playlists => {
			return playlists.find(function(list) {
				return list.sharedKey && list.sharedKey == sharedId;
			});
			
		})
  }
  getArchives(){
    return this.storage.get("archives").then(playlists =>{
      return playlists;
    });   
  }
  archivePlaylist(playlist){
    let date = (new Date()).getTime();
    let songs=[];
    playlist.songs.forEach(song =>{
      songs.push({title:song.title, orgKey:song.orgKey});
    })
    return this.getArchives().then(playlists => {
      if(playlists) {
        playlists.push({'title':playlist.title,'songs':songs, 'archivedTime':date, 'createdTime':playlist.createdTime});
        this.storage.set('archives',playlists);
        return playlists;
      }
      else {
        this.storage.set('archives',[{'title':playlist.title,'songs':songs, 'archivedTime':date}]);
        return [{'title':playlist.title,'songs':songs, 'archivedTime':date}];
      }
    })
  }
  restore(index){
    return this.getArchives().then(playlists => {
      if(playlists){
        playlists.splice(index,1);
        this.storage.set('archives',playlists);
        return playlists;
      }
      else return false;
    }); 
  }
  updatePlaylist(id, playlist){
    return this.getLocalPlaylists().then(playlists => {
      if (playlists[id]) {
        playlists[id] = playlist;
        this.storage.set('playlists',playlists);
        return playlists[id];
      }
      else return false;
    });
  }
	async createPlaylist(title, songs, date){
		let playlists = await this.storage.get("playlists");
		let params = await this.paramsProvider.getParams();
		if(!date) date = (new Date()).getTime();
		//store just title and key of songs --> si on copie une playlist partagée, on ne garde pas le contenu des chants
		let songArray = [];
		songs.forEach(song=>{
			songArray.push({
				title:song.title,
				orgKey:song.orgKey,
				songbooks:song.songbooks?song.songbooks:null,
				i18n:song.i18n?song.i18n:null,
				copyright:song.copyright?song.copyright:null             
			});
		});
		if(playlists) {
			playlists.push({'title':title,'songs':songArray, 'createdTime':date, carnet:params.carnet});
			this.storage.set('playlists',playlists);
			return playlists;
		}
		else {
			this.storage.set('playlists',[{'title':title,'songs':songArray,'createdTime':date, carnet:params.carnet}]);
			return [{'title':title,'songs':[],'createdTime':date, carnet:params.carnet}];
		}
	}
  deletePlaylistById(id){
    return this.storage.get("playlists").then(playlists => {
      if(playlists){
        playlists.splice(id,1);
        this.storage.set('playlists',playlists);
        return playlists;
      }
      else return false;
    });
  }
  async addSongToPlaylist(song, index, isShared:boolean) {
	  //get actual carnet
		let params = await this.paramsProvider.getParams();
		if(params) this.carnet=params.carnet;
    if(isShared) {
      return this.getSharedPlaylists().then(playlists =>{
        //playlist partagée => mettre à jour les détails de la playlist sur Firebase
        return this.fireDb.collection("playlists").doc(playlists[index].id).get().then(snap=>{
          let playlist = snap.data();
          if(playlist.songs.length<=19) {
            playlist.songs.push(song);
            this.fireDb.collection("playlists").doc(playlists[index].id).set({songs:playlist.songs},{ merge: true })
            return {success:true, message:"OK"};
          }
          else return {success:false, message:"Les playlists partagées sont limitées à 20 chants"};
        })
      })
    }
    else {
      return this.getLocalPlaylists().then(playlists =>{
        if(!playlists[index].sharedTime){
          let newSong:any = {
            title:song.title,
            orgKey:song.orgKey?song.orgKey:null,
            songbooks:song.songbooks?song.songbooks:null,
            i18n:song.i18n?song.i18n:null,
            copyright:song.copyright?song.copyright:null,           
			carnet:song.carnet?song.carnet:this.carnet     
		};
		console.log(song.carnet)
		  if(song.orgKey=="external_song" && song.notes) newSong.notes = song.notes;
          playlists[index].songs.push(newSong);

          this.storage.set('playlists',playlists);
          return {success:true, message:"OK"};
        }
      })
    }
  }
	async addSongsToPlaylist(songArray, index, isShared:boolean) {
		//get actual carnet
		let params = await this.paramsProvider.getParams();
		if(params) this.carnet=params.carnet;
		if(isShared) {
			//playlist partagée => mettre à jour les détails de la playlist sur Firebase
			return this.fireDb.collection("playlists").doc(index).get().then(snap=>{
				let playlist = snap.data();
				if(!playlist.songs) playlist.songs=[];
				if(playlist.songs.length<=19) {
				playlist.songs = playlist.songs.concat(songArray);
				this.fireDb.collection("playlists").doc(index).set({songs:playlist.songs},{ merge: true })
				return {success:true, message:"OK"};
				}
				else return {success:false, message:"Les playlists partagées sont limitées à 20 chants"};
			})
		}
		else {
		return this.getLocalPlaylists().then(playlists =>{
			if(!playlists[index].sharedTime){
				//playlists[index].songs = playlists[index].songs.concat(songArray);
				let newSongs = [];
				songArray.forEach(song=>{
					newSongs.push({
					title:song.title,
					orgKey:song.orgKey,
					songbooks:song.songbooks?song.songbooks:null,
					i18n:song.i18n?song.i18n:null,
					copyright:song.copyright?song.copyright:null,           
					carnet:song.carnet?song.carnet:this.carnet     
				});
			});
			playlists[index].songs = playlists[index].songs.concat(newSongs);
			this.storage.set('playlists',playlists);
			return {success:true, message:"OK"};
			}
		})
		}
		
	}

	/*DEAL WITH FOLDERS FOR PLAYLISTS*/
	async getFolders(){
		let folders = await this.storage.get("sb_folders");
		return folders;
	}
	async getFolder(id:number) {
		let folders = await this.storage.get("sb_folders");
		if(folders && folders[id]) return folders[id];
		else return null;
	}
	async addPlaylistToFolder(id:number,playlist:any,playlistId:number) {
		let folders = await this.storage.get("sb_folders");
		if(folders && folders[id]) {
			folders[id].playlists.push({
				title:playlist.title,
				id:playlistId,
				createdTime:playlist.createdTime
			})
			return this.storage.set("sb_folders",folders);
		}
		else return null;
	}

	async createFolder(title, playlists, date) {
		let folders = await this.storage.get("sb_folders");
		if(!date) date = (new Date()).getTime();
		if(folders) {
			folders.push({'title':title,'playlists':playlists, 'createdTime':date});
			this.storage.set('sb_folders',folders);
			return folders;
		}
		else{
			this.storage.set('sb_folders',[{'title':title,'playlists':playlists,'createdTime':date}]);
			return [{'title':title,'playlists':[],'createdTime':date}];
		}
	}
	async deleteFolder(id:number){
		let folders = await this.storage.get("sb_folders");
		if(folders){
			folders.splice(id,1);
			this.storage.set('sb_folders',folders);
			return folders;
		}
		else return null;
	}
	async updateFolder(id:number, folder:any){
		let folders = await this.storage.get("sb_folders");
		if(folders && folders[id]){
			folders[id]=folder;
			this.storage.set('sb_folders',folders);
			return folders;
		}
		else return null;
	}

	
}
