import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSongPage } from './add-song';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AddSongPage,
  ],
  imports: [
	IonicPageModule.forChild(AddSongPage),
	PipesModule
  ],
})
export class AddSongPageModule {}
