import { Component } from '@angular/core';
import { IonicPage, ViewController , NavParams } from 'ionic-angular';

/**
 * Generated class for the DurationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-duration',
  templateUrl: 'duration.html',
})
export class DurationPage {
	song:any;
	constructor(public viewCtrl: ViewController, private navParams:NavParams) {
		this.song = this.navParams.get("song");

	}
  
	close() {
	  this.viewCtrl.dismiss(this.song);
	}
	getSec(duration:number){
		let sec = duration%60;
		if(sec<10) return "0"+sec;
		else return ""+sec;
	}
	getMin(duration:number){
		return Math.floor(duration/60);
	}

}
