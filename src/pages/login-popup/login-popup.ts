import { Component } from '@angular/core';
import { ViewController , IonicPage, NavController, NavParams , LoadingController , Loading, AlertController , ToastController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Facebook } from "@ionic-native/facebook";
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the LoginPopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-popup',
  templateUrl: 'login-popup.html',
})
export class LoginPopupPage {
  action:string;
  loading: Loading;
  accept_cgu:boolean=false;
  registerCredentials = { email: '', password: '',name:'' };
  process :string= 'login';
  user:any;
  passwordType:string="password";
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController,
    private toastCtrl : ToastController,
    public facebook: Facebook,
    private auth: AuthServiceProvider, 
    public viewCtrl: ViewController, 
	private translateProvider:TranslateProvider
  ) 
  {
    this.action = this.navParams.data.action;
    //this.facebook.browserInit(353325841767533, "v2.10");
  }

  ionViewDidLoad() {
  }

  public login() {
    this.showLoading();
    this.registerCredentials.email = this.registerCredentials.email.toLowerCase();
    this.auth.login(this.registerCredentials).subscribe(res => {
    },
    error => {
      this.showError(error);
    },
    ()=>{
      this.loading.dismiss();
      this.viewCtrl.dismiss('initialize');
    }
  );
  }
  public signInWithGoogle(){
    if(!this.accept_cgu && this.action=='creation'){
      this.showError(this.translate('error_cgu',"Vous devez accepter les Conditions générales d'utilisation pour pouvoir utiliser l'application"));
    }
    else {
      this.showLoading();
      this.auth.loginWithGoogle().subscribe((res) => {
      if(res == "user") {
        if (this.loading) this.loading.dismiss();
        this.viewCtrl.dismiss('initialize');
      }
    }, err => {
        console.log(err);
        this.showError(err.message);
      });
    }
    
  }
  public signInWithFacebook(){
    if(!this.accept_cgu && this.action=='creation'){
		this.showError(this.translate('error_cgu',"Vous devez accepter les Conditions générales d'utilisation pour pouvoir utiliser l'application"));
    }
    else {
      this.showLoading();
      this.auth.loginWithFacebook().subscribe((res) => {
        if(res == "user") {
          if (this.loading) this.loading.dismiss();
          this.viewCtrl.dismiss('initialize');
        }
      }, err => {
        console.log(err);
        this.showError(err.message);
      });
    }
  }

  async register() {
    if(!this.accept_cgu && this.action=='creation'){
		this.showError(this.translate('error_cgu',"Vous devez accepter les Conditions générales d'utilisation pour pouvoir utiliser l'application"));
    }
    else {
      this.registerCredentials.email = this.registerCredentials.email.toLowerCase();
      
      this.showLoading();
      let allowedName = await this.auth.checkUserName(this.registerCredentials.name);
      if(!allowedName)  this.showError(this.translate('error_name',"Ce nom est déjà utilisé"));
      else this.auth.register(this.registerCredentials).subscribe(success => {
        if (success) {
        } else {
          this.showError("Problème lors de la création du compte");
        }
      },
        error => {
          this.showError(error);
        },
      ()=>{
        if(this.loading) this.loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Votre compte a bien été enregistré',
          duration: 3000
        });
        toast.present();
        this.viewCtrl.dismiss('initialize');    
      });
    }
  }




  initializePwd(errors:any){
    console.log(this.registerCredentials.email);
    this.registerCredentials.email = this.registerCredentials.email.toLowerCase();
    if(!this.registerCredentials.email|| (errors && errors.required)){
      let toast = this.toastCtrl.create({
        message: 'Merci de renseigner votre adresse email',
        duration: 3000
      });
      toast.present();
    }
    else if(errors && errors.email){
      let toast = this.toastCtrl.create({
        message: 'L\'adresse mail n\'est pas valide',
        duration: 3000
      });
      toast.present();
    }
    else this.auth.initializePassword(this.registerCredentials.email).then(
      (response)=>{
        if(response.success) {
          let toast = this.toastCtrl.create({
            message: 'Un email vous a été envoyé',
            duration: 3000
          });
          toast.present();
        }
        else {
          let toast = this.toastCtrl.create({
            message: 'Erreur inconnue',
            duration: 3000
          });
          toast.present();
        }
      }
    );
  }
  correctEmail(){
    this.registerCredentials.email = (this.registerCredentials.email.toLowerCase()).replace(/[  ]/g,'');
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
  showError(text) {
    if(this.loading) this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Erreur',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  readCgu(){
    this.navCtrl.push("CguPage");
  }
  togglePassword(){
    console.log(this.passwordType);
    if(this.passwordType=="password") this.passwordType="text";
    else this.passwordType="password";
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
}
