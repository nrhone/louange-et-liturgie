import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController , ModalController, Slides} from 'ionic-angular';
//import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TranslateProvider } from '../../providers/translate/translate'
import { ParamsProvider } from '../../providers/params/params';
import { SongsProvider } from '../../providers/songs/songs';
import { ViewChild } from '@angular/core';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	@ViewChild(Slides) slides: Slides;
	process :string= 'login';
  user:any;
  isLoggedIn:boolean=false;editMode:boolean=false;
  params:any={lang:"FR", carnet:"APPLI_FR"};
  availableBooks = [
    {id:"APPLI_FR",label:"FRANCE"},
    {id:"2018_HU",label:"MAGYARORSZAG"}
  ];
  selectOptions:any;
  constructor(
    private nav: NavController,
    private auth: AuthServiceProvider, 
    private alertCtrl: AlertController, 
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
	private translateProvider:TranslateProvider,
	private paramsProvider:ParamsProvider, 
	private songsProvider:SongsProvider
	) 
	{
		this.auth.getUserInfo().then(res=>{
		if(res && res != null && res!='no_user') {
			this.user=res;this.process="logout";this.isLoggedIn=true;
		}
		else {this.process="login";console.log('no user');}
		});
		this.paramsProvider.getParams().then(params=>{
			if(params) {this.params=params; }
			else {
			  this.params = {showChords:true, capoMode:false, position:false, fontSize:16, veille:10, carnet:"APPLI_FR",lang:"FR",autoscroll:false};
			  this.paramsProvider.updateParams(this.params);  
			}
			//this.params.carnet=null;
		})
		this.selectOptions = {	  cssClass:this.params.darkMode?'dark':''	}



  }
  updateLang(){
	this.paramsProvider.updateParams(this.params);
	this.translateProvider.updateLang(this.params.lang);
  }
  updateSongBook(){  
	this.paramsProvider.updateParams(this.params).then(
	   params=>{
		 this.songsProvider.changeCarnet(this.params.carnet);
	   });
   }
   goToSlide(i:number) {
		this.slides.slideTo(i, 500);
	}
  openModal(action:string){
    let modal = this.modalCtrl.create("LoginPopupPage", { action: action });
    modal.present();
    modal.onDidDismiss(
      (data)=>{
        if(data=="initialize") this.nav.setRoot('HomePage',{initialisation:true});    
      }
    );
  }


   
  public noConnection(){
    let alert = this.alertCtrl.create({
      title: this.translate('login_no_account','Ne pas créer de compte'),
      message: this.translate('login_no_account_desc','Sans créer de compte, vous n\'aurez accès qu\'aux chants libres de droits.'),
      buttons: [
        {
          text: this.translate('cancel','Annuler'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate('continuer','Continuer'),
          handler: () => {
            this.auth.setUser('no_user').then(()=>{
              this.nav.setRoot('HomePage',{initialisation:true});
            });
            
          }
        }
      ]
    });
    alert.present();
    
  }

  public logOut(){
    this.auth.logout().subscribe((success) => {
        console.log(success);
        //this.nav.setRoot('HomePage',{initialisation:true});
      }, err => {
        console.log(err);
        this.showPopup("Erreur", err.message);
      },
    ()=>{
      let toast = this.toastCtrl.create({
        message: 'Vous avez été déconnecté',
        duration: 3000
      });
      toast.present();
      this.process='login';this.isLoggedIn=false;
	  this.user=null;
    });
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }
  
  public edit(){
	this.editMode = !this.editMode;
}
public save(){
	this.auth.editUserName(this.user.username).then( res=>{
	   let toast = this.toastCtrl.create({
		   message: 'Nom d\'utilisateur mis à jour',
		   duration: 3000
		 });
	   this.editMode = !this.editMode;
	   return  toast.present(); 
   })
   .catch( res=>{
	   let toast =  this.toastCtrl.create({
		   message: 'Erreur de connexion',
		   duration: 3000
		 });
	   this.editMode = !this.editMode;
	   return  toast.present(); 
   });
}
  ionViewDidLoad() {
    //this.navCtrl.push(HomePage);
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}


}
