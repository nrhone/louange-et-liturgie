import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController} from 'ionic-angular';
import { FavorsProvider } from '../../providers/favors/favors';
import { SongsProvider } from '../../providers/songs/songs';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the ArchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-archive',
  templateUrl: 'archive.html',
})
export class ArchivePage {
  archives:any[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public favorsProvider: FavorsProvider,
    public songsProvider: SongsProvider,
	public toastCtrl: ToastController,
	private translateProvider:TranslateProvider
  ) 
  {
    this.favorsProvider.getArchives().then(result=>{
      this.archives = result;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArchivePage');
  }
  restore(playlist, index){
    this.favorsProvider.restore(index).then(playlists=>{
      this.archives = playlists;
      let songs = [];
      playlist.songs.forEach(song => {
        this.songsProvider.getSongById(song.orgKey).then(res=>{
          songs.push(res);
        })
      });
      this.favorsProvider.createPlaylist(playlist.title,songs, playlist.createdTime).then(
        result=>{
          let toast = this.toastCtrl.create({
            message: 'La playlist a été restaurée',
            duration: 3000
          });
          toast.present();
        }
      );
    })
  }
  	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}

}
