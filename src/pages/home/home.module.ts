import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PipesModule } from '../../pipes/pipes.module';
import {RoundProgressModule} from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [
	HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    PipesModule,
    RoundProgressModule
  ],
  exports:[]
})
export class HomePageModule {
  
}
