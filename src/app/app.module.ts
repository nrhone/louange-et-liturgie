import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HeaderColor } from '@ionic-native/header-color';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook'; //Added Facebook
import { Insomnia } from '@ionic-native/insomnia';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { FCM } from '@ionic-native/fcm';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';
import { NativeAudio } from '@ionic-native/native-audio';
import { IonicStorageModule } from '@ionic/storage';
import {HttpClientModule} from "@angular/common/http";
import { HTTP } from '@ionic-native/http';
import { HttpModule } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { AppRate } from '@ionic-native/app-rate';

//providers
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { FavorsProvider } from '../providers/favors/favors';//<--- here
import { SongsProvider } from '../providers/songs/songs';
import { ParamsProvider } from '../providers/params/params';
import { ProductsProvider } from '../providers/products/products';
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';
import { AnalyticsProvider } from '../providers/analytics/analytics';
import { NotesProvider } from '../providers/notes/notes';
import { DonationsProvider } from '../providers/donations/donations';
import { PopoverPage } from '../pages/select-users/select-users';

//Text editor
// Import Froala Editor.
import "froala-editor/js/froala_editor.pkgd.min.js";

// Import Angular2 plugin.
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';



import { MyApp } from './app.component';
import { TranslateProvider } from '../providers/translate/translate';
import { MessagesProvider } from '../providers/messages/messages';
import { MigrationProvider } from '../providers/migration/migration';

export const firebaseConfig = {
  apiKey: "AIzaSyC7Da9yjFHshtfQZn_tUr3SGT7n8oRoE90",
    authDomain: "songbook-4972a.firebaseapp.com",
    databaseURL: "https://songbook-4972a.firebaseio.com",
    projectId: "songbook-4972a",
    storageBucket: "songbook-4972a.appspot.com",
    messagingSenderId: "197868433292"
};

@NgModule({
  declarations: [
    MyApp,
	PopoverPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
		name: '__ccn_db',
	}),
    AngularFireModule.initializeApp(firebaseConfig),
    //AngularFirestoreModule.enablePersistence(),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RoundProgressModule,
    FroalaEditorModule.forRoot(), 
	FroalaViewModule.forRoot(),
    HttpClientModule,
	HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverPage
  ],
  providers: [
    StatusBar,
    HeaderColor,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SongsProvider,
    AuthServiceProvider,
    FavorsProvider,
    ParamsProvider,
    GooglePlus,
    Facebook,
    Insomnia,
    InAppBrowser,
    EmailComposer,
    FCM,
    AppVersion,
    Keyboard,
    NotesProvider,
    ProductsProvider,
    HTTP,
    NativeAudio,
    SmartAudioProvider,
    AnalyticsProvider,
	GoogleAnalytics,
    DonationsProvider,
	TranslateProvider,
	InAppPurchase,
    MessagesProvider,
	MigrationProvider,
	AppRate
    //ProjectorProvider
  ]
})
export class AppModule {}
