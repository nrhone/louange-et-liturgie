import { Component ,ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController , PopoverController , Searchbar , LoadingController, ToastController} from 'ionic-angular';
import firebase from 'firebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { Keyboard } from '@ionic-native/keyboard';
import { AnalyticsProvider } from '../../providers/analytics/analytics';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the SelectUsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  template: `
    <ion-list radio-group [(ngModel)]="userRights" (ngModelChange)="change()">
      <ion-item tappable >
          <ion-icon name="create"></ion-icon>
          <ion-label>Édition</ion-label>
          <ion-radio value="edition"></ion-radio>
      </ion-item>
      <ion-item tappable >
        <ion-icon name="eye"></ion-icon>
        <ion-label>Consulation</ion-label>
        <ion-radio value="view"></ion-radio>
        </ion-item>
      <ion-item tappable (click)="select('delete')">
        <ion-icon name="trash"></ion-icon>
        Supprimer
      </ion-item>
    </ion-list>
  `
})
export class PopoverPage {
  contentEle: any;
  textEle: any;
  user:any;
  userRights:any="view";
  constructor(private navParams: NavParams, public viewCtrl: ViewController) {
    this.user = this.navParams.data.user;
    if(this.user.canEdit) this.userRights="edition";
  }
  ngOnInit() {
  }
  select(action){
    this.viewCtrl.dismiss(action);
  }
  change(){
    this.select(this.userRights);
  }
}



@IonicPage()
@Component({
  selector: 'page-select-users',
  templateUrl: 'select-users.html',
})
export class SelectUsersPage {
  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  @ViewChild('search') searchbar:Searchbar;
  public users:any[];
  private allUsers:any[];
  public fireDb: any;
  public userSearch:string="";
  public selectedUsers:Array<any>=[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController, 
    afs: AngularFirestore, 
    private popoverCtrl: PopoverController,
    private keyboard: Keyboard,
    private loadingCtrl:LoadingController,
    private toastCtrl:ToastController,
	private analyticsProvider:AnalyticsProvider, 
	private auth:AuthServiceProvider
  )
  {
    this.fireDb = firebase.firestore();
	this.selectedUsers = this.navParams.get('users');
	
  }
  

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Partage de playlist");
    setTimeout(()=>{
      //input.setFocus();
      this.searchbar.setFocus();
      this.keyboard.show();
    },150)
  }
  initializeItems(): void {
    this.users = this.allUsers;
  }
  onInput(ev){
    //this.themes = this.allThemes;
    var val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '' && val.length==3) {
      this.getUsers(val);
    }
    else if (val && val.trim() != '' && val.length==5) {
      this.getUsers(val);
    }
    else if (val && val.trim() != '' && val.length==7) {
      this.getUsers(val);
	}
	else if (val && val.trim() != '' && val.length==9) {
		this.getUsers(val);
	}
	else if (val && val.trim() != '' && val.length==11) {
		this.getUsers(val);
	}
  }
  getUsers(val){
	  console.log("get users");
    this.fireDb.collection("usersInfo").where("username", ">=", val).orderBy("username").limit(5).get().then(
      snap => {
        let users = [];
        snap.forEach(
          user => {
            let userSummary = user.data();
            userSummary.userId=user.id;
            users.push(userSummary);
          }
        )
        //this.allUsers = users;
        this.users = users;
        //loading.dismiss();
    })
    .catch(err=>{
      //loading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Erreur de connexion',
        duration: 3000,
      });   
      toast.present();
    })
  }
  select(user){
    if(!this.isSelected(user)) this.selectedUsers.push(user);
  }
  delete(index){
    this.selectedUsers.splice(index, 1);
  }
  isSelected(currentUser){
    let selected=false;
    this.selectedUsers.forEach( user => {
      if(user.userId==currentUser.userId){selected= true;}
    });
    return selected;
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  share() {
    this.viewCtrl.dismiss(this.selectedUsers);
  }
  presentPopover(ev, index) {
    let popover = this.popoverCtrl.create(PopoverPage,{user:this.selectedUsers[index]});

    popover.present({
      ev: ev
    });
    popover.onDidDismiss((data)=>{
      if(data=='delete') this.selectedUsers.splice(index, 1);
      else if(data == 'edition') this.selectedUsers[index].canEdit=true;
      else if (data == 'view') this.selectedUsers[index].canEdit=false;
    });
  }
}
