import { Component, ViewChild } from '@angular/core';
import { IonicPage, ViewController, NavParams, ToastController, ModalController, Searchbar} from 'ionic-angular';
import { TranslateProvider } from '../../providers/translate/translate'
import { SongsProvider } from '../../providers/songs/songs';
import { FavorsProvider } from '../../providers/favors/favors';
import { AnalyticsProvider } from '../../providers/analytics/analytics';
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the CapoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-song',
  templateUrl: 'add-song.html',
})
export class AddSongPage {
	@ViewChild('input') searchbar:Searchbar;
	songs:any[];songsBackup:any[];copyrights:any[];
	carnet:"APPLI_FR";
	selectedSongs:any[] = [];
	index:number;isShared:boolean=false;
	constructor(
		public viewCtrl: ViewController,
		private translateProvider:TranslateProvider,
		private songsProvider:SongsProvider,
		private favorsProvider:FavorsProvider,
		private navParams:NavParams,
		private toastCtrl:ToastController,
		private analyticsProvider:AnalyticsProvider,
		private keyboard: Keyboard
	) {
    this.songsProvider.getListOfSongs("alpha").then((value)=>{
		this.songs = value.slice();
		this.songsBackup = value.slice();

	});
	this.songsProvider.getCopyrights().then(cp=>{this.copyrights = cp; });
	this.index = this.navParams.get('listId');
	this.isShared = this.navParams.get('isShared');
  }

  ionViewDidLoad() {
	this.searchbar.setFocus();
	this.keyboard.show();
  }
  ionViewDidEnter(){
   
  }
  close(){
    this.viewCtrl.dismiss();
  }
  isSelected(song){
    return (this.selectedSongs.indexOf(song)>-1);
  }
  
  selectSong(song){
    if(this.isSelected(song)) {
      this.selectedSongs.splice(this.selectedSongs.indexOf(song),1);
    }
    else{
      this.selectedSongs.push(song);
    }
  }
  
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	filterItems(ev){
		let accent = [
			/[\340-\346]/g, // a
			/[\350-\353]/g, // e
			/[\354-\357]/g, // i
			/[\362-\370]/g, // o
			/[\371-\374]/g, // u
			/[ç]/g, // u
			/[,]/g,
			/['’]/g
		];
		let noaccent = ['a','e','i','o','u','c','',''];
		let filterAccents = function(title:string){
			for(let i = 0; i < accent.length; i++){
				title = title.replace(accent[i], noaccent[i]);
			}
			return title;
		}
		let compare = function(searchString:string, string2:string){
			searchString = filterAccents(searchString.toLowerCase());
			string2 = filterAccents(string2.toLowerCase());
			let isInString:boolean = true;
			let wordArray = searchString.split(' ');
			for(let word in wordArray){
				if(string2.indexOf(wordArray[word])<0) isInString=false;
			}
			return isInString;
		}
	  	// Reset items back to all of the items
		this.songs = this.songsBackup.slice();
		// set val to the value of the searchbar
		let val = ev.target.value;
	  
		  // if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.songs = this.songs.filter((item) => {
			  if(compare(val, item.title)) return true;
			  else if(item.alternate_title && compare(val, item.alternate_title)) return true;
			  else if(item.i18n){
				for(let index in item.i18n){
				  if (compare(val,item.i18n[index])) return true;
				}
			  }
			  /*else if(this.params.search=='lyrics') {
				  if (compare(val,item.texteConcat)) return true;
			  }*/
			  else return false;
			})       
		  }
	}
	onCancel(ev){	
	}
	addSongs(){
		return this.favorsProvider.addSongsToPlaylist(this.selectedSongs, this.index, this.isShared).then(result=>{
			if(result.success){
				this.viewCtrl.dismiss(this.selectedSongs);
				let toast = this.toastCtrl.create({
				  message: this.translate('added_to_playlist','Ajoutés à la playlist'),
				  duration: 2000
				});
				toast.present();
				this.selectedSongs.forEach(song => {
				  this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
				})
				this.selectedSongs=[];
			}
			else {
			  let toast = this.toastCtrl.create({
				message: "Erreur : " + result.message,
				duration: 3000
			  });
			  toast.present();
			  
			}
		  });
	}
	
}
