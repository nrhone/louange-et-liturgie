import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FavorsProvider } from '../../providers/favors/favors';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the NewPlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-playlist',
  templateUrl: 'new-playlist.html',
})
export class NewPlaylistPage {
  newPlaylist:any = {};
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    public favorsProvider:FavorsProvider,
    private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
	) 
  {
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Nouvelle playlist");
  }
  close() {
    this.viewCtrl.dismiss();
  }
  createNewPlaylist(){
    this.favorsProvider.createPlaylist(this.newPlaylist.title,[],0).then(
      result=>{
        this.analyticsProvider.clickEvent("Création de playlist");
        this.viewCtrl.dismiss(result);
      }
    );
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}

}
