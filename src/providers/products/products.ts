import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProductsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductsProvider {
  configUrl:string="https://www.laboutique-chemin-neuf.com/api/v1/ecommerce/products?fields%5B%5D=text&fields%5B%5D=summary&fields%5B%5D=image&lang=fr%7Cfr&limit=12&pageId=55c8777145205ef317c62e2a&queryId=5a201ce3396588ec3c3fb24c&siteId=55c8777145205ef317c62e09&specialOffersOnly=false&start=0";
  constructor(private http: HTTP) {
  }
  getProducts(){
    return this.http.get(this.configUrl, {}, {})
    .then(data => {
      return JSON.parse(data.data);

    })
    .catch(error => {
    });
  }

}
