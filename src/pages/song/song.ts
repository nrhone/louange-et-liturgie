import { Component,ViewChild , ViewChildren , QueryList, EventEmitter } from '@angular/core';
import { IonicPage, Content , NavController, NavParams, Slides , ActionSheetController, ToastController, PopoverController , AlertController , LoadingController } from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
//import { ProjectorProvider } from '../../providers/projector/projector';
import { FavorsProvider } from '../../providers/favors/favors';
import { ParamsProvider } from '../../providers/params/params';
import { NotesProvider } from '../../providers/notes/notes';
import { AnalyticsProvider } from '../../providers/analytics/analytics';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import "froala-editor/js/froala_editor.pkgd.min.js";
import firebase from 'firebase';
//declare var MIDI:any;
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
/**
 * Generated class for the SongPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 

@IonicPage()
@Component({
  selector: 'page-song',
  templateUrl: 'song.html',
})
export class SongPage {
	@ViewChild('myslider') slider: Slides;
	@ViewChildren(Content) parents:QueryList<any>;
  options:any;
  selectedItem: any; rubriqueId: string;
  song:any;
  songs:any = [];
  copyrights:any;
  activeIndex : number;
  playlists:any[];
  showYoutube:boolean=false;
  orderBy:string="alpha";
  hasExultet:boolean=false;
  hasYoutube:boolean=false;
  hasSpotify:boolean=false;
  hasTunes:boolean=false;
  hasNote:boolean=false;
  carnet:string;
  viewedTime:number;
  isLoggedIn:boolean=false;
  isConnected:boolean=false;
  localSongs:boolean=false;
  editMode=false;
  playNote:boolean=false;first_note:string;
  fullScreenMode:boolean=false;
  showYoutubePlayer:boolean = false; hideYoutube:boolean=false; youtubeUrl:string="";
  cpStates={reserved:"Droits réservés", permission:"Utilisé avec permission"};
  public editorOptions: Object = {
	placeholderText: 'Ajouter une note'
	//shortcutsEnabled:['bold', 'italic']
    /*toolbarButtonsXS: ['bold' ,'color', 'fontSize', 'insertLink',  '|', 'undo', 'redo'],
    toolbarButtons: ['bold' ,'color', 'fontSize', 'insertLink',  '|', 'undo', 'redo'],*/
  };
  isScrolling:boolean=false;manualScroll:boolean=false;scrollTop:number=0;interval=null;showScroll:boolean=false;
  tonality:string=null;displayTonality:string=null; 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private songsProvider:SongsProvider,
    private favorsProvider:FavorsProvider,
    private paramsProvider:ParamsProvider,
    private notesProvider:NotesProvider,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    private iab: InAppBrowser,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider,
    public audioProvider:SmartAudioProvider,
    private analyticsProvider:AnalyticsProvider,
	private loadingCtrl:LoadingController,
    //private projector: ProjectorProvider
  ) 
  {
  this.options={};
    this.selectedItem = navParams.get('id');
    this.orderBy = navParams.get('order');
    this.rubriqueId = navParams.get('rubrique') ? navParams.get('rubrique') : '';
    this.localSongs = navParams.get('local') ? navParams.get('local') : false;
    this.songsProvider.getCloseSongs(this.selectedItem, '',this.rubriqueId, this.orderBy, this.localSongs).then(result=>{
      this.songs = result.list;
      this.activeIndex = result.index;
      this.songs[this.activeIndex].showSong = true;
      this.hasExultet = this.songs[this.activeIndex].exultet?true:false;
      this.hasSpotify = this.songs[this.activeIndex].spotify?true:false;
      this.hasYoutube = this.songs[this.activeIndex].youtube?true:false;  
      this.hasTunes = this.songs[this.activeIndex].itunes?true:false;  
	  this.hasNote = this.songs[this.activeIndex].first_note?true:false;  
      if(this.hasNote) this.audioProvider.preload(this.songs[this.activeIndex].first_note,'assets/sounds/'+this.songs[this.activeIndex].first_note+'.mp3');
      this.viewedTime=Date.now();
      this.notesProvider.getNoteById(this.songs[this.activeIndex].orgKey).then((informations) => {
		if(informations) {
			this.songs[this.activeIndex].notes=informations.notes;
			this.songs[this.activeIndex].duration=informations.duration;
			if(informations.capo) this.songs[this.activeIndex].capo=informations.capo;	
			if(informations.transposition || informations.transposition ==0) {
				this.songs[this.activeIndex].transposition=informations.transposition;
			}
		}
		if(this.songs[this.activeIndex].tonality) {
			this.getTonality(this.songs[this.activeIndex]);
		}
		else this.tonality = null;
		
	})
    });
    this.paramsProvider.getParams().then(params=>{
      if(params) {this.options=params; this.carnet=params.carnet;}
      else {
        this.options = {showChords:false, capoMode:false, fontSize:14, position:false};
      }

    })
    this.songsProvider.getCopyrights().then(cp=>{this.copyrights = cp; })
    this.auth.getUserInfo().then(res=>{
      if(res && res != null) {this.isLoggedIn=true;}
    });
    firebase.database().ref('.info/connected').on('value', snapshot => {
      // If we're not currently connected, don't do anything.
      if (snapshot.val() == false) {
        this.isConnected=false;
      }
      else {
        this.isConnected=true;
      }
    });
    //this.projectionMode=this.projector.isConnected;
    
  }
  ionViewDidLoad() {

  }
  ionViewWillLeave(){
    if(this.viewedTime && (Date.now()-this.viewedTime > 30*1000)){
      this.analyticsProvider.songEvent('30s',this.songs[this.slider.getActiveIndex()]);
    }  
    if(this.viewedTime && (Date.now()-this.viewedTime > 90*1000)){
      this.analyticsProvider.songEvent('2mn',this.songs[this.slider.getActiveIndex()]);
	} 
	if(this.isScrolling) this.stopScroll()
  }
  
  initFabs(){
    if(this.slider &&this.songs){
      let index = this.slider.getActiveIndex();
      if(this.songs[index]){
        this.hasExultet = this.songs[index].exultet?true:false;
        this.hasSpotify = this.songs[index].spotify?true:false;
        this.hasYoutube = this.songs[index].youtube?true:false;
        this.hasTunes = this.songs[index].itunes?true:false;
        this.hasNote = this.songs[index].first_note?true:false;  
		if(this.songs[index].tonality) {
			this.getTonality(this.songs[index]);
		}
		else this.tonality = null;        
		if(this.hasNote) this.audioProvider.preload(this.songs[index].first_note,'assets/sounds/'+this.songs[index].first_note+'.mp3');
	  }
	  let content = this.parents.toArray()[index+1];
	  setTimeout(()=>{
		content.resize();
		var height = content.getContentDimensions().scrollHeight;
		setTimeout(()=>{
			if(height>content.contentHeight*1.2) this.showScroll=true;
			else this.showScroll=false;
		},50);  
	  },50);
  
    }
  }
  
  nextSong(){
	if(this.isScrolling) this.stopScroll();

    let newIndex = this.slider.getActiveIndex();
	this.activeIndex = newIndex;

    if(this.viewedTime && (Date.now()-this.viewedTime > 30*1000)){
      this.analyticsProvider.songEvent('30s',this.songs[this.slider.getPreviousIndex()]);
    }
    if(this.viewedTime && (Date.now()-this.viewedTime > 90*1000)){
      this.analyticsProvider.songEvent('2mn',this.songs[this.slider.getPreviousIndex()]);
    }
    this.viewedTime=Date.now();
    if(this.songs[newIndex] && !this.songs[newIndex].showSong) {
      this.songs[newIndex].showSong = true;
      this.notesProvider.getNoteById(this.songs[newIndex].orgKey).then(info => {
		this.songs[newIndex].notes=info.notes;
		this.songs[newIndex].duration=info.duration;
		this.songs[newIndex].capo=info.capo;
		this.songs[newIndex].transposition=info.transposition;
		if(this.songs[newIndex].tonality) {
			this.getTonality(this.songs[newIndex]);
		}
		else this.tonality = null;
      });
      this.initFabs();
    }
    else this.initFabs();
    if(newIndex==1) {
      this.songs[0].showSong = true;
      this.initFabs();
      this.notesProvider.getNoteById(this.songs[0].orgKey).then(info => {
		this.songs[0].notes=info.notes;
		this.songs[0].duration=info.duration;
      });
      /*ONLY IF GOING IN THE RIGHT WAY*/
      if(this.slider.swipeDirection=='prev') this.updateSongs('backward');
	}
  }
  
  updateSongs(dir){   
    let newIndex = this.slider.getActiveIndex();
    if(newIndex && this.songs && this.songs.length>0) {
      
      let key:any;
      if(dir=='forward') key = this.songs[newIndex+1].orgKey;
      else if(dir=='backward') key = this.songs[newIndex-1].orgKey;
      this.songsProvider.getCloseSongs(key, dir, this.rubriqueId,  this.orderBy, this.localSongs).then(result=>{
        if(dir=='forward') {
          this.songs = this.songs.concat(result.list); 
        }
        else if(dir=='backward') {
          this.slider.slideTo((result.list).length, 0, false);        
          this.songs = (result.list).concat(this.songs); 
		}    
		this.activeIndex = this.slider.getActiveIndex();   
      })
    }
  }
  
  async presentActionSheet() {
    let loader:any;
    if(this.isConnected) {
      loader = this.loadingCtrl.create({
        content: "Getting playlists...",
      });
      loader.present();
    }
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Ajouter à la playlist',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    this.playlists = await this.favorsProvider.getLocalPlaylists();
    for(let i in this.playlists){
      //ajouter les playlists, sauf playlist locale partagée
      if(!this.playlists[i].isShared) {
        actionSheet.addButton(
          {'text':this.playlists[i].title, 
            'handler': () => {
            this.addToPlaylist(i, this.playlists[i].title,false);
            }
          }
        );
      }
    }
    if(this.isConnected) {
      let sharedPlaylists = await this.favorsProvider.getSharedPlaylists();
      for(let i in sharedPlaylists){
        //ajouter les playlists
        if(sharedPlaylists[i].canEdit) actionSheet.addButton(
          {'text':sharedPlaylists[i].title, 
            'handler': () => {
            this.addToPlaylist(i, sharedPlaylists[i].title,true);
            }
          });
      }
    }
    actionSheet.addButton(
      {
        text:'Créer une playlist',
        icon:'add',
        handler:()=>{
          this.analyticsProvider.clickEvent("Création de playlist"); 
          let prompt = this.alertCtrl.create({
            title: 'Nouvelle playlist',
            //message: "Enter a name for this new album you're so keen on adding",
            inputs: [
              {
                name: 'title',
                placeholder: 'Titre'
              },
            ],
            buttons: [
              {
                text: 'Annuler',
                handler: data => {
                }
              },
              {
                text: 'Créer',
                handler: data => {
                  if(data.title == "") return false;
                  else {
                    this.favorsProvider.createPlaylist(data.title,[],0).then(()=>{
                      actionSheet.dismiss();
                      this.presentActionSheet();
                    });
                    
                  }
                }
              }
            ]
          });
          prompt.present();
        }
      }
    );
    if(this.isConnected) loader.dismiss();
    actionSheet.present();
  }
  addToPlaylist(index, playlistTitle, isShared:boolean){
    let songIndex = this.slider.getActiveIndex();
    let song = this.songs[songIndex];console.log(song);
    this.favorsProvider.addSongToPlaylist(song, index, isShared).then(result=>{
      if(result.success){
        let toast = this.toastCtrl.create({
          message: 'Ajouté à la playlist '+playlistTitle,
          duration: 2000
        });
        toast.present();
        this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
      }
      else {
        let toast = this.toastCtrl.create({
          message: "Erreur : " + result.message,
          duration: 3000
        });
        toast.present();
        this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
      }
    });
  }
  openOptions(ev) {
	//this.isFavorite();
	let myEmitter = new EventEmitter < any >();
	myEmitter.subscribe(
		v=> {
			if(this.songs[index].tonality) this.getTonality(this.songs[index]);
		}
	);
    let index = this.slider.getActiveIndex();
    let popover = this.popoverCtrl.create("OptionsPage", {"options":this.options, "song":this.songs[index],"isLoggedIn":this.isLoggedIn,"tonality":this.tonality, emitter:myEmitter});
    popover.present({
      ev: ev,
    });
    let capo = this.songs[index].capo?this.songs[index].capo:0;
	let transposition = this.songs[index].transposition?this.songs[index].transposition:0;
    popover.onDidDismiss((data)=>{
		if(this.songs[index].tonality) this.getTonality(this.songs[index]);
		if(capo != this.songs[index].capo) {
			this.notesProvider.updateCapoById(this.songs[index].orgKey, this.songs[index].capo);
	  }
	  if(transposition != this.songs[index].transposition) {
		this.notesProvider.updateTranspositionById(this.songs[index].orgKey, this.songs[index].transposition);
	}
	this.paramsProvider.updateParams(this.options);  
      if(data && data.editMode !=null) this.editMode=data.editMode;
    });
    
  }
  getUrl(url:string){
    let videoId = url.split("watch?v=")[1];
    return "https://www.youtube.com/embed/"+videoId;
  }
  openUrl(){
    let index = this.slider.getActiveIndex();
    this.analyticsProvider.clickEvent("Exultet", this.songs[index].title)
    this.iab.create(this.songs[index].exultet, "_system", "location=true");
  }
  openSpotify(){
    let index = this.slider.getActiveIndex();
    let url = this.songs[index].spotify;
    this.analyticsProvider.clickEvent("Spotify", this.songs[index].title)
    let id = url.split("track/")[1];
    let popover = this.popoverCtrl.create("SpotifyPage",{id:id, type:"spotify"});
    popover.present({
    });
	popover.onDidDismiss(res=>{this.viewedTime=Date.now()})
}
  openItunes(){
    let index = this.slider.getActiveIndex();
    this.analyticsProvider.clickEvent("Itunes", this.songs[index].title)
    this.iab.create(this.songs[index].itunes, "_system", "location=true");
  }
  openYoutube(){
    let index = this.slider.getActiveIndex();
    let url = this.songs[index].youtube;
    this.analyticsProvider.clickEvent("Youtube", this.songs[index].title)
	let id = url.split("watch?v=")[1];
	
	if(this.songs[index].id_ltc) {
		let popover = this.popoverCtrl.create("SpotifyPage",{id:id, type:"youtube"});
		popover.present({
		});
		popover.onDidDismiss(res=>{this.viewedTime=Date.now()})
	}
	else {
		this.showYoutubePlayer = true;
		this.youtubeUrl = "https://www.youtube.com/embed/"+id+"?showinfo=0"
	}
}

slideVideo(){
	if(!this.hideYoutube) {
		document.getElementById('video_container').style.marginLeft = "80%";
		this.hideYoutube = true;	
	}
	else {
		document.getElementById('video_container').style.marginLeft = "5%";
		this.hideYoutube = false;	
	}
}
async closeVideo(){
	this.showYoutubePlayer = false;
}
  editNote(){
	this.editMode=true;
  }
  
  saveNote(){
	let index = this.slider.getActiveIndex();
    if(this.editMode) {
		setTimeout(()=>{
			  this.notesProvider.updateNoteById(this.songs[index].orgKey, this.songs[index].notes);
		},100);
    }
    this.editMode = !this.editMode;
  }
  editSong(){
    let index = this.slider.getActiveIndex();
    this.navCtrl.push("AddPage",{id:this.songs[index].orgKey})
  }
  
  play(){
    let songIndex = this.slider.getActiveIndex();
    if(this.songs[songIndex] && this.songs[songIndex].first_note){
      this.first_note=this.songs[songIndex].first_note.substr(0, this.songs[songIndex].first_note.length-1);
      this.playNote=true;
      this.audioProvider.play(this.songs[songIndex].first_note);
      setTimeout(() => {
        this.playNote=false;
       }, 1000);
    }
  }
  fullScreen(screenMode:boolean){
    this.fullScreenMode=screenMode;
  }
  setDuration(myEvent){
	let index = this.slider.getActiveIndex();
	let popover = this.popoverCtrl.create("DurationPage",{song:this.songs[index]});
	popover.onDidDismiss((data)=>{
		this.notesProvider.updateDurationById(this.songs[index].orgKey, this.songs[index].duration);
	});
	popover.present({     
		ev: myEvent
    });
  }
  
  autoScroll(ev, toggle:boolean){
	let index = this.slider.getActiveIndex();	
	let content = this.parents.toArray()[index+1];content.resize();
	this.scrollTop = content.getContentDimensions().scrollTop;
	var height = (content.getContentDimensions().scrollHeight-content.contentHeight);
	var duration = this.songs[index].duration*1000;
	let speed = height/duration; let intervalle = 10; let time=0;
	if(!this.isScrolling || !toggle) {
		clearInterval(this.interval); this.isScrolling=false;
		this.interval=setInterval(()=>{
			time+=intervalle;
			this.scrollTop+=speed*intervalle;
			content.scrollTop=this.scrollTop;
			if(this.scrollTop>height*1.2) {
				clearInterval(this.interval); this.isScrolling=false;
			}
		}, intervalle);
		this.isScrolling=true;
	}
	else {
		clearInterval(this.interval); this.isScrolling=false;
	}
  }
	stopScroll(){
		setTimeout(()=>{
			clearInterval(this.interval);this.isScrolling=false;
		},50);
		
	}
  	slower(ev){
		let song = this.songs[this.slider.getActiveIndex()];
		if(song.duration<330) song.duration +=30;
		this.notesProvider.updateDurationById(song.orgKey, song.duration);
		this.autoScroll(ev,false);
	}
	faster(ev){
		let song = this.songs[this.slider.getActiveIndex()];
		if(song.duration>30) song.duration -=30;
		this.notesProvider.updateDurationById(song.orgKey, song.duration);
		this.autoScroll(ev,false);
	}
	startScroll(ev:any){
			if(this.isScrolling) {
			this.manualScroll=true;
			clearInterval(this.interval);
		}
	}
	endScroll(ev:any){
		if(this.isScrolling && this.manualScroll) {
			this.autoScroll(ev,false);
			this.manualScroll=false;
		}
	}
	getTonality(song){
		let transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"];
		//get tonality to display chords
		let isMinor = false;
		//transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"]
		let tonality = song.tonality.replace(/[m]/,(match, minor)=>{
			if(minor) isMinor = true;
			return "";
		})
		let oldIndex = transposeOrder.indexOf(tonality);
		let transpositionFactor = 0;
		if(!song.capo) song.capo=0;
		if(song.songbooks && song.songbooks[this.carnet] && (song.songbooks[this.carnet].capo || song.songbooks[this.carnet].capo==0 )) transpositionFactor += song.songbooks[this.carnet].capo;
		if(song.capo) transpositionFactor += song.capo;
		if(song.songbooks && song.songbooks[this.carnet] && song.songbooks[this.carnet].transposition) transpositionFactor -=  song.songbooks[this.carnet].transposition;
		if (song.transposition) transpositionFactor -= song.transposition;
		this.displayTonality = transposeOrder[(oldIndex-transpositionFactor+12)%12]; if(isMinor) this.displayTonality+="m";
		this.tonality = transposeOrder[(oldIndex-transpositionFactor+song.capo+12)%12]; if(isMinor) this.tonality+="m";
}
	

}


