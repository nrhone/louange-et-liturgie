import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnalyticsProvider } from '../../providers/analytics/analytics';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the RubriquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rubriques',
  templateUrl: 'rubriques.html',
})
export class RubriquesPage {
  rubriques:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private analyticsProvider:AnalyticsProvider, private translateProvider:TranslateProvider
    ) {
    this.rubriques = [
      {'title':'Animation','img':'animation.jpg','id':'ANIMATION'},
      {'title':'Louange','img':'louange.jpg','id':'LOUANGE'},
      {'title':'Adoration - Confiance','img':'ado.jpg','id':'ADORATION - CONFIANCE'},
      {'title':'Esprit-Saint','img':'esprit_saint.jpg','id':'ESPRIT-SAINT'},
      {'title':'Réconciliation','img':'reco.jpg','id':'RÉCONCILIATION'},
      {'title':'Refrains et litanies','img':'refrains.jpg','id':'REFRAINS ET LITANIES'},
      {'title':'Hymnes et cantiques','img':'hymnes.jpg','id':'HYMNES ET CANTIQUES'},
      {'title':'Psaumes','img':'psaumes.jpg','id':'PSAUMES'},
      {'title':'Chants liturgiques','img':'liturgie.jpg','id':'CHANTS LITURGIQUES'},
      {'title':'Ordinaires de la messe','img':'messes.jpg','id':'ORDINAIRES DE LA MESSE'},
      {'title':'Danses','img':'danses.jpg','id':'DANSES'},
      {'title':'Prières','img':'prieres.jpg','id':'PRIÈRES'},
      {'title':'Bénédicités','img':'prieres.jpg','id':'BÉNÉDICITÉS'}
    ];
  }


  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Rubriques");
  }
  goToRubrique(event, rubrique) {
    this.analyticsProvider.rubriqueEvent(rubrique.title);
    this.navCtrl.push("RubriquePage", {
      rubriqueId: rubrique.id,
      title:rubrique.title
    });
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}

}
