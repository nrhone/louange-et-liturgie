import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, reorderArray } from 'ionic-angular';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'
import { FavorsProvider } from '../../providers/favors/favors';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-folder',
  templateUrl: 'folder.html',
})
export class FolderPage {
	folderId:number;
	folder:any;
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		private analyticsProvider:AnalyticsProvider,
		private translateProvider:TranslateProvider,
		public favorsProvider:FavorsProvider,
		private alertCtrl:AlertController
	) {
		console.log("folder")
		this.folderId = navParams.get('id');
		this.favorsProvider.getFolder(this.folderId).then(res=>{
			this.folder=res;
		})
	}

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("About");
  }
  
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	goToPlaylist(playlistId) {
		
		this.navCtrl.push("PlaylistPage", {
			id: playlistId,
			isShared:false,
			canEdit:true
		});
		
	}
	deleteFolder(){
		let confirm = this.alertCtrl.create({
			title: 'Supprimer le dossier ?',
			message: 'Cette action est irréversible.',
			buttons: [
			  {
				text: 'Annuler',
				handler: () => {
				}
			  },
			  {
				text: 'Supprimer',
				handler: () => {
				  this.favorsProvider.deleteFolder(this.folderId).then(()=>{
					this.navCtrl.pop();
				  })
				}
			  }
			]
		  });
		  confirm.present();
	}
	deletePlaylist(index:number){
		this.folder.playlists.splice(index,1);
		this.favorsProvider.updateFolder(this.folderId,this.folder);
	}
	reorderItems(ev){
		this.folder.playlists = reorderArray(this.folder.playlists, ev);
		this.favorsProvider.updateFolder(this.folderId,this.folder);
	}
}


    