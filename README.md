# mise à jour iOs
cd ~/Documents/APPS/Songbook

1. cordova plugin remove cordova-plugin-googleplus
cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID="com.googleusercontent.apps.197868433292-jcji220kpgo0c1c1jcgcke3dbe8r07fs"

1. ionic cordova build ios --prod --release
2. Ouvrir XCode
3. Vérifier dans l'onglet général
4. Changer Bundle Identifier pour fr.chemin-neuf.chants
5. Product / Archive
6. Window / Orginizer pour mettre en ligne



# mise à jour Android version "moderne"
ionic cordova plugin remove cordova-plugin-crosswalk-webview
2. Augmenter numéro de version
4. ajouter 9 à la fin du android-versionCode
minSdkVersion = 19 dans config.xml
ionic cordova build android --prod --release


# Mise à jour de l'appli Android <4.4
cd ~/Documents/APPS/Songbook
1. ionic cordova plugin add cordova-plugin-crosswalk-webview
2. minSdkVersion = 16 dans config.xml
3. supprimer 9 à la fin du android-versionCode
ionic cordova build android --prod --release
# si erreur à propos de flavors => build.gradle l.188 : https://stackoverflow.com/questions/44105127/android-studio-3-0-flavor-dimension-issue
# étape pour créer la clé... à ne faire qu'une fois, pour la première version
#keytool -genkey -v -keystore chants.keystore -alias chants -keyalg RSA -keysize 2048 -validity 10000
#à chaque fois
#mot de passe : chants4jesus

---------------
# Déployer comme PWA
décommenter service worker dans index.html
commenter cordova.js
ionic build --prod
firebase deploy --only hosting:ccn-songbook

Facebook : 
1. ionic cordova plugin add cordova-plugin-facebook4 --variable APP_ID="353325841767533" --variable APP_NAME="Louange et Liturgie" --save

2. Si on supprime la platform android, ionic cordova platform add android@6.4.0 (pour pouvoir être compatible avec Android 4.4.2)
3. Générer la clé de hashage : keytool -exportcert -alias chants -keystore chants.keystore | openssl sha1 -binary | openssl base64

------------------
Google : 
#cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID="197868433292-eu6e7fg2jfga801l2ue0kk2l17leo0m4.apps.googleusercontent.com"
! Pour la signature SHA-1, indiquer celle donnée par Google Play App Signing

-------------------

FCM plugin : si  Terminating app due to uncaught exception 'com.firebase.core', reason: 'Default app has already been configured.' 
=> https://github.com/fechanique/cordova-plugin-fcm/issues/58
-------------------
Crosswalk 
cordova plugin add cordova-plugin-crosswalk-webview

----------------

Si The library com.google.android.gms:play-services-basement is being requested
Commenter les doublons dans platforms/android/project.properties
-----------------
iOs : si pb avec GoogleToolboxForMac : aller dans le dossier platforms/ios et faire pod update
------------------

Icone de notification:
créée avec http://romannurik.github.io/AndroidAssetStudio/icons-notification.html
ajouter les icônes dans platforms/android/res
ajouter platforms/android/res/values/colors.xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorAccent">#ff4814</color>
</resources>

Ajouter dans AndroidManifest.xml
        <meta-data android:name="com.google.firebase.messaging.default_notification_icon" android:resource="@drawable/ic_stat_icon" />
        <meta-data android:name="com.google.firebase.messaging.default_notification_color" android:resource="@color/colorAccent" />

-----------------
#UTILITIES
#Editeur de texte : 
https://github.com/froala/angular-froala-wysiwyg#usage





Songsheet generator :
http://tenbyten.com/software/songsgen/download.php

MIDI files
https://github.com/mudcube/MIDI.js/

Projection
http://fraunhoferfokus.github.io/cordova-plugin-presentation/

Firebase analytics
Pour iOs, installer CocoaPods : sudo gem install cocoapods



---------- STRUCTURE DB -------------
users
	id:{
		canCreatePlaylists : 0/1 (creation de playlists globles)
		canEdit : 0/1 (édition des chants)
		canEditUsers : 0/1 (édition des utilisateurs)
		canNotify : 0/1 (création de messages)
		userId : string
	}



-------------------
No installed build tools found. Install the Android build tools version 19.1.0 or higher

==> 
export ANDROID_HOME=/Users/macbook/Library/Android/sdk
