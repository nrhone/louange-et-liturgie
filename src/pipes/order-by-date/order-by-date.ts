import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the OrderByDatePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'orderByDate',
})
export class OrderByDatePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(array: any, carnet:string) {
    if(!carnet || !array || array === undefined || array.length === 0) return null;
    array.sort((a: any, b: any) => {
      let dateA = a.songbooks[carnet]&&a.songbooks[carnet].date?a.songbooks[carnet].date:0;
      let dateB = b.songbooks[carnet]&&b.songbooks[carnet].date?b.songbooks[carnet].date:0;
      if (+(dateA) > +(dateB)) {
        return -1;
      } else if ( +(dateA) < +(dateB)) {
        return 1;
      }
      else if (  +(dateA) == +(dateB)) {
        //si même date tri par ordre alpha
        if( a.title.toLowerCase()<b.title.toLowerCase()) {
          return -1;
        }
        else if( a.title.toLowerCase() > b.title.toLowerCase()) {
          return 1;
        }      
      }
      else return 0;
    });
    return array;

  }


}
