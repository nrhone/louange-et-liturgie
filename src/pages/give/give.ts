import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController , Platform} from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { DonationsProvider } from '../../providers/donations/donations'
import { TranslateProvider } from '../../providers/translate/translate'
import { ParamsProvider } from '../../providers/params/params'
import { AuthServiceProvider } from '../../providers/auth-service/auth-service'
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { AppRate } from '@ionic-native/app-rate';

/**
 * Generated class for the GivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-give',
  templateUrl: 'give.html',
})
export class GivePage {
  url:string="https://www.chemin-neuf.fr/fr/aidez-nous/5b2b9d463965880114b4673d/application-telephone-chants-du-chemin-neuf";
	items:any[];
	amount:string="5eur";
	  donations:any;
	  loading:boolean=false;
	  user:any;
	constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private iab: InAppBrowser,
	private analyticsProvider:AnalyticsProvider,
	private toastCtrl:ToastController,
	private donationsProvider:DonationsProvider,
	private translateProvider:TranslateProvider,
	private platform:Platform,
	private iap: InAppPurchase,
	private auth:AuthServiceProvider,
	private rateCtrl: AppRate,
	private paramsProvider:ParamsProvider
	) {
		this.donationsProvider.getDonations().then(res=>{
			this.donations=res; 
		});
		this.auth.getUserInfo().then(res=>{
			this.user = res;
		});
		if(this.platform.is('android')){
			this.loading=true;
			this.iap
			.getProducts(['1eur', '2eur', '5eur', '10eur', '20eur'])
			.then((products) => {
			  this.items = products;
			  this.loading=false;
			   //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
			})
			.catch((err) => {
				this.loading=false;
				let toast = this.toastCtrl.create({
					message: 'Erreur inconnue',
					duration: 3000,
				});
				toast.present();
			});
		}
  }

  ionViewDidLoad() {
	this.analyticsProvider.pageEvent("Dons");
  }
  give(){
    this.iab.create(this.url, "_system", "location=true");
    this.analyticsProvider.clickEvent("Donner");
  }
  getProductTitle(string){
	  return string.replace(/\(.*?\)/,"");
  }
  pay(){
	if(this.amount){
		let donation:any={date:(new Date()).getTime(), amount:0};
		if(this.user) {
			donation.userName = this.user.username;
			donation.email = this.user.email;
		}
		switch(this.amount){
			case "1eur": donation.amount = 1;break;
			case "2eur": donation.amount = 2;break;
			case "5eur": donation.amount = 5;break;
			case "10eur": donation.amount = 10;break;
			case "20eur": donation.amount = 20;break;
		}  
		this.analyticsProvider.donationEvent("Clic sur bouton", donation.amount);
		this.iap
		.buy(this.amount)
		.then((data)=> {
		  this.iap.consume(data.productType, data.receipt, data.signature).then(()=>{
			let toast = this.toastCtrl.create({
				message: 'Votre don a bien été enregistré. Merci de votre aide !',
				duration: 4000,
			});
			toast.present();
			
			this.donationsProvider.addDonation(donation);
			this.analyticsProvider.donationEvent("Don effectué", donation.amount);
			this.paramsProvider.stopDonationsPopup();
			this.donationsProvider.getDonations().then(res=>{
				this.donations=res;
			})
		  });
		  // {
		  //   transactionId: ...
		  //   receipt: ...
		  //   signature: ...
		  // }
		})
		.catch((err)=> {
		  console.log(err);
		});
	}
	else{
	  let toast = this.toastCtrl.create({
		  message: 'Merci d\'indiquer le montant du don',
		  duration: 3000,
	  });
	  toast.present();
	}
	
  }
  
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	showRatePrompt(){
		this.rateCtrl.preferences = {
			simpleMode: true,
			useLanguage:'fr',
			storeAppURL : {
				ios: '1349978129',
				android: 'market://details?id=fr.chemin_neuf.chants'
			}
		};
	  
		return this.rateCtrl.promptForRating(true); 
	}
}
