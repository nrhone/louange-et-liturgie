import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the KeysPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'keys',
})
export class KeysPipe implements PipeTransform {
  
  transform(value:any, carnet:string)  {
    let langOrder = [];let langLabels = {
      "FR":"Français",
      "EN":"English",
      "DE":"Deutsch",
      "ES":"Español",
      "NL":"Nederlands",
      "PT":"Português",
      "HU":"Magyar",
      "IT":"Italiano",
      "PL":"Polski",
      "LN":"Lingala",
      "LA":"Latin",
      "HE":"Hébreu",
	  "HE_trans":"",
	  "AR":"Arabe",
	  "AR_trans":"",
      "CR":"Créole",
      "CZ":"Tchèque",
      "MF":"Créole mauricien"
	};
	if(carnet) {
		let carnetLang = carnet.split("_")[1];
		switch(carnetLang) { 
		  case "HU": { 
			langOrder = ["HU","EN","FR","DE", "ES", "PT", "IT", "PL", "LN", "NL","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  }
		  case "DE": { 
			langOrder = ["DE","EN","FR","HU", "ES", "PT", "IT", "PL", "LN", "NL","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		}
		  case "IT": { 
			langOrder = ["IT","EN","FR","DE", "ES", "PT", "HU", "PL", "LN", "NL","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  }  
		  case "PL": { 
			langOrder = [ "PL","EN","FR","DE", "ES", "PT", "IT","HU",  "LN","NL", "LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  } 
		  case "BR": { 
			langOrder = [ "PT","EN","FR","DE", "ES", "PL", "IT","HU",  "LN", "NL","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  }
		  case "IL": { 
			langOrder = [ "HE","HE_trans","AR","AR_trans","FR","EN","DE", "ES", "PL", "IT","HU",  "LN","NL", "LA","PT","CR","CZ","MF"];
			break; 
		  }
		  case "AR": { 
			langOrder = [ "AR","AR_trans","HE","HE_trans","FR","EN","DE", "ES", "PL", "IT","HU",  "LN", "NL","LA","PT","CR","CZ","MF"];
			break; 
		  }
		  case "EN": { 
			langOrder = ["EN", "FR","DE", "ES", "PT", "HU", "IT", "PL", "LN", "NL","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  }
		  case "NL": { 
			langOrder = ["NL", "EN", "FR","DE", "ES", "PT", "HU", "IT", "PL", "LN","LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  }
		  default: { 
			langOrder = ["FR", "EN","DE", "ES", "PT", "HU", "IT", "PL", "LN","NL", "LA","HE","HE_trans","AR","AR_trans","CR","CZ","MF"];
			break; 
		  } 
		}
		let keys = [];
		if(value) {
		  for(let i in langOrder) {
			var lang = langOrder[i];
			if(value[lang] && value[lang].length) keys.push({key: lang, value: value[lang], label:langLabels[lang]});
		  }
		}
		return keys;
	}
	else {
		return null;
	}
  }
}