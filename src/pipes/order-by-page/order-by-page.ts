import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the OrderByPagePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'orderByPage',
})
export class OrderByPagePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(array: any, carnet:string) {
    if(!carnet || !array || array === undefined || array.length === 0) return null;
    array.sort((a: any, b: any) => {
      let pageA = a.songbooks[carnet] && a.songbooks[carnet].page?a.songbooks[carnet].page:null;
      let pageB = b.songbooks[carnet] && b.songbooks[carnet].page?b.songbooks[carnet].page:null;
      if(pageA && pageB) {
        if (+(pageA) < +(pageB)) {
          return -1;
        } 
        else if ( +(pageA) > +(pageB)) {
          return 1;
        } 
        else {//pour chants sur la même page
          if(a.massOrder && b.massOrder) {
            if(a.massOrder<b.massOrder) return -1;
            else return +1;
          }
          else if( a.title.toLowerCase()<b.title.toLowerCase()) {
            return -1;
          }
          else if( a.title.toLowerCase() > b.title.toLowerCase()) {
            return 1;
          }
        }
      }
      else {
        if(!pageA  && pageB) return 1;
        else if(pageA && !pageB) return -1;
        else return 0;
      }
    });
    return array;

  }
}

