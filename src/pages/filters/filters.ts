import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'
import { ParamsProvider } from '../../providers/params/params';
import { SongsProvider } from '../../providers/songs/songs';

/**
 * Generated class for the TagsSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html',
})
export class FiltersPage {
	options:any={};
	availableBooks:any[]=[];
	booksActionSheetOptions: any = {
		header: 'Sélectionner un livre',
	};
	reloadSongs:boolean=false;
	params:any={};
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider,
	private paramsService : ParamsProvider,
	private songsService : SongsProvider
	) {
		this.paramsService.getParams().then(res=>this.params=res);
		this.songsService.getAvailableCarnets().then(books=>{
			for(let book in books) {
				this.availableBooks.push({id:book, title:books[book].title})
			}
		});
		

  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Filters");
  }
  ionViewDidEnter(){
	this.options = this.navParams.data;
	this.reloadSongs = false;

  }
  ionViewWillLeave(){
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
	selectFilters(){
		this.viewCtrl.dismiss({
			options:this.options,
			reloadSongs:this.reloadSongs
		});	
	}
	booksFilter(books:any[]){
		return books.filter(book=>{return book.id != this.params.carnet})
	}
	updateSongBook(){ 
		this.reloadSongs = true; 
		this.paramsService.updateParams(this.params).then(
		   params=>{
			 this.songsService.changeCarnet(this.params.carnet);
		   }
		);
	}
	async updateParams(){
		this.reloadSongs = true; 
		await this.paramsService.updateParams(this.params);
		this.songsService.changeSelectedCarnets();
	}
  
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
}
