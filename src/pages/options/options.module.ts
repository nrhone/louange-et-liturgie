import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OptionsPage } from './options';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    OptionsPage,
  ],
  imports: [
	IonicPageModule.forChild(OptionsPage),
	PipesModule
  ],
})
export class OptionsPageModule {}
