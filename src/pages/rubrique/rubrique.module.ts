import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RubriquePage } from './rubrique';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RubriquePage,
  ],
  imports: [
    IonicPageModule.forChild(RubriquePage),
    PipesModule
  ],
})
export class RubriquePageModule {}
