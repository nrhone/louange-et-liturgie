import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TagsSearchPage } from './tags-search';

@NgModule({
  declarations: [
    TagsSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(TagsSearchPage),
  ],
})
export class TagsSearchPageModule {}
