import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaylistSongsPage } from './playlist-songs';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  declarations: [
    PlaylistSongsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaylistSongsPage),
    PipesModule,
    FroalaEditorModule,
    FroalaViewModule,
    ComponentsModule
  ],
})
export class PlaylistSongsPageModule {}
