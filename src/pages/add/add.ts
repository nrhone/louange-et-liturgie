import { Component , ViewChildren, QueryList} from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController } from 'ionic-angular';
import "froala-editor/js/froala_editor.pkgd.min.js";
import 'froala-editor/js/plugins.pkgd.min.js';

import FroalaEditor from 'froala-editor';
import { SongsProvider } from '../../providers/songs/songs';
import { AngularFirestore } from 'angularfire2/firestore';
import firebase from 'firebase';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
	@ViewChildren('divs') texts : QueryList<any>;
	public options:Object = {
		placeholderText: this.translate('text','Texte')+'...',
		charCounterCount: false,
		toolbarButtons:[ 'bold','underline','|', 'undo', 'redo','chord'],
		toolbarButtonsXs:[ 'bold','underline','|', 'undo', 'redo','chord'],
		immediateAngularModelUpdate:true,
		
    //htmlAllowedTags: []
  	};
  verseTypes = [
    {"value":"R", "label":this.translate('ref',"Refrain")},
    {"value":"C", "label":this.translate('couplet',"Couplet")},
    {"value":"B", "label":this.translate('bridge',"Bridge")},
    {"value":"I", "label":this.translate('intro',"Intro")},
    {"value":"T", "label":this.translate('inter',"Inter")},
    {"value":"P", "label":this.translate('preref',"Pré refrain")},
    {"value":"G", "label":this.translate('contrechant',"Contrechant")},
    {"value":"D", "label":this.translate('coda',"Coda")},
    {"value":"E", "label":this.translate('fin',"Fin")},
    {"value":"A", "label":this.translate('antienne',"Antienne")},
    {"value":"V", "label":this.translate('verset',"Verset")}
  ];
  rubriques:string[] = [
	  this.translate('ANIMATION',"ANIMATION"),
	  this.translate('rubrique2',"LOUANGE"),
	  this.translate('rubrique3',"ADORATION - CONFIANCE"),
	  this.translate('rubrique4',"ESPRIT-SAINT"),
	  this.translate('rubrique5',"RÉCONCILIATION"),
	  this.translate('rubrique6',"REFRAINS ET LITANIES"),
	  this.translate('rubrique7',"HYMNES ET CANTIQUES"),
	  this.translate('rubrique8',"PSAUMES"),
	  this.translate('rubrique9',"CHANTS LITURGIQUES"),
	  this.translate('rubrique10',"ORDINAIRES DE LA MESSE"),
	  this.translate('rubrique11',"DANSES"),
	  this.translate('rubrique12',"BÉNÉDICITÉS"),
	  this.translate('rubrique13',"PRIÈRES")];
  carnet:string="local";
  song:any=  { title: '', texte:[], songbooks:{local:{scenario:{}}} };
  newVerse:any={texte:'', type:'',lang:'',number:1};
  private songsCollection: any;
  public fireDb: any;
  public langs:any[];
  public id:string;
  public editMode:boolean=false;
  public themes:string[]=[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private songsProvider:SongsProvider,
    public alertCtrl: AlertController,
	afs: AngularFirestore,
	private translateProvider:TranslateProvider
  ) 
  {
    this.id = navParams.get('id');
    if(this.id && this.id!=''){
      this.editMode=true;
      this.songsProvider.getLocalSongById(this.id).then(song=>{
        this.song=song;
        this.song.texte=[];
        for (let lang in song.text_parts) {
          let verses = song.text_parts[lang];
          for(let verseId in verses) {
            let lines = verses[verseId].lines;
            let html="";
            for(let line of lines) {
              if(line!="") html+="<p>"+line+"</p>";
            }
            this.song.texte.push({
              lang:lang,
              type:verseId[0],
              texte:html,
              number:verseId[1]
            });
          }
        }

      });
    }
    this.songsProvider.getTags().then((themes) => {
      themes.forEach(theme=>{
        this.themes = this.themes.concat(theme.themes);
      })
    });  
    this.fireDb =  firebase.firestore();
    this.songsCollection = this.fireDb.collection('songs');
    FroalaEditor.DefineIcon('chordIcon', {NAME: 'music'});
    FroalaEditor.RegisterCommand('chord', {
      title: 'Ajouter un accord',
      type: 'dropdown',
      focus: false,
      undo: false,
      icon: 'chordIcon',
      refreshAfterCallback: true,
      options: {
        'A': 'A',
        'Bb': 'Bb',
        'B':'B',
        'C':'C',
        'C#':'C#',
        'D':'D',
        'Eb':'Eb',
        'E':'E',
        'F':'F',
        'F#':'F#',
        'G':'G',
        'G#':'G#'
      },
      callback: function (cmd, val) {
        this.html.insert('['+val+']');
      },
      
    });
    this.langs = [
      {"value":"FR", "label":"Français"},
      {"value":"EN", "label":"English"},
      {"value":"ES", "label":"Español"},
      {"value":"PT", "label":"Português"},
      {"value":"HU", "label":"Magyar"},
      {"value":"IT", "label":"Italiano"},
      {"value":"PL", "label":"Polski"},
      {"value":"LN", "label":"Lingala"},
      {"value":"LA", "label":"Latin"},
      {"value":"HE", "label":"Hébreu"},
      {"value":"CZ", "label":"Tchèque"},
      {"value":"CR", "label":"Créole"}
    ];
  }

  ionViewDidLoad() {
  }
  	createSong(){

		if(this.editMode) this.texts.forEach((div,i) => {
			this.song.texte[i].texte = div.nativeElement.value;
		});

      //créer le chant
		let text_parts={};this.song.songbooks[this.carnet].scenario={};
		this.song.texte.forEach(verse => {
			if(text_parts[verse.lang]) text_parts[verse.lang][verse.type+verse.number]={lines:verse.texte.replace(/<p>/g, "").replace(/<br>/g, "</p>").split("</p>")};
			else {
				text_parts[verse.lang]={};
				text_parts[verse.lang][verse.type+verse.number]={lines:verse.texte.replace(/<p>/g, "").replace(/<br>/g, "</p>").split("</p>")};
			}
			if(this.song.songbooks[this.carnet].scenario[verse.lang]) this.song.songbooks[this.carnet].scenario[verse.lang].push(verse.type+verse.number);
			else {
				this.song.songbooks[this.carnet].scenario[verse.lang]=[];
				this.song.songbooks[this.carnet].scenario[verse.lang].push(verse.type+verse.number);
			}
		});
		if(!this.editMode) {     
			this.song.orgKey='local-'+this.songsCollection.doc().id;
			this.song.createTime=(new Date()).getTime();
			this.song.lastUpdateTime=(new Date()).getTime();
			this.song.text_parts=text_parts;
			delete(this.song.texte);
			this.songsProvider.addLocalSong(this.song).then(res=>{
				this.navCtrl.setRoot("LocalSongsPage");
			})
    	}
    
		else {
			//enregistrer le chant
			this.song.lastUpdateTime=(new Date()).getTime();
			this.song.text_parts=text_parts;
			var texte = this.song.texte;
			delete(this.song.texte);
			this.songsProvider.editLocalSong(this.id,this.song).then(res=>{
				this.song.texte  = texte;
				//this.navCtrl.remove(1);
			})
		}

	}
  
  addVerse(){
    if(this.newVerse.type==''||this.newVerse.texte=='') {
      let popup = this.alertCtrl.create({
        title:'Champs manquants',
        subTitle:'Le couplet doit avoir un type, une langue et du texte.',
        buttons: ['OK']
      });
      popup.present();
    }
    else {
      this.song.texte.push({
        //lang:this.newVerse.lang,
        lang:'FR',
        type:this.newVerse.type,
        texte:this.newVerse.texte,
        number:this.newVerse.number
      });
      this.newVerse.texte='';
    }
    
  }
  	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
  

}