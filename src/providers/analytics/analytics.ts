import { Injectable } from '@angular/core';
import { config } from '../../ltc'
import { HTTP } from '@ionic-native/http';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


/*
  Generated class for the ProvidersLtcProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnalyticsProvider {
  private baseUrl = "https://db.ltc-asaph.com";
  private accessToken = "";
  isCordova:boolean=true;
  isReady:boolean=false;
  constructor(
    public http: HTTP, 
    private platform: Platform, 
    public storage: Storage,
    private ga: GoogleAnalytics,
  ) 
  {
    if(!this.platform.is('cordova')){
      this.isCordova = false;
    }
    this.getToken();
    this.platform.ready().then(() => {
      this.ga.startTrackerWithId('UA-129811783-1').then(() => {
        this.isReady=true;
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));   
    });

  }
  getToken(){
    let postData = config;
    postData['grant_type']="password";
    if(this.isCordova){
      this.http.post(this.baseUrl + "/oauth/token", postData, {})
        .then(data => {
          this.accessToken = JSON.parse(data.data).access_token;
          this.notify();
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  async registerSong(songId, title){
    return this.http.post(this.baseUrl + "/api/v1/songs/"+songId+"/usages", {song_id:songId},{ Authorization: 'Bearer '+this.accessToken })
      .then(data=>{
        //le chant a bien été enregistré chez LTC
        this.ga.trackEvent("song_view", "2mn", title);
        this.ga.trackEvent("song_view", "2mn_ltc", songId);
      })
      .catch(error=>{
        console.log("le chant n'a été enregistré - pas de connection internet");
        //le chant n'a pas été enregistré -> on enregistre localement
        this.getRegisteredSongs().then(res=>{
          if(res) res.push({song_id : songId, nb_usages : "1", title:title})
          else res = [{song_id : songId, nb_usages : "1", title:title}];
          return this.storage.set('ltc_songs', res);
        })
      })   
  }
  
  getRegisteredSongs(){
    return this.storage.get('ltc_songs');
  }

  async notify(){
    let data = await this.getRegisteredSongs();
    if(this.isReady){
    }
    else{
      await this.platform.ready();
      await this.ga.startTrackerWithId('UA-129811783-1');
	}
	if(data && data.length>0){
		let isConnected=false;
		for(let song of data){
			this.http.post(this.baseUrl + "/api/v1/songs/"+song.song_id+"/usages", {song_id:song.song_id},{ Authorization: 'Bearer '+this.accessToken })
				.then(data=>{
					this.ga.trackEvent("song_view", "2mn_ltc", song.song_id);
					this.ga.trackEvent("song_view", "2mn", song.title);
				})
				.catch(error=>{});
		}
		//if(isConnected){
		this.storage.set('ltc_songs',[]);
		//}
	}
    
    
  }
  songEvent(duration:string,song){
	console.log(duration, song);
    if(duration=='2mn' && song.id_ltc && song.id_ltc!=''){
      //save event if no connection and LTC
      this.registerSong(song.id_ltc, song.title);
    }
    else {
      this.ga.trackEvent("song_view", duration, song.title);  
    }

  }
  rubriqueEvent(rubrique:string){
    this.ga.trackEvent("click", "rubrique", rubrique);
  }
  pageEvent(page:string){
    this.ga.trackView(page);
  }
  clickEvent(type:string, label?:string){
    if(label) this.ga.trackEvent("click",type,label);
    else this.ga.trackEvent("click",type);
  }
  donationEvent(type:string, amount:string){
	this.ga.trackEvent("dons",type,amount);
  }

  
}
