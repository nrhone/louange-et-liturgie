import {Component, ViewChild } from '@angular/core';
import { IonicPage,  NavController, NavParams , LoadingController , AlertController, ModalController , ActionSheetController , ToastController, Searchbar } from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { FavorsProvider } from '../../providers/favors/favors';
import { ParamsProvider } from '../../providers/params/params';
import { OrderByDatePipe } from '../../pipes/order-by-date/order-by-date'
import { OrderByPagePipe } from '../../pipes/order-by-page/order-by-page'
import { FilterByLangPipe } from '../../pipes/filter-by-lang/filter-by-lang'
import { Loading } from 'ionic-angular/components/loading/loading';
import { AngularFirestore } from 'angularfire2/firestore';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/first';
import { Storage } from '@ionic/storage';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TranslateProvider } from '../../providers/translate/translate'
import { MessagesProvider } from '../../providers/messages/messages';
import { OrderByAlphaPipe } from '../../pipes/order-by-alpha/order-by-alpha.pipe';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild('input') searchbar:Searchbar;
  songs :any[]; 
  fakeArray:any[] = new Array(20);
  songsList : any[]; 
  songsBackup :any[];
  playlists:any[];
  showSearch : boolean =false;
  pageSearch : boolean =false;
  sortBy:string = "alpha";
  lang:string = "Tous";
  searchPage:number;
  isMultipleSelection:boolean=false;
  selectedSongs:any[] = [];
  copyrights:any;
  	filterOptions:any={
		sortBy:"alpha",
		lang:"Tous"
	};
	orderFilters: any = {
		"date":OrderByDatePipe,
		"page": OrderByPagePipe,
		"alpha": OrderByAlphaPipe
	}
  byLang:FilterByLangPipe;
  tags:string;
  loader:Loading;
  progress:number; showLoader:boolean=false;
  carnet:string="APPLI_FR";params:any;
  origin:string;
  isLoggedIn:boolean=false;
  user:any;
  messages:any[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private songsProvider:SongsProvider,
    private paramsProvider:ParamsProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private afs: AngularFirestore,
    private favorsProvider:FavorsProvider,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    private keyboard: Keyboard,
    private storage:Storage,
    private analyticsProvider:AnalyticsProvider,
    private auth: AuthServiceProvider, 
	private translateProvider:TranslateProvider,
	private messageProvider:MessagesProvider
  ) 
  {
	this.orderFilters.date = new OrderByDatePipe();
	this.orderFilters.page = new OrderByPagePipe();
	this.orderFilters.alpha = new OrderByAlphaPipe();
	this.byLang = new FilterByLangPipe();
    this.progress=0;
    if(!this.songs) {
      this.initSongs();  
    }
    this.paramsProvider.getParams().then(params=>{
      if(params) {
        if(params.carnet) {this.carnet=params.carnet;}
        else {
          params.carnet = "APPLI_FR"; this.carnet = params.carnet; this.paramsProvider.updateParams(params);
        }
      }
      else {
        let params ={showChords:true, capoMode:false, position:false, fontSize:16, veille:10, carnet:"APPLI_FR",chordFormat:"INT",darkMode:false};
        this.carnet = params.carnet; this.paramsProvider.updateParams(params);
	  }
	  this.params=params;
    })
    this.origin = navParams.get('origin');
    this.auth.getUserInfo().then(res=>{
      if(res && res != null && res!='no_user') {
		this.isLoggedIn=true;
		this.user=res;
      }
	});
	//get home message
	this.messageProvider.getHomeMessage().subscribe(res=>this.messages = res)
  }
  
  ionViewDidLoad() {
	this.analyticsProvider.pageEvent("Home");
  }

  async initSongs(){
    if(this.navParams.get('initialisation')){
      let version = await this.afs.collection("config").doc('version').valueChanges().first().toPromise();
      this.songsProvider.setVersion(version["number"]); 
      this.initialize();
    }
    else {
      this.getSongs().then(res=>{

        if(this.isLoggedIn && res.length<10) this.reinit();
      });
      let version = await this.songsProvider.getVersion();
      if(!version || version==0) {
        this.songsProvider.setVersion(1); 
      }
      let date = Date.now();    
      let lastUpdateTime = await this.storage.get("lastUpdateTime");
      if(!lastUpdateTime) {
        this.storage.set('lastUpdateTime', lastUpdateTime);
      }   
      //tous les 2 jours, vérifier les mises à jour
      else if(date-lastUpdateTime>2*24*3600*1000){
        //else{
        let databaseVersion = await this.afs.collection("config").doc('version').valueChanges().first().toPromise();
        //si on est connecté
        if(databaseVersion){
          //si nouvelle version de la DB ou si l'initialisation est demandée, on réinitialise tout
          if(version && (version !=databaseVersion["number"]) ){
            this.songsProvider.setVersion(databaseVersion["number"]);
            this.initialize();
          }
          // si c'est la même version, on update seulement les derniers chants modifiés lors de l'ouverture de l'app
          else if(this.navParams.get('updateSongs')){
			this.songsProvider.updateSongs().then(async res=>{
				this.getSongs();
				if(res>0){
					let toast = await this.toastCtrl.create({
						message:res+" "+this.translate("updated_songs","chants mis à jour"),
						duration:2000
					})
					await toast.present();
				}
			})
          }
          //update copyrights
          this.songsProvider.getCpVersion().then(cpVersion=>{
            if(!cpVersion || cpVersion==0) {
              cpVersion=1;
              this.songsProvider.setCpVersion(1);
            }
            this.afs.collection("config").doc('version').valueChanges().subscribe(
              cpDatabaseVersion =>{
                if(cpDatabaseVersion) {
                  if(cpVersion != cpDatabaseVersion["copyrights"]) {
                    this.songsProvider.setCpVersion(cpDatabaseVersion["copyrights"]);
                    this.songsProvider.updateCopyrights();
                  }
                }
              }
            );
          })

        }
      }  
    } 
  }
  doRefresh(refresher){
    this.songsProvider.updateSongs().then(nbOfSongs=>{
		if(nbOfSongs>0) this.getSongs().then(async (res)=>{
			refresher.complete();
			let toast = await this.toastCtrl.create({
				message:nbOfSongs+" "+this.translate("updated_songs","chants mis à jour"),
				duration:2000
			})
			await toast.present();
		});
		else refresher.complete();
    })

  }
  reinit(){
    let alert = this.alertCtrl.create({
      title: 'Erreur',
      message: 'Il semble que les chants aient été mal téléchargés. Voulez-vous télécharger à nouveau le contenu de l\'application ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.initialize();
          }
        }
      ]
    });
    alert.present();
  }
  initialize(){
    this.showLoader=true;
    this.songsProvider.initialize().subscribe(
      res=>this.progress+=res,
      (err)=>console.log(err),
      ()=>{
        this.songsProvider.initSongs().then(res=>{
          this.progress = 100;
          this.showLoader=false;
          this.getSongs();
        })
      }
    )
  }
  async getSongs(){
    return this.songsProvider.getListOfSongs(this.sortBy).then((value)=>{
      if(value && value.length>0){
        setTimeout(async ()=>{
          this.songs = value.slice();
		  this.songsBackup = value.slice();
		  if(!this.params) this.params= await this.paramsProvider.getParams();
		  if(this.params.search && this.params.search=='lyrics'){
			this.songsBackup=value.map((item)=>{
				let string="";	//concat lyrics in texteConcat			
				for(let lang in item.text_parts){
					for(let verseId in item.text_parts[lang]){
						for(let verse of item.text_parts[lang][verseId].lines) {
							string+=verse.replace(/\[([^\[\]]+)\]/g,"")+" ";//concat texte sans accords
						}
					}
				}
				item.texteConcat = string;
				return item;
			})
		  }
		  else this.songsBackup = value.slice();
        });
      }
      else{
      }
      return value;
    })    
  }
  goToLogin(){
    this.navCtrl.setRoot("LoginPage");
  }
  goToSong(song) {
    if(!this.isMultipleSelection) {
      this.navCtrl.push("SongPage", {
        id: song.orgKey,
        order:this.sortBy
      });
    }
    else this.selectSong(song);
  }
  showSearchBox(input){
    this.showSearch = true;
    setTimeout(()=>{
      //input.setFocus();
      this.searchbar.setFocus();
      this.keyboard.show();
    },150)
  }
  filterItems(ev){
    let accent = [
      /[\340-\346]/g, // a
      /[\350-\353]/g, // e
      /[\354-\357]/g, // i
      /[\362-\370]/g, // o
      /[\371-\374]/g, // u
      /[ç]/g, // u
      /[,]/g,
      /['’]/g
    ];
    let noaccent = ['a','e','i','o','u','c','',''];
    let filterAccents = function(title:string){
      for(let i = 0; i < accent.length; i++){
        title = title.replace(accent[i], noaccent[i]);
      }
      return title;
	}
	let checkTranslations = function(searchString:string,trads:any){
		for(let index in trads){
			if (compare(searchString,trads[index])) return true;
		}
		return false;
	}
    let compare = function(searchString:string, string2:string){
      searchString = filterAccents(searchString.toLowerCase());
      string2 = filterAccents(string2.toLowerCase());
      let isInString:boolean = true;
      let wordArray = searchString.split(' ');
      for(let word in wordArray){
		  if(string2.indexOf(wordArray[word])<0) isInString=false;
      }
      return isInString;
    }
// Reset items back to all of the items
    this.songs = this.songsBackup.slice();
    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.tags=val;
      this.songs = this.songs.filter((item) => {
        if(compare(val, item.title)) return true;
        else if(item.alternate_title && compare(val, item.alternate_title)) return true;
		else if(item.i18n && checkTranslations(val,item.i18n)) return true;
		else if(this.params.search=='lyrics' && item.texteConcat) {
			if (compare(val,item.texteConcat)) return true;
		}
        else return false;
      })       
    }
    else this.tags=null;
  }   
  onCancel(ev){
    this.showSearch=false; this.tags=null;
  }
  
  presentModal() {
    let modal = this.modalCtrl.create("TagsSearchPage",{},{cssClass:this.params.darkMode?'dark':''});
    modal.present();
    modal.onDidDismiss(
      (data)=>{
        this.songs = this.songsBackup.slice();
        if(data) {
          this.tags = data;
          this.songs = this.songs.filter((item) => {
            return (item.themes && item.themes.indexOf(data) > -1);
          })
        }
        else this.tags=null;
      }
    );
  }

  resetTags(){
	this.tags=null;
	this.searchbar.value="";
    this.songs = this.songsBackup.slice();
  }
//   selectOrder(){
//     let popup = this.alertCtrl.create({
//       "title":this.translate('sort_by',"Trier par"),
//       "inputs":[
//         {type:"radio", label:this.translate('titre',"Titre"), checked:this.sortBy=="alpha", value:"alpha", handler:
//           (data)=>{
//             if(this.tags) {
//               this.songs = this.songsBackup.filter((item) => {
//                 return (item.themes && item.themes.indexOf(this.tags) > -1);
//               })
//               this.sortBy = data.value;popup.dismiss(); 
//             }
//             else {
//               this.songs = this.songsBackup.slice();
//               this.sortBy = data.value;popup.dismiss();  
//             }        
//           }
//         },
//         {type:"radio", label:this.translate('nouveautes',"Nouveautés"), checked:this.sortBy=="date", value:"date", handler:
//           (data)=>{
//             this.songs = this.byDate.transform(this.songs, this.carnet);
//             this.sortBy = data.value;popup.dismiss();
//           }
          
//         },
//         {type:"radio", label:this.translate('page',"Page"), checked:this.sortBy=="page", value:"page", handler:
//           (data)=>{
//             this.songs = this.byPage.transform(this.songs, this.carnet);
//             this.sortBy = data.value;popup.dismiss();
//           }
          
//         }
// 	  ],
// 	  cssClass:this.params.darkMode?'dark':''
//     });
//     popup.present();
    
//   }
  selectLang(){
	  let popup = this.alertCtrl.create({
		  title:this.translate('filter_by_lang',"Filtrer par langue"),
		  inputs:[
			{type:"radio", label:this.translate('all',"Tous"), checked:this.lang=="Tous", value:"all", handler:
				(data)=>{
				this.songs = this.songsBackup.slice();
				this.lang = data.value;popup.dismiss();
				}
			},
			{type:"radio", label:"Français", checked:this.lang=="FR", value:"FR", handler:
				(data)=>{
					this.lang = data.value;
					this.songs = this.byLang.transform(this.songs,this.lang,this.carnet);
					popup.dismiss();
				}
			},
			{type:"radio", label:"English", checked:this.lang=="EN", value:"EN", handler:
				(data)=>{
					this.lang = data.value;
					this.songs = this.byLang.transform(this.songs,this.lang,this.carnet);
					popup.dismiss();
				}
			},	
			{type:"radio", label:"Deutsch", checked:this.lang=="DE", value:"DE", handler:
				(data)=>{
					this.lang = data.value;
					this.songs = this.byLang.transform(this.songs,this.lang,this.carnet);
					popup.dismiss();
				}
			},
			{type:"radio", label:"Español", checked:this.lang=="ES", value:"ES", handler:
				(data)=>{
					this.lang = data.value;
					this.songs = this.byLang.transform(this.songs,this.lang,this.carnet);
					popup.dismiss();
				}
			},
			{type:"radio", label:"Português", checked:this.lang=="PT", value:"PT", handler:
				(data)=>{
					this.lang = data.value;
					this.songs = this.byLang.transform(this.songs,this.lang,this.carnet);
					popup.dismiss();
				}
			},			  
		  ],
		  cssClass:this.params.darkMode?'dark':''
		});
	  popup.present();

  }
  openFilters(){
	let modal = this.modalCtrl.create("FiltersPage",this.filterOptions,{});
    modal.present();
    modal.onDidDismiss(async (data)=>{
		console.log(data);
		if(data && data.options){
			if(data.reloadSongs) {
				this.getSongs(); // if change of songbooks
				let newParams = await this.paramsProvider.getParams();
				this.carnet= newParams.carnet;
			}
			if(data.options.sortBy){
			    this.songs = this.songsBackup.slice();
				this.songs = (this.orderFilters[data.options.sortBy]).transform(this.songs, this.carnet).slice();
				if(data.options.lang && data.options.lang != 'Tous') {
					this.songs = this.byLang.transform(this.songs,data.options.lang,this.carnet);
				}
			}
		}
	}); 
  }
  filterByPage(){
    this.songs = this.songsBackup.slice();
    if (this.searchPage && this.searchPage >0) {
      this.songs = this.songs.filter((item) => {
        return (item.songbooks && item.songbooks[this.carnet] && item.songbooks[this.carnet].page== this.searchPage)
      })       
    }
  }
  isSelected(song){
    return (this.selectedSongs.indexOf(song)>-1);
  }
  multipleSelection(ev,song){
    this.selectedSongs = [song];
    this.isMultipleSelection=true;
  }
  selectSong(song){
    if(this.isSelected(song)) {
      this.selectedSongs.splice(this.selectedSongs.indexOf(song),1);
    }
    else{
      this.selectedSongs.push(song);
    }
  }
  
  backToList(){
    this.isMultipleSelection=false;
    this.selectedSongs=[];
  }

  presentActionSheet() {
    
    let actionSheet = this.actionsheetCtrl.create({
      title: this.translate('add_to_playlist','Ajouter à la playlist'),
      buttons: [
        {
          text: this.translate('cancel','Annuler'),
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    this.favorsProvider.getLocalPlaylists().then(result=>{
      this.playlists = result;
      for(let i in this.playlists){
        //ajouter les playlists, sauf playlist locale partagée
        if(!this.playlists[i].isShared) {
          actionSheet.addButton(
            {'text':this.playlists[i].title, 
              'handler': () => {
              this.addToPlaylist(i, this.playlists[i].title,false);
              }
            }
          );
        }
      }
      this.favorsProvider.getSharedPlaylists().then(result=>{
        let sharedPlaylists =result;console.log(sharedPlaylists);
        for(let i in sharedPlaylists){
          //add to available playlists if own playlist or has rights
          if(sharedPlaylists[i].canEdit || sharedPlaylists[i].createUserId == this.user.userId) actionSheet.addButton(
            {'text':sharedPlaylists[i].title, 
              'handler': () => {
              this.addToPlaylist(sharedPlaylists[i].id, sharedPlaylists[i].title,true);
              }
            }
          );
        }
        actionSheet.addButton(
          {
            text:this.translate('create_playlist','Créer une playlist'),
            icon:'add',
            handler:()=>{
              this.analyticsProvider.clickEvent("Création de playlist");
              let prompt = this.alertCtrl.create({
                title: this.translate('add_playlist','Nouvelle playlist'),
                //message: "Enter a name for this new album you're so keen on adding",
                inputs: [
                  {
                    name: 'title',
                    placeholder: this.translate('titre','Titre')
                  },
                ],
                buttons: [
                  {
                    text: this.translate('cancel','Annuler'),
                    handler: data => {
                    }
                  },
                  {
                    text: this.translate('create','Créer'),
                    handler: data => {
                      if(data.title == "") return false;
                      else {
                        this.favorsProvider.createPlaylist(data.title,[],0).then(()=>{
                          actionSheet.dismiss();
                          this.presentActionSheet()
                        });
                        
                      }
                    }
                  }
                ]
              });
              prompt.present();
            }
          }
        )
      })
      
    });
    actionSheet.present();
  }
  addToPlaylist(index, playlistTitle, isShared:boolean){
    this.favorsProvider.addSongsToPlaylist(this.selectedSongs, index, isShared).then(result=>{
      if(result.success){
        
		this.isMultipleSelection=false;
		this.selectedSongs=[];
		let toast = this.toastCtrl.create({
		message: this.translate('added_to_playlist','Ajoutés à la playlist')+' '+playlistTitle,
		duration: 2000
		});
		toast.present();
		this.selectedSongs.forEach(song => {
		this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
		})
      }
      else {
        let toast = this.toastCtrl.create({
          message: "Erreur : " + result.message,
          duration: 3000
        });
        toast.present();
        this.selectedSongs.forEach(song => {
          this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
        })
      }
    });
  }
  	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	goToPlaylist(playlistId:string){
		//pour une playlist partagée dans un message d'accueil
		
		this.afs.collection("playlists", ref=>ref.where('shareCode','==',playlistId)).valueChanges().subscribe(playlists=>{
			if(playlists && playlists.length>0){
				let playlist:any = playlists[0];
				this.navCtrl.push("PlaylistPage", {
					id: playlist.sharedKey?playlist.sharedKey:playlist.id,
					isShared:true,
					canEdit:false
				  });
			}
			else {
				const toast = this.toastCtrl.create({
					message: 'Erreur inconnue',
					duration: 2000
				  });
				  toast.present();
			}
		})


	}

}
