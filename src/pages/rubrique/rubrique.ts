import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController , ActionSheetController , ToastController} from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { OrderByDatePipe } from '../../pipes/order-by-date/order-by-date'
import { OrderByPagePipe } from '../../pipes/order-by-page/order-by-page'
import { ParamsProvider } from '../../providers/params/params';
import { FavorsProvider } from '../../providers/favors/favors';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the RubriquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rubrique',
  templateUrl: 'rubrique.html',
})
export class RubriquePage {
  title:string;
  rubriqueId:string;
  songs:any[];songsBackup:any[];
  sortBy:string = "alpha";
  byDate :OrderByDatePipe;
  byPage :OrderByPagePipe;
  carnet:string="APPLI_FR";
  playlists:any[];
  selectedSongs:any[] = [];
  isMultipleSelection:boolean=false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public songsProvider:SongsProvider, 
    private paramsProvider:ParamsProvider,
    private favorsProvider:FavorsProvider,
    public alertCtrl: AlertController,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
	private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
  ) 
  {
    this.byDate = new OrderByDatePipe();
    this.byPage = new OrderByPagePipe();
    this.title = navParams.get('title');
    this.rubriqueId = navParams.get('rubriqueId');

    if(this.rubriqueId=='PSAUMES' || this.rubriqueId=='ORDINAIRES DE LA MESSE'){
      this.sortBy="page";
    }
    this.songsProvider.getRubrique(this.rubriqueId, this.sortBy).then((value)=>{
      this.songsBackup = value.slice();
      this.songs = value.slice();       
    });
    this.paramsProvider.getParams().then(params=>{
      if(params) {this.carnet=params.carnet;}
    })
  }

  ionViewDidLoad() {
  }
  goToSong(event, song) {
    if(!this.isMultipleSelection) {
      this.navCtrl.push("SongPage", {
        id: song.orgKey,
        rubrique:this.rubriqueId,
        order:this.sortBy
      });
    }
    else this.selectSong(song);
  }
  selectOrder(){
    console.log(this.sortBy);
    let popup = this.alertCtrl.create({
      "title":this.translate('sort_by',"Trier par"),
      "inputs":[
        {type:"radio", label:this.translate('titre',"Titre"), checked:this.sortBy=="alpha", value:"alpha", handler:
          (data)=>{
            this.songs = this.songsBackup.slice() ; 
            this.sortBy = data.value;popup.dismiss()
          }
        },
        {type:"radio", label:this.translate('nouveautes',"Nouveautés"), checked:this.sortBy=="date", value:"date", handler:
          (data)=>{
            this.songs = this.byDate.transform(this.songs, this.carnet) ;
            this.sortBy = data.value;popup.dismiss()
          }
        },
        {type:"radio", label:this.translate('page',"Page"), checked:this.sortBy=="page", value:"page", handler:
          (data)=>{
            this.songs = this.byPage.transform(this.songs, this.carnet) ;
            this.sortBy = data.value;popup.dismiss()
          }
        }
      ],
      //"buttons":[{text:"OK", handler: data=>{this.sortBy = data; }}]
    });
    //popup.
    popup.present();
    
  }
  isSelected(song){
    return (this.selectedSongs.indexOf(song)>-1);
  }
  multipleSelection(ev,song){
    this.selectedSongs = [song];
    //song.isSelected=true;
    this.isMultipleSelection=true;
  }
  selectSong(song){
    if(this.isSelected(song)) {
      this.selectedSongs.splice(this.selectedSongs.indexOf(song),1);
    }
    else{
      this.selectedSongs.push(song);
    }
  }
  
  backToList(){
    this.isMultipleSelection=false;
    this.selectedSongs=[];
  }

  presentActionSheet() {
    
    let actionSheet = this.actionsheetCtrl.create({
      title: this.translate('add_to_playlist','Ajouter à la playlist'),
      buttons: [
        {
          text: this.translate('cancel','Annuler'),
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    this.favorsProvider.getLocalPlaylists().then(result=>{
      this.playlists = result;
      for(let i in this.playlists){
        //ajouter les playlists, sauf playlist locale partagée
        if(!this.playlists[i].isShared) {
          actionSheet.addButton(
            {'text':this.playlists[i].title, 
              'handler': () => {
              this.addToPlaylist(i, this.playlists[i].title,false);
              }
            }
          );
        }
      }
      this.favorsProvider.getSharedPlaylists().then(result=>{
        let sharedPlaylists =result;
        for(let i in sharedPlaylists){
          //ajouter les playlists
          if(sharedPlaylists[i].canEdit) actionSheet.addButton(
            {'text':sharedPlaylists[i].title, 
              'handler': () => {
              this.addToPlaylist(sharedPlaylists[i].id, sharedPlaylists[i].title,true);
              }
            }
          );
        }
        actionSheet.addButton(
          {
            text:this.translate('create_playlist','Créer une playlist'),
            icon:'add',
            handler:()=>{
              this.analyticsProvider.clickEvent("Création de playlist"); 
              let prompt = this.alertCtrl.create({
                title: this.translate('add_playlist','Nouvelle playlist'),
                //message: "Enter a name for this new album you're so keen on adding",
                inputs: [
                  {
                    name: 'title',
                    placeholder: this.translate('titre','Titre')
                  },
                ],
                buttons: [
                  {
                    text: this.translate('cancel','Annuler'),
                    handler: data => {
                    }
                  },
                  {
                    text: this.translate('create','Créer'),
                    handler: data => {
                      if(data.title == "") return false;
                      else {
                        this.favorsProvider.createPlaylist(data.title,[],0).then(()=>{
                          actionSheet.dismiss();
                          this.presentActionSheet()
                        });
                        
                      }
                    }
                  }
                ]
              });
              prompt.present();
            }
          }
        )
      })
      
    });
    actionSheet.present();
  }
  addToPlaylist(index, playlistTitle, isShared:boolean){
    this.favorsProvider.addSongsToPlaylist(this.selectedSongs, index, isShared).then(result=>{
      if(result.success){
        let toast = this.toastCtrl.create({
          message: this.translate('added_to_playlist','Ajoutés à la playlist')+' '+playlistTitle,
          duration: 2000
        });
        this.isMultipleSelection=false;
        this.selectedSongs=[];
        toast.present();
        this.selectedSongs.forEach(song => {
          this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
        })
        
      }
      else {
        let toast = this.toastCtrl.create({
          message: "Erreur : " + result.message,
          duration: 3000
        });
        toast.present();
        this.selectedSongs.forEach(song => {
          this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
        })
      }
    });
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
}
