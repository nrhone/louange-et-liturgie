import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPopupPage } from './login-popup';

@NgModule({
  declarations: [
    LoginPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPopupPage),
  ],
})
export class LoginPopupPageModule {}
