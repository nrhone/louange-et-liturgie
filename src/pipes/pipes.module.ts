import { NgModule } from '@angular/core';
import { KeysPipe } from './keys/keys';
import { ChordPipe } from './chord/chord';
import { OrderByDatePipe } from './order-by-date/order-by-date';
import { OrderByPagePipe } from './order-by-page/order-by-page';
import { SafePipe } from './safe/safe';
import { TranslatePipe } from './translate/translate';
import { OriginalTitlePipe } from './original-title/original-title';
import { FilterByLangPipe } from './filter-by-lang/filter-by-lang';
import { OrderByAlphaPipe } from './order-by-alpha/order-by-alpha.pipe';

@NgModule({
	declarations: [KeysPipe,
    ChordPipe,
    OrderByDatePipe,
	OrderByPagePipe,
	OrderByAlphaPipe,
    SafePipe,
    TranslatePipe,
    OriginalTitlePipe,
    FilterByLangPipe,
    ],
	imports: [],
	exports: [KeysPipe,
    ChordPipe,
    OrderByDatePipe,
	OrderByPagePipe,
	OrderByAlphaPipe,
    SafePipe,
    TranslatePipe,
    OriginalTitlePipe,
    FilterByLangPipe]
})
export class PipesModule {}
