import { Component , EventEmitter , Input, Output } from '@angular/core';
import { ModalController} from 'ionic-angular';
import firebase from 'firebase';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TranslateProvider } from '../../providers/translate/translate'
import { SongsProvider } from '../../providers/songs/songs';
/**
 * Generated class for the SongDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'song-detail',
  templateUrl: 'song-detail.html',
})
export class SongDetailComponent {
  @Input() song:any;
  @Input() options:any;
  @Input() isConnected:any;
  @Input() actualTonality:string;
  @Input() displayTonality:string;
	@Input() copyrights:any;
	@Input() defaultCarnet:string;
  @Input() scroll:boolean;
  @Input() autoscroll:boolean;
  @Input() isPlaylist:string;
  @Input() displayText:boolean;
  @Output() changeScreenMode = new EventEmitter<boolean>();
  text: string;
  storage:any;
  images:any;
  songbooks:any;
  trads:any;
  fullScreenMode:boolean=false;
  trad_labels:any={
    "FR":"Traduction française",
    "EN":"Traduction anglaise",
    "PL":"Traduction polonaise",
    "HU":"Traduction hongroise",
    "ES":"Traduction espagnole",
    "PT":"Traduction portugaise",
  }
  cpStates={reserved:"Droits réservés", permission:"Utilisé avec permission"};
  availableInBooks:any[]=[];
  constructor(
    private modalCtrl:ModalController,
    private iab:InAppBrowser,
	private translateProvider:TranslateProvider, 
	private songsService:SongsProvider
  ) {
	  this.storage = firebase.storage().ref();
	  
  }
  ngOnInit(){
	this.trads = this.getCpTrads();
	this.images={};
    if(this.song && this.song.images){
        for(let image of this.song.images){
            this.images[image.id]=image;
            this.storage.child(image.ref).getDownloadURL().then((url) => {       
              this.images[image.id].url=url;
              //check on images the one with the good ref TODO
            }).catch((error) => {
              // Handle any errors
            });
        }
	}
  }
  async ngAfterViewInit(){
	//this.songbooks = this.getCarnet(this.song.songbooks);
	let available = await this.songsService.getAvailableCarnets();
	setTimeout(()=>{
		for(let book in this.song.songbooks){
			if(available[book]) this.availableInBooks.push({id:book, title:available[book].title})
		}
	})
	if(this.isPlaylist=='false') {this.song.carnet = this.defaultCarnet; }
	else if(!this.song.carnet) this.song.carnet = this.defaultCarnet;
	if(this.song.carnet!="") this.songbooks = this.getCarnet(this.song.songbooks);
}
ngOnChanges(changes) {
	if(changes.autoscroll) this.songbooks = this.getCarnet(this.song.songbooks);
}
getCarnet(songbooks) {
	if(songbooks && songbooks[this.song.carnet]) {
	  //get scenario of default carnet
	  if(this.autoscroll && songbooks[this.song.carnet].scenario2) return songbooks[this.song.carnet].scenario2;
	  else return songbooks[this.song.carnet].scenario;
  	}
  else if (songbooks && songbooks['local']) {
	  if(songbooks['local'].scenario) return songbooks['local'].scenario;
	  else return songbooks['local'];
  }
  else {
	  let scenario = [];
	  for(let book in songbooks) {
		  if(this.autoscroll && songbooks[book] && songbooks[book].scenario2) {
			  scenario = songbooks[book].scenario2;
			  this.song.carnet = book;
			  break;
		  }
		  else if(songbooks[book] && songbooks[book].scenario) {
			  scenario = songbooks[book].scenario; 					
			  this.song.carnet = book;
			  break;
		  }
	  }
	  return scenario;
  }
}
	changeCarnet(){
		if(this.song.carnet != "") this.songbooks = this.getCarnet(this.song.songbooks);
	}
	getCpTrads(){
		let traductions = [];
		if(this.song.traductions) {
			for(let key of Object.keys(this.song.traductions)){
			if(this.song.songbooks[this.song.carnet] && this.song.songbooks[this.song.carnet].scenario && this.song.songbooks[this.song.carnet].scenario[key] ){
				traductions.push({lang:key, data:this.song.traductions[key]})
			}
			}
		}
		return traductions;
	}
  verseType(verseId){
    return verseId.charAt(0);
  }
  verseText = function(verse){
    let string:string="";
    switch(verse.charAt(0)) {
        case "R":
            string="R";
            break;
        case "C":case "V":
            string=verse.slice(1,4);
            break;
        case "B":
            string="Bridge";
            break;
        case "P":
            string="Pré-ref";
            break;
        case "I":
            string="Intro";
            break;
        case "T":
            string="Inter";
            break;
        case "E":
            string="Fin";
            break;
        case "D":
            string="Coda";
            break;
        case "G":
            string="Contrechant";
            break;
        case "S":
            string="Stance";
            break;
        case "Z":
            string="Traduction";
            break;
        default:
            string=verse.slice(1,4);
    }
    return string;
  }
  expand(images, order){
    let modal = this.modalCtrl.create("ImagePage", { images: images, order:order });
    modal.present();
  }
  fullScreen(event, order){
	  if(!this.scroll){//si on est en train de scroll, le clic arrête le scroll seulement
		var target = event.target || event.srcElement || event.currentTarget;
		var idAttr = target.attributes.id;
		if(idAttr && idAttr.nodeValue=="image" && !this.fullScreenMode) this.expand(this.images, order);
		else {
			this.fullScreenMode=!this.fullScreenMode;
			this.changeScreenMode.emit(this.fullScreenMode);
		}
	  }
    
  }
  openLtc(){
    if(this.song.id_ltc) this.iab.create('http://www.ltc-asaph.com/script/song-search-page?q='+this.song.id_ltc, "_system", "location=true");

  }
  	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	typeof(input){
		return typeof(input);
	}
}
