import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController} from 'ionic-angular';
import { TranslateProvider } from '../../providers/translate/translate';
import { MessagesProvider } from '../../providers/messages/messages';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { AngularFirestore } from 'angularfire2/firestore';
import { FavorsProvider } from '../../providers/favors/favors';
import { ParamsProvider } from '../../providers/params/params'

/**
 * Generated class for the InboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {
	messages:any[];params:any;
  constructor(
	  public navCtrl: NavController, 
	  public navParams: NavParams,
	  private translateProvider:TranslateProvider,
	  private alertCtrl:AlertController,
	  private messagesProvider:MessagesProvider,
	  private analyticsProvider:AnalyticsProvider,
	  private afs:AngularFirestore,
	  private favorsProvider:FavorsProvider,
	  private paramsProvider:ParamsProvider
	  ) {
		this.messagesProvider.getMessages().then(res=>{
			this.messages = this.orderByDate(res.messages);
		});
		this.paramsProvider.getParams().then(res=>this.params=res)
	}

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Messages");
  }
	openMessage(id){
		let message = this.messages[id];
		this.analyticsProvider.clickEvent("open_message",message.title);
		let alert = this.alertCtrl.create({
			title:message.title,
			subTitle:message.body, 
			buttons : [
				{
					text:this.translate('close','Fermer'),
					role:'cancel'
				}
			],
			cssClass:this.params.darkMode?'dark':''
		});
		if(message.playlistId) alert.addButton({
			text:this.translate('download','Télécharger'),
			handler: () => {
				this.copyPlaylist(message.playlistId);
			}
		})
		this.messagesProvider.changeReadStatus(message.id).then(res=>this.messages = this.orderByDate(res.messages));
		return alert.present();
	}
	deleteMessage(id){
		this.analyticsProvider.clickEvent("delete_message");
		this.messagesProvider.deleteMessage(id).then(res=>this.messages = this.orderByDate(res.messages));
	}
	orderByDate(array){
		return array.map((item, index)=>{
			item.id = index;
			return item
		}).sort((a: any, b: any) => {
			if(a.date>b.date) return -1;
			else return 1;
		});
	}
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	async copyPlaylist(playlistId){
		this.afs.collection("globalPlaylists").doc(playlistId).valueChanges().subscribe(res=>{
			let playlist = res;
			this.analyticsProvider.clickEvent("download_playlist",playlist['title']);
			let songs=[];
			if(playlist['songs']) {
				playlist['songs'].forEach(song=>{
					songs.push({title:song.title, orgKey:song.orgKey, i18n:song.i18n?song.i18n:{}, songbooks:song.songbooks?song.songbooks:{}});
				})
			}
			this.favorsProvider.createPlaylist(playlist['title'], songs, 0).then(res=>{
				this.navCtrl.push("PlaylistsPage");
			});
	  });
	}
}
