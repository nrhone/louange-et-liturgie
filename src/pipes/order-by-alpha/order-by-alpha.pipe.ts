import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderByAlpha'
})
export class OrderByAlphaPipe implements PipeTransform {

  	transform(array: any[], carnet:string): any[] {
		let accent = [
			/[\340-\346]/g, // a
			/[\350-\353]/g, // e
			/[\354-\357]/g, // i
			/[\362-\370]/g, // o
			/[\371-\374]/g, // u
		];
		let noaccent = ['a','e','i','o','u'];
		let lang = "FR";
		lang = carnet.split("_")[1];
		if(lang=="BR") lang = "PT";
		else if(lang=="IL") lang="HE";

		// temporary array holds objects with position and sort-value
		let mapped = array.map(function(el, i) {
		  	let title = ""; 
		  	if(el.i18n && el.i18n[lang] && el.i18n[lang]!="") title = el.i18n[lang].toLowerCase();
		  	else if(el.title) title=el.title.toLowerCase();
			for(let i = 0; i < accent.length; i++){
				title = title.replace(accent[i], noaccent[i]);
			}
		  	return { index: i, value: title};
		})
		// sorting the mapped array containing the reduced values
		mapped.sort(function(a, b) {
			if (a.value > b.value) {
				return 1;
			}
			if (a.value < b.value) {
				return -1;
			}
			else return 0;
		});
		// container for the resulting order
		let result = mapped.map(function(el){
		  return array[el.index];
		});
		return result;
  	}

}
