import { Component, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController} from 'ionic-angular';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the CapoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {
  params: any;
  song:any;
  isLoggedIn:boolean=false;
  tonality:string="";
  emitter: EventEmitter < any >;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams,	private translateProvider:TranslateProvider
	) {  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OptionsPage');
  }
  ionViewDidEnter(){
    this.params = this.navParams.data.options;
	this.song = this.navParams.data.song;
	this.tonality = this.navParams.data.tonality;console.log(this.tonality);
	this.emitter = this.navParams.data.emitter;
    if(!this.song.capo) this.song.capo=0;
	if(!this.song.transposition) this.song.transposition=0;
    if(this.navParams.data.isLoggedIn) this.isLoggedIn=true;
  }
  close(){
    this.viewCtrl.dismiss(this.params);
  }
  
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	getTonality(song){
		let transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"];
		//get tonality to display chords
		let isMinor = false;
		//transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"]
		let tonality = song.tonality.replace(/[m]/,(match, minor)=>{
			if(minor) isMinor = true;
			return "";
		})
		let oldIndex = transposeOrder.indexOf(tonality);
		this.tonality = transposeOrder[(oldIndex+this.song.transposition+12)%12]; if(isMinor) this.tonality+="m";
	}
	changeTonality(){
		this.emitter.emit();
		this.getTonality(this.song);
	}
	public changeCapo(){
		this.emitter.emit();
	}
	
}
