export interface SongMeta {
	title: string;
	orgKey: string;
	copyright:number,
	packages:Array<string>,
	themes:Array<string>,
	date:number,
	section:string,
	page:any,
	i18n:any
	/*public_repos: number;
	public_gists: number;
	followers: number;
	following: number;*/
  }