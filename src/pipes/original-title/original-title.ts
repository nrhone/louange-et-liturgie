import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the OriginalTitlePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'originalTitle',
})
export class OriginalTitlePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  lang:string="FR";
  
  transform(value: string, trads:any, carnet:string) {
	if(carnet){
		this.lang = carnet.split("_")[1];
		//si le titre est traduit -> renvoyer titre original
		if (trads && trads[this.lang] && trads[this.lang]!="") return value;
		//sinon pas de titre original
		else return null;
	}
	else return null;
  }
}
