import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ParamsProvider } from '../../providers/params/params';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { SongsProvider } from '../../providers/songs/songs';
import { TranslateProvider } from '../../providers/translate/translate'
import { AppRate } from '@ionic-native/app-rate';
import { AnalyticsProvider } from '../../providers/analytics/analytics'

/**
 * Generated class for the ParamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-params',
  templateUrl: 'params.html',
})
export class ParamsPage {
  params:any;
  user:any;
  allowChord:boolean=false;
//   availableBooks = [
//     {id:"APPLI_FR",label:"FRANCE"}
//   ];
  availableBooks:any[] = [];
  selectOptions:any;
  constructor(
    public auth:AuthServiceProvider, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private paramsProvider:ParamsProvider,
    private alertCtrl : AlertController,
	private songsProvider:SongsProvider,
	private translateProvider:TranslateProvider,
	private rateCtrl: AppRate,
	private analyticsProvider:AnalyticsProvider
  ) {
    this.paramsProvider.getParams().then(params=>{
      if(params) {this.params=params; }
      else {
        this.params = {showChords:true, capoMode:false, position:false, fontSize:16, veille:10, carnet:"APPLI_FR",lang:"FR",autoscroll:false};
        this.paramsProvider.updateParams(this.params);  
	  }
	  this.selectOptions = {	  cssClass:this.params.darkMode?'dark':''	}
      //this.params.carnet=null;
	});
	this.songsProvider.getAvailableCarnets().then(books=>{
		setTimeout(()=>{
			for(let book in books) {
				this.availableBooks.push({id:book, label:books[book].title})
			}
		},200)
		
	})
    this.auth.getUserInfo().then(res=>{
      if(res && res != null) {
        this.user=res; this.allowChord=true;

      }
    });
  }

  ionViewDidLoad() {
	this.analyticsProvider.pageEvent("Paramètres");
  }

  updateParams(){
	this.selectOptions = {cssClass:this.params.darkMode?'dark':''}
	this.paramsProvider.updateParams(this.params).then(
      params=>{
      });
  }
  updateLang(){
	this.paramsProvider.updateParams(this.params);
	this.translateProvider.updateLang(this.params.lang);
  }
  updateSongBook(){  
   this.paramsProvider.updateParams(this.params).then(
      params=>{
        this.songsProvider.changeCarnet(this.params.carnet);
      });
  }
  showVeille(){
    let popup = this.alertCtrl.create({
      "title":this.translate('insomnia',"Mise en veille de l'écran"),
      "inputs":[
        {type:"radio", label:"1 minute", checked:this.params.veille==1, value:'1', handler:
          (data)=>{
            this.params.veille=1;this.updateParams();popup.dismiss(); 
          }
        },
        {type:"radio", label:"5 minutes", checked:this.params.veille==5, value:'5', handler:
          (data)=>{
            this.params.veille=5;this.updateParams();popup.dismiss(); 
          }
        },
        {type:"radio", label:"10 minutes", checked:this.params.veille==10, value:'10', handler:
          (data)=>{
            this.params.veille=10;this.updateParams();popup.dismiss(); 
          }
        },
        {type:"radio", label:"30 minutes", checked:this.params.veille==30, value:'30', handler:
          (data)=>{
            this.params.veille=30;this.updateParams();popup.dismiss(); 
          }
        },
        {type:"radio", label:this.translate('never',"Jamais"), checked:this.params.veille==1000, value:'1000', handler:
        (data)=>{
          this.params.veille=1000;this.updateParams();popup.dismiss(); 
        }
      }
        
	  ],
	  cssClass:this.params.darkMode?'dark':''
      //"buttons":[{text:"OK", handler: data=>{this.sortBy = data; }}]
    });
    //popup.
    popup.present();
  }
   goToPage(page:string){
    this.navCtrl.push(page);
  }
  	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
	showRatePrompt(){
		this.rateCtrl.preferences = {
			simpleMode: true,
			useLanguage:'fr',
			storeAppURL : {
				ios: '1349978129',
				android: 'market://details?id=fr.chemin_neuf.chants'
			}
		};
	  
		return this.rateCtrl.promptForRating(true); 
	}
	booksFilter(books:any[]){
		return books.filter(book=>{return book.id != this.params.carnet})
	}
}
