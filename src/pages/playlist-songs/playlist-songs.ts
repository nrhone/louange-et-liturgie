import { Component, ViewChild , ViewChildren , QueryList, EventEmitter } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Slides ,PopoverController , ActionSheetController , ToastController , AlertController , ModalController , LoadingController} from 'ionic-angular';

import { SongsProvider } from '../../providers/songs/songs';
import { ParamsProvider } from '../../providers/params/params';
import { NotesProvider } from '../../providers/notes/notes';
import { AnalyticsProvider } from '../../providers/analytics/analytics';
import { FavorsProvider } from '../../providers/favors/favors';
import "froala-editor/js/froala_editor.pkgd.min.js";
import 'froala-editor/js/plugins.pkgd.min.js';

import firebase from 'firebase';
//declare var MIDI:any;
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { InAppBrowser } from '@ionic-native/in-app-browser';


/**
 * Generated class for the PlaylistSongsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playlist-songs',
  templateUrl: 'playlist-songs.html',
})
export class PlaylistSongsPage {
  @ViewChild('myslider') slider: Slides;
  @ViewChildren(Content) parents:QueryList<any>;
  //playlistId:string;
  activeIndex : number;
  playlist:any;
  options:any;
  playlists:any[];
  songs:any[];
  copyrights:any;
  hasExultet:boolean=false;
  hasYoutube:boolean=false;
  hasSpotify:boolean=false;
  hasNote:boolean=false;
  carnet:string="APPLI_FR";
  viewedTime:number;
  editMode=false;
  playNote:boolean=false;first_note:string;
  isConnected:boolean=false;
  cpStates={reserved:"Droits réservés", permission:"Utilisé avec permission"}
  fullScreenMode:boolean=false;
  public editorOptions: Object = {
    placeholderText: 'Ajouter une note',
    charCounterCount: false,
    toolbarButtonsXS: [['bold' ,'color', 'fontSize', 'insertLink'], ['undo', 'redo']],
	toolbarButtons: [['bold' ,'color', 'fontSize', 'insertLink'], ['undo', 'redo']],
	immediateAngularModelUpdate:true,
  };
  isScrolling:boolean=false;manualScroll:boolean=false;scrollTop:number=0;interval=null;showScroll:boolean=false;
  tonality:string=null;displayTonality:string=null; 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private songsProvider:SongsProvider,
    private paramsProvider:ParamsProvider,
    private notesProvider:NotesProvider,
    public popoverCtrl: PopoverController,
    public actionsheetCtrl: ActionSheetController,
    private favorsProvider:FavorsProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private modalCtrl: ModalController,
    public audioProvider:SmartAudioProvider,
    private analyticsProvider:AnalyticsProvider,
    private iab: InAppBrowser,
    private loadingCtrl:LoadingController
  ) 
  {
    this.options = {showChords:false, capoMode:false, fontSize:16, position:false};
    let initIndex = navParams.get('id');
    this.playlist = navParams.get('playlist');
    let start = 0; let end=0;
    if(initIndex<=2) {start=0; this.activeIndex=initIndex}
	else {start = initIndex-2; this.activeIndex=2}
    if(initIndex>this.playlist.songs.length -2) end=this.playlist.songs.length;
    else end = initIndex +2;
    this.songs=this.playlist.songs.slice(start,end);
	this.viewedTime=Date.now();
	let carnet = this.songs[this.activeIndex].carnet;
    if(!this.songs[this.activeIndex].text_parts) this.songsProvider.getSongById(this.songs[this.activeIndex].orgKey).then(song => {
		this.songs[this.activeIndex] = song;
		this.songs[this.activeIndex].carnet = carnet;
      	this.loadSong(this.activeIndex);
    });
    else {
		this.loadSong(this.activeIndex);
	}
    this.paramsProvider.getParams().then(params=>{
      	if(params) {
			this.options=params;
			if(!this.playlist.carnet) this.carnet=params.carnet;
			else this.carnet = this.playlist.carnet;

		}
    });
    this.songsProvider.getCopyrights().then(cp=>{this.copyrights = cp; });
    firebase.database().ref('.info/connected').on('value', snapshot => {
      // If we're not currently connected, don't do anything.
      if (snapshot.val() == false) {
        this.isConnected=false;
      }
      else this.isConnected=true;
    })
  }

  ionViewDidLoad() {
     
  }
  ionViewWillLeave(){
	if(this.isScrolling) this.stopScroll();
	if(this.viewedTime && (Date.now()-this.viewedTime > 30*1000)){
      this.analyticsProvider.songEvent('30s',this.songs[this.slider.getActiveIndex()]);
    }  
    if(this.viewedTime && (Date.now()-this.viewedTime > 90*1000)){
      this.analyticsProvider.songEvent('2mn',this.songs[this.slider.getActiveIndex()]);
    }  
  }
  
  nextSong(dir){
	if(this.isScrolling) this.stopScroll();
	let newIndex = this.slider.getActiveIndex();
	let index = this.playlist.songs.indexOf(this.songs[newIndex]);
    if(newIndex==this.songs.length && dir=='forward'){
    }
    else {
      if(this.viewedTime && (Date.now()-this.viewedTime > 30*1000)){
        this.analyticsProvider.songEvent('30s',this.songs[this.slider.getPreviousIndex()]);
      }    
      if(this.viewedTime && (Date.now()-this.viewedTime > 90*1000)){
        this.analyticsProvider.songEvent('2mn',this.songs[this.slider.getPreviousIndex()]);
      }  
      this.viewedTime=Date.now();
    }
    if(newIndex==1 && dir=='backward') {
      if(index==2) {
        this.songs = this.playlist.songs.slice(0,1).concat(this.songs); 
        this.slider.slideTo(2,0,false);
      }
      else if(index>2 && this.songs.length==3){
        this.songs = this.playlist.songs.slice(index-2, index-1).concat(this.songs);
        this.slider.slideTo(2,0,false);
      }
      else if(index>2 ){
        this.songs = this.playlist.songs.slice(index-3, index-1).concat(this.songs);
        this.slider.slideTo(3,0,false);
      }
    }
    else if(newIndex == this.songs.length-1 && dir=='forward' && index>-1) {
      this.songs = this.songs.concat(this.playlist.songs.slice(index+1, index+4)); 
    }
    if(newIndex==this.songs.length && dir=='forward'){
    }
    else if(this.slider){
		  newIndex = this.slider.getActiveIndex();
      	if(!isNaN(newIndex) && this.songs[newIndex]&&!this.songs[newIndex].text_parts && this.songs[newIndex].orgKey!='external_song') {
			let carnet = this.songs[newIndex].carnet;
			this.songsProvider.getSongById(this.songs[newIndex].orgKey).then(song => {
				this.songs[newIndex] = song;
				this.songs[newIndex].carnet = carnet;
				this.loadSong(newIndex);
			});
		}
      	else if(isNaN(newIndex)){
		}
		else {
			this.loadSong(newIndex);
		}
	}
    
   
  }
  loadSong(id){
	this.activeIndex =this.slider.getActiveIndex();
	if(isNaN(this.activeIndex)) {// fix error ... do not knwo why
		let initIndex = this.navParams.get('id');
		if(initIndex<=2) this.activeIndex = initIndex; 
		else this.activeIndex = 2;
	}
	if(this.songs[id] && !this.songs[id].showSong) {
		this.songs[id].showSong = true;
	}
	if(this.songs[id]){
		this.hasExultet = (this.songs[id] && this.songs[id].exultet)?true:false;
		this.hasSpotify = (this.songs[id] && this.songs[id].spotify)?true:false;
		this.hasYoutube = (this.songs[id] && this.songs[id].youtube)?true:false; 
		this.hasNote = (this.songs[id] && this.songs[id].first_note)?true:false;  
		if(this.hasNote) this.audioProvider.preload(this.songs[id].first_note,'assets/sounds/'+this.songs[id].first_note+'.mp3');
		if(this.songs[id].orgKey !="external_song") this.notesProvider.getNoteById(this.songs[id].orgKey).then((informations) => {
			if(informations) {
				if(informations.notes && informations.notes!="") this.songs[id].notes=informations.notes;
				else this.songs[id].notes="";
				this.songs[id].duration=informations.duration;
				this.songs[id].capo=informations.capo?informations.capo:0;
			}
			if(this.songs[id].tonality) {
				this.getTonality(this.songs[id]);
			}
		})
		if(this.songs){
			setTimeout(()=>{
				if(this.parents) {
					let content = this.parents.toArray()[id+1];
					content.resize();
					setTimeout(()=>{
						var height = content.getContentDimensions().scrollHeight;
						if(height>content.contentHeight*1.2) this.showScroll=true;
						else this.showScroll=false;
					},0);  
				}
			},500);
		}
	}

  }  

  verseType(verseId){
    return verseId.charAt(0);
  }
  verseText = function(verse){
    let string:string="";
    switch(verse.charAt(0)) {
        case "R":
            string="R";
            break;
        case "C":case "V":
            string=verse.slice(1,3);
            break;
        case "B":
            string="Bridge";
            break;
        case "P":
            string="Pré-ref";
            break;
        case "I":
            string="Intro";
            break;
        case "T":
            string="Inter";
            break;
        case "E":
            string="Fin";
            break;
        case "S":
            string="Stance";
            break;
        case "Z":
            string="Traduction";
            break;
        default:
            string=verse.slice(1,3);
    }
    return string;
  }

  async presentActionSheet() {
    let loader:any;
    if(this.isConnected) {
      loader = this.loadingCtrl.create({
        content: "Getting playlists...",
      });
      loader.present();
    }
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Ajouter à la playlist',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    this.playlists = await this.favorsProvider.getLocalPlaylists();
    for(let i in this.playlists){
      //ajouter les playlists, sauf playlist locale partagée
      if(!this.playlists[i].isShared) {
        actionSheet.addButton(
          {'text':this.playlists[i].title, 
            'handler': () => {
            this.addToPlaylist(i, this.playlists[i].title,false);
            }
          }
        );
      }
    }
    if(this.isConnected) {
	  let sharedPlaylists = await this.favorsProvider.getSharedPlaylists();
      for(let i in sharedPlaylists){
        //ajouter les playlists
        if(sharedPlaylists[i].canEdit) actionSheet.addButton(
          {'text':sharedPlaylists[i].title, 
            'handler': () => {
            this.addToPlaylist(i, sharedPlaylists[i].title,true);
            }
          }
        );
      }
    }
    actionSheet.addButton(
      {
        text:'Créer une playlist',
        icon:'add',
        handler:()=>{
          this.analyticsProvider.clickEvent("Création de playlist"); 
          let prompt = this.alertCtrl.create({
            title: 'Nouvelle playlist',
            //message: "Enter a name for this new album you're so keen on adding",
            inputs: [
              {
                name: 'title',
                placeholder: 'Titre'
              },
            ],
            buttons: [
              {
                text: 'Annuler',
                handler: data => {
                }
              },
              {
                text: 'Créer',
                handler: data => {
                  if(data.title == "") return false;
                  else {
                    this.favorsProvider.createPlaylist(data.title,[],0).then(()=>{
                      actionSheet.dismiss();
                      this.presentActionSheet()
                    });
                    
                  }
                }
              }
            ]
          });
          prompt.present();
        }
      });
   if(this.isConnected) loader.dismiss();
 
    actionSheet.present();
  }
  
  addToPlaylist(index, playlistTitle, isShared:boolean){
    let songIndex = this.slider.getActiveIndex();
    let song = this.songs[songIndex];
    this.favorsProvider.addSongToPlaylist(song, index, isShared).then(result=>{
      if(result.success){
        let toast = this.toastCtrl.create({
          message: 'Ajouté à la playlist '+playlistTitle,
          duration: 2000
        });
        toast.present();
        this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
      }
      else {
        let toast = this.toastCtrl.create({
          message: "Erreur : " + result.message,
          duration: 3000
        });
        toast.present();
        this.analyticsProvider.clickEvent("Ajout à une playlist", song.title);
      }
    });
  }




  openOptions(ev) {
	//this.isFavorite();
	let myEmitter = new EventEmitter< any >();
	myEmitter.subscribe(
		v=> {
			if(this.songs[index].tonality) this.getTonality(this.songs[index]);
		}
	);
    let index = this.slider.getActiveIndex();
    let popover = this.popoverCtrl.create("OptionsPage", {"options":this.options, "song":this.songs[index],"isLoggedIn":true,"tonality":this.tonality, emitter:myEmitter});
    popover.present({
      ev: ev,
    });
    let capo = this.songs[index].capo;
    popover.onDidDismiss((data)=>{
		if(this.songs[index].tonality) this.getTonality(this.songs[index]);
		if(capo != this.songs[index].capo) {
		this.notesProvider.updateCapoById(this.songs[index].orgKey, this.songs[index].capo);
      }
      this.paramsProvider.updateParams(this.options);  
    });
  }
  openSpotify(){

    let index = this.slider.getActiveIndex();
    let url = this.songs[index].spotify;
    this.analyticsProvider.clickEvent("Spotify", this.songs[index].title)
    let id = url.split("track/")[1];
    let popover = this.popoverCtrl.create("SpotifyPage",{id:id, type:"spotify"});
    popover.present({
	});
	popover.onDidDismiss(res=>{this.viewedTime=Date.now()})
  }
  openYoutube(){
    let index = this.slider.getActiveIndex();
    let url = this.songs[index].youtube;
    this.analyticsProvider.clickEvent("Youtube", this.songs[index].title)
    let id = url.split("watch?v=")[1];
    let popover = this.popoverCtrl.create("SpotifyPage",{id:id, type:"youtube"});
    popover.present({
    });
	popover.onDidDismiss(res=>{this.viewedTime=Date.now()})
}
  openUrl(){
    let index = this.slider.getActiveIndex();
    this.analyticsProvider.clickEvent("Exultet", this.songs[index].title)
    this.iab.create(this.songs[index].exultet, "_system", "location=true");
  }
  editNote(){
	  this.editMode = true;
  }
  saveNote(){
    let index = this.slider.getActiveIndex();
    if(this.editMode) {
      this.notesProvider.updateNoteById(this.songs[index].orgKey, this.songs[index].notes);
    }
    this.editMode = !this.editMode;
  }
  expand(images){
    let modal = this.modalCtrl.create("ImagePage", { images: images });
    modal.present();
  }
  play(){
    let songIndex = this.slider.getActiveIndex();
    if(this.songs[songIndex] && this.songs[songIndex].first_note){
      this.first_note=this.songs[songIndex].first_note.substr(0, this.songs[songIndex].first_note.length-1);
      this.playNote=true;
      this.audioProvider.play(this.songs[songIndex].first_note);
      setTimeout(() => {
        this.playNote=false;
       }, 1000);
    }
  }
  fullScreen(screenMode:boolean){
    this.fullScreenMode=screenMode;
  }
  setDuration(myEvent){
	let index = this.slider.getActiveIndex();
	let popover = this.popoverCtrl.create("DurationPage",{song:this.songs[index]});
	popover.onDidDismiss((data)=>{
		this.notesProvider.updateDurationById(this.songs[index].orgKey, this.songs[index].duration);
	});
	popover.present({     
		ev: myEvent
    });
  }
  autoScroll(ev, toggle:boolean){
	let index = this.slider.getActiveIndex();	
	let content = this.parents.toArray()[index+1];content.resize();
	this.scrollTop = content.getContentDimensions().scrollTop;
	var height = (content.getContentDimensions().scrollHeight-content.contentHeight);
	var duration = this.songs[index].duration*1000;
	let speed = height/duration; let intervalle = 10;
	if(!this.isScrolling || !toggle) {
		clearInterval(this.interval); this.isScrolling=false;
		this.interval=setInterval(()=>{
			this.scrollTop+=speed*intervalle;
			content.scrollTop=this.scrollTop;
			if(this.scrollTop>height) {
				clearInterval(this.interval); this.isScrolling=false;
			}
		}, intervalle);
		this.isScrolling=true;
	}
	else {
		clearInterval(this.interval); this.isScrolling=false;
	}
  }
  stopScroll(){
	  setTimeout(()=>{
		clearInterval(this.interval);this.isScrolling=false;
	  },50);
	 
  }
  slower(ev){
	let song = this.songs[this.slider.getActiveIndex()];
	if(song.duration<330) song.duration +=30;
	this.notesProvider.updateDurationById(song.orgKey, song.duration);
	this.autoScroll(ev,false);
	}
	faster(ev){
		let song = this.songs[this.slider.getActiveIndex()];
		if(song.duration>30) song.duration -=30;
		this.notesProvider.updateDurationById(song.orgKey, song.duration);
		this.autoScroll(ev,false);
	}
	startScroll(ev:any){
		if(this.isScrolling) {
			this.manualScroll=true;
			clearInterval(this.interval);
		}
	}
	endScroll(ev:any){
		if(this.isScrolling && this.manualScroll) {
			this.autoScroll(ev,false);
			this.manualScroll=false;
		}
	}
	getTonality(song){
		let transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"];
		//get tonality to display chords
		let isMinor = false;
		//transposeOrder = ["A","Bb","B","C","C#","D","Eb","E","F","F#","G","Ab"]
		let tonality = song.tonality.replace(/[m]/,(match, minor)=>{
			if(minor) isMinor = true;
			return "";
		})
		let oldIndex = transposeOrder.indexOf(tonality);
		let transpositionFactor = 0;
		if(!song.capo) song.capo=0;
		if(song.songbooks && song.songbooks[this.carnet] && (song.songbooks[this.carnet].capo || song.songbooks[this.carnet].capo==0 )) transpositionFactor += song.songbooks[this.carnet].capo;
		if(song.capo) transpositionFactor += song.capo;
		if(song.songbooks && song.songbooks[this.carnet] && song.songbooks[this.carnet].transposition) transpositionFactor -=  song.songbooks[this.carnet].transposition;
		if (song.transposition) transpositionFactor -= song.transposition;

		this.displayTonality = transposeOrder[(oldIndex-transpositionFactor+12)%12]; if(isMinor) this.displayTonality+="m";
		this.tonality = transposeOrder[(oldIndex-transpositionFactor+song.capo+12)%12]; if(isMinor) this.tonality+="m";
}

}
