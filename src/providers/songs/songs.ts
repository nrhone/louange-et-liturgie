//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ParamsProvider } from '../../providers/params/params';
//import { SongMeta } from '../../models/song-meta';
import { OrderByDatePipe } from '../../pipes/order-by-date/order-by-date';
import { OrderByPagePipe } from '../../pipes/order-by-page/order-by-page';
import { AngularFirestore } from 'angularfire2/firestore';
import { OrderByAlphaPipe } from '../../pipes/order-by-alpha/order-by-alpha.pipe';
//import { AngularFirestoreCollection } from 'angularfire2/firestore';

/*
  Generated class for the SongsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SongsProvider {
  public songsList:any[];// liste abbrégée des chants avec seulement métadonnées pour tri - triée par ordre alpha - filtrée par carnet
  public songsListDate:any[];// liste abbrégée des chants avec seulement métadonnées pour tri - triée par ordre alpha - filtrée par carnet
  public songsListPage:any[];// liste abbrégée des chants avec seulement métadonnées pour tri - triée par ordre alpha - filtrée par carnet
  public songs:any;//liste des chants complets
  public songbooks:any;//liste des songbooks
  public localSongs:any;//liste des chants locaux
 // public songsAlpha:Array<{groupName:string, songs:SongMeta[]}>;//liste abbrégée des chants avec seulement métadonnées pour tri - triée et groupée par ordre alpha
  private nbOfSlides : number = 2; // nb de slides autour d'un chant (avant et après)
  public themes:any;
  public copyrights:any;
  private user:any;
  public carnet:string = "APPLI_FR";
  public availableSbx:string[]=["APPLI_FR","2018_HU"];
  byDate :OrderByDatePipe;
  byPage :OrderByPagePipe;
  byAlpha :OrderByAlphaPipe;
  private songsCollection: any;
  private cpCollection: any;
  private themesCollection: any;
  private songbooksCollection: any;
  public fireDb: any;

  constructor(public storage: Storage, public auth:AuthServiceProvider, private paramsProvider:ParamsProvider,afs: AngularFirestore) {
    this.fireDb =  firebase.firestore();
    this.themesCollection = this.fireDb.collection("themes");
    this.songsCollection = this.fireDb.collection('songs');
    this.cpCollection = this.fireDb.collection("copyrights");
    this.songbooksCollection = this.fireDb.collection("songbooks");
    this.byDate = new OrderByDatePipe();
    this.byPage = new OrderByPagePipe();
    this.byAlpha = new OrderByAlphaPipe();
    this.paramsProvider.getParams().then(params=>{
	  if(params) {this.carnet=params.carnet; }
		//si première connexion, initialiser les paramètres
	  else {
		let params ={showChords:true, capoMode:false, position:false, fontSize:16, veille:10, carnet:"APPLI_FR"};
        this.carnet = params.carnet; this.paramsProvider.updateParams(params);
	  }
    }); 
  }
  initialize(){    
    return Observable.create(observer => {
      let getUserInfo = ()=> {
			return this.auth.getUserInfo().then(res=>{
				this.user=res;
				if(this.user && this.user.songbooks){
					for (let book of this.user.songbooks){
						//si user a des carnets non présents dans la liste officielle => les récupérer aussi
						if(this.availableSbx.indexOf(book)==-1){
							this.availableSbx.push(book);
						}
					}
				}
				observer.next(10);
				return true;
			});
      	}
		let getThemes = ()=>{
			return this.themesCollection.get().then(
			themesSnap => {
				var themes = [];
				themesSnap.forEach(theme=>{
				themes.push(theme.data());
				});
				observer.next(10);
				return this.storage.set("themes", themes);
			})
      	}
		let getCps = ()=>{
			return this.cpCollection.get().then(
			cps => {
				//let copyrights=new Array(cps.length);
				let copyrights={};
				cps.forEach(
				cp=>{
					copyrights[cp.id] = cp.data();
				}
				)
				observer.next(10);
				return this.storage.set("copyrights", copyrights);
			})
		}
    let getSongbooks = ()=>{
			let nbOfSongbooks = this.availableSbx.length;
			this.songbooks={};
			let songbookData = {};
			let getSongbookFn = async (book)=> {
				const res = await this.songbooksCollection.doc(book).get();
				songbookData[book]=res.data();
				this.songbooks[book]=res.data();
				this.songbooks[book].songs={};
				const songsSnap = await this.songbooksCollection.doc(book).collection("songs").get();
					songsSnap.forEach(song=>{
						this.songbooks[book].songs[song.id]=song.data();
					});
				observer.next(30/nbOfSongbooks);
			}
			return Promise.all(this.availableSbx.map(book=>getSongbookFn(book))).then(()=>this.storage.set("songbooks_data", songbookData));
		}
      
		Promise.all([getUserInfo().then(()=>getSongbooks()), getThemes(), getCps()]).then(()=>{
			observer.complete();
		})
    })
  }
  async initSongs(){
    if(this.user && this.user.packages){
      var hasSongBooks = this.user.packages.open;
      var hasSpecialAccess = this.user.packages.private;
    }
    let packageId="package1";
    if(hasSongBooks && !hasSpecialAccess) packageId = "package2";
    else if(hasSongBooks && hasSpecialAccess) packageId = "package3";
    let songsListTemp = [];
    await this.songsCollection.where("packages", "<=",packageId).get().then(snap=>{
      snap.forEach(meta =>{
        let song = meta.data();
        song.songbooks={};
        for(let book of this.availableSbx){
          if(this.songbooks[book].songs[song.orgKey]) song.songbooks[book]=this.songbooks[book].songs[song.orgKey];
        }
        songsListTemp.push(song);      
      });
	});
	this.songs = songsListTemp.slice();
    songsListTemp = this.orderBy(songsListTemp);//order alpha
    this.songsList = songsListTemp.slice();
    this.songsList = await this.filterByCarnet(this.songsList);//filter on carnet
    this.storage.set('lastUpdateTime', (new Date()).getTime())
	  this.storage.set('songslist', songsListTemp);
    return this.songsList;
  }
  
  async updateSongs(){
	let nbOfSongs:number=0;
    this.user = await this.auth.getUserInfo();
    if(this.user && this.user.packages){
      var hasSongBooks = this.user.packages.open;
      var hasSpecialAccess = this.user.packages.private;
    }
    let lastUpdateTime = Date.now();    
    let date = await this.storage.get("lastUpdateTime");
    if(!date) {
      this.storage.set('lastUpdateTime', lastUpdateTime);
    }
    else {
      //get total list of songs
      let songsListTemp = await this.storage.get('songslist');
      //get last updated public songs
      let publicSongsSnap=  await this.songsCollection.where('lastUpdateTime', '>=', date).where('packages', '==', "package1").get();
      publicSongsSnap.forEach(songSnap =>{
        let song = songSnap.data();
        let index = songsListTemp.findIndex(element=>element.orgKey==song["orgKey"]);
        if(index>=0){
          var songbooks = songsListTemp[index].songbooks;
          songsListTemp[index]=song;
          songsListTemp[index].songbooks=songbooks?songbooks:{};
        }
		else songsListTemp.push(song);
		nbOfSongs++;
      });
      //get last updated protected songs
      if(hasSongBooks || hasSpecialAccess) {
        let protectedSongsSnap=  await this.songsCollection.where('lastUpdateTime', '>=', date).where('packages', '==', "package2").get();
        protectedSongsSnap.forEach(songSnap =>{
          let song = songSnap.data();
          let index = songsListTemp.findIndex(element=>element.orgKey==song["orgKey"]);
          if(index>=0){
            var songbooks = songsListTemp[index].songbooks;
            songsListTemp[index]=song;
            songsListTemp[index].songbooks=songbooks?songbooks:{};
          }
		  else songsListTemp.push(song);
		  nbOfSongs++;
        });
      }
      //get last updated private songs
      if(hasSpecialAccess) {
        let privateSongsSnap=  await this.songsCollection.where('lastUpdateTime', '>=', date).where('packages', '==', "package3").get();
        privateSongsSnap.forEach(songSnap =>{
          let song = songSnap.data();
          let index = songsListTemp.findIndex(element=>element.orgKey==song["orgKey"]);
          if(index>=0){
            var songbooks = songsListTemp[index].songbooks;
            songsListTemp[index]=song;
            songsListTemp[index].songbooks=songbooks?songbooks:{};
          }
		  else songsListTemp.push(song);
		  nbOfSongs++;
        });
      }
      //get last updated infos in songbooks
      if(this.user.songbooks){
        for (let book of this.user.songbooks){
          //si user a des carnets non présents dans la liste officielle => les récupérer aussi
          if(this.availableSbx.indexOf(book)==-1){
            this.availableSbx.push(book);
          }
        }
	  }
	  let songbooksData = await this.storage.get("songbooks_data");//null
	  if(!songbooksData) songbooksData = {};
      for(let book of this.availableSbx){
		if(!songbooksData[book]) this.songbooksCollection.doc(book).get().then(snap =>{
			songbooksData[book] = snap.data();
			this.storage.set("songbooks_data", songbooksData);
			if(book != 'APPLI_FR') this.downloadCarnet(book);
		})
        let songbooksSnap = await this.songbooksCollection.doc(book).collection("songs").where('lastUpdateTime', '>=', date).get();
        songbooksSnap.forEach(songSnap =>{
          let index = songsListTemp.findIndex(element=>element.orgKey==songSnap.id);
          if(index>-1) {
            if(!songsListTemp[index].songbooks) songsListTemp[index].songbooks={};
			songsListTemp[index].songbooks[book] = songSnap.data();
          }
        })
      }
      this.storage.set('lastUpdateTime', (new Date()).getTime());
      songsListTemp = this.orderBy(songsListTemp);//order alpha
      //this.songsList = this.filterByCarnet(songsListTemp.slice());//filter on carnet
      await this.storage.set('songslist', songsListTemp);
      this.songsList=[];
      return nbOfSongs;
    }
  }  
  
  getListOfSongs(order:string){
    switch (order) {
      case "date" : {
        if(this.songsListDate) return  Promise.resolve(this.songsListDate);
        else {
          this.songsListDate = this.byDate.transform(this.songsList.slice(), this.carnet);
          return Promise.resolve(this.songsListDate);
        }
      }
      case "page" : {
        if(this.songsListPage) return  Promise.resolve(this.songsListPage);
        else {
          this.songsListPage = this.byPage.transform(this.songsList.slice(), this.carnet);
          return Promise.resolve(this.songsListPage);
        }
        
      }
      default : {
        if(this.songsList && this.songsList.length>0) {
			return  Promise.resolve(this.songsList);
		}
        else return this.storage.get('songslist').then(
          (val) => {
            return this.getLocalSongs().then(async local=>{
				let temp=[];
				if(local) {
					let array = await this.filterByCarnet(val);
					temp =  local.concat(array);
					temp = this.orderBy(temp);//order alpha
				}
              	else {
					let array = await this.filterByCarnet(val);
					temp = this.orderBy(array);
				}
				this.songsList = temp.slice();
				return temp;
            })
          }
        );
      }
    }

  }
  async getListOfSongsAlpha(){
	let val = await this.storage.get('songslist'); this.songs = val;
	let local = await this.getLocalSongs();
	let songs = await this.filterByCarnet(val);
	if(local) {
		val = this.byAlpha.transform(songs.concat(local),this.carnet); //this.orderBy(songs.concat(local));//order alpha
	}
	else val = this.byAlpha.transform(songs,this.carnet); //this.orderBy(songs);
	this.songsList = val;
	return val;
}
  getRubrique(rubrique:string, order:string){
    return this.getListOfSongs(order).then(
      songs => {
        let list = [];
        for(let i of songs) {
          if(i.songbooks[this.carnet] && (i.songbooks[this.carnet].rubrique== rubrique)) list.push(i);
          else if(i.songbooks['local'] && (i.songbooks['local'].rubrique== rubrique)) list.push(i);
        }
        return list;
      }
    )
  }
  
  getSongById(id){
	if(id.substring(0,5)=="local"){
		return this.getLocalSongs().then(local=>{
			let index = local.findIndex(element=>element.orgKey==id);
			return Promise.resolve(local[index])
	  
		})	
	}
    if(this.songs) {
      let index = this.songs.findIndex(element=>element.orgKey==id);
      return Promise.resolve(this.songs[index])
    }
    else return this.storage.get('songslist').then(
      val => {
        if(val) {
		  //this.songsList = val; bizarre...
		  this.songs = val;
          let index = this.songs.findIndex(element=>element.orgKey==id);
          return val[index];
        }
        else return false;

      }
    )
  }
  updateSongById(id, song){
	  if(id.substring(0,5)=="local"){}
	  else{
		let index = this.songsList.findIndex(element=>element.orgKey==id);
		if(index>=0){
			this.songsList[index]=song;
		  }
		else this.songsList.push(song);
		return this.storage.set('songslist',this.songsList).then(val=>{return song});
	}
  }
  async getCloseSongs(id:string, dir:string, rubrique:string, order:string, local:boolean){ //id du chant, direction du slider, rubrique
	let list = [];
	if(local) list = await this.getLocalSongs();
	else if(rubrique=='') list = await this.getListOfSongs(order);
	else list = await this.getRubrique(rubrique, order);
	let index = list.findIndex(item => item.orgKey == id); 
	let start = index>=this.nbOfSlides ? index - this.nbOfSlides : 0; 
	let end = index < list.length -this.nbOfSlides ? index + this.nbOfSlides :list.length;
	let firstSlide = index>=this.nbOfSlides ? this.nbOfSlides : index;
	if(dir=='forward') return {'list':list.slice(index+1, end+1), 'index':0};
	else if(dir=='backward') return {'list':list.slice(start, index), 'index':0};
	else return {'list':list.slice(start, end), 'index':firstSlide};

  }
  getVersion(){
    return this.storage.get('version').then(
      val => {return val;}
    )
  }
  setVersion(index){
    return this.storage.set('version', index);
  }
  getCpVersion(){
    return this.storage.get('cpVersion').then(
      val => {return val;}
    )
  }
  setCpVersion(index){
    return this.storage.set('cpVersion', index);
  }
  getTags(){
    if(this.themes) return Promise.resolve(this.themes);
    else return this.storage.get("themes").then(
      val => {this.themes = val; return val;}
    );
  }
  getCopyrights(){
    if(this.copyrights) return Promise.resolve(this.copyrights);
    else return this.storage.get("copyrights").then(
      val => {this.copyrights = val; return val;}
    );
  }
  async getCopyrightsInSongbook(){
    let cps = await this.getCopyrights();
    let songs  = await this.getListOfSongs("alpha");
    let orderedCps = [];
    let addedCps = [];
    for(let song of songs){
      if(song.copyright && song.copyright.international){
        if(addedCps.indexOf(String(song.copyright.international))==-1 && cps[song.copyright.international]) {
          addedCps.push(String(song.copyright.international));
          orderedCps.push({
            title : cps[song.copyright.international].title,
            id:song.copyright.international
          });
        }
      }
    }
    return this.orderBy(orderedCps);

  }
  updateCopyrights(){
    return this.cpCollection.get().then(
      cps => {
        this.copyrights=new Array(cps.length);
        cps.forEach(
          cp=>{
            this.copyrights[cp.id] = cp.data();
          }
        )
        this.storage.set("copyrights", this.copyrights);
      }
    )
  }
  async changeCarnet(newCarnet:string){
    this.carnet=newCarnet;
    this.songsList=null;
    this.songsListDate=null;
	this.songsListPage=null;
	
	//TODO : check if user has songbook !!
	let songbooksData = await this.storage.get("songbooks_data");//null
	if(!songbooksData) songbooksData = {};
	if(!songbooksData[newCarnet]) {
		//get songbook info and scenarios
		let snap = await this.songbooksCollection.doc(newCarnet).get();
		songbooksData[newCarnet] = snap.data();
		this.storage.set("songbooks_data", songbooksData)
		this.downloadCarnet(newCarnet);
	}

  }
  async downloadCarnet(carnet:string){
	let songsListTemp = await this.storage.get('songslist');
	let songsSnap = await this.songbooksCollection.doc(carnet).collection("songs").get();
	songsSnap.forEach(songSnap =>{
		let index = songsListTemp.findIndex(element=>element.orgKey==songSnap.id);
		if(index>-1) {
			if(!songsListTemp[index].songbooks) songsListTemp[index].songbooks={};
			songsListTemp[index].songbooks[carnet] = songSnap.data();
		}
	})
	}
	async getAvailableCarnets(){
		return await this.storage.get("songbooks_data");
	}
	async changeSelectedCarnets(){
		this.songsList=null;
		this.songsListDate=null;
		this.songsListPage=null;
		await this.getListOfSongsAlpha();
	}
  getCarnet(){
    return this.carnet;
  }
  //TODO : order with translated title
  orderBy(array){
    let accent = [
      /[\340-\346]/g, // a
      /[\350-\353]/g, // e
      /[\354-\357]/g, // i
      /[\362-\370]/g, // o
      /[\371-\374]/g, // u
    ];
    let noaccent = ['a','e','i','o','u'];
    let lang = "FR";
	
	lang = this.carnet.split("_")[1];
	if(lang=="BR") lang = "PT";
	else if(lang=="IL") lang="HE";

    // temporary array holds objects with position and sort-value
	let mapped = array.map(function(el, i) {
      let title = ""; 
      if(el.i18n && el.i18n[lang] && el.i18n[lang]!="") title = el.i18n[lang].toLowerCase();
      else if(el.title) title=el.title.toLowerCase();
      for(let i = 0; i < accent.length; i++){
        title = title.replace(accent[i], noaccent[i]);
      }
      return { index: i, value: title};
	})
	
    // sorting the mapped array containing the reduced values
    mapped.sort(function(a, b) {
      if (a.value > b.value) {
        return 1;
      }
      else if (a.value < b.value) {
        return -1;
      }
      else {return 0;}
	});
    // container for the resulting order
    let result = mapped.map(function(el){
      return array[el.index];
	});
    return result;
  }
  
  async filterByCarnet(array){
    let result=[];
	let params = await this.paramsProvider.getParams();
	if(!params.songbooks) {
		params.songbooks = ["APPLI_FR"];
		this.storage.set("params",params);
	}

	let booksArray = ["local"];
	for(let book of params.songbooks) {
		booksArray.push(book);
	}
	if(booksArray.indexOf(params.carnet)==-1) booksArray.push(params.carnet);//always display default songbook

	if(array) result = array.filter((item) => {
		if(!item.songbooks) return false;
		else {
			var hasCarnet = false;
			for(let book of booksArray) {
				if(item.songbooks[book]) {hasCarnet = true; break;}
			}
			return hasCarnet;
		}
	});
    return result;
  }
  /*LOCAL SONGS*/
  addLocalSong(song){
    return this.getLocalSongs().then(songs=>{
      if(songs && songs.length>0) {
        songs.push(song);this.localSongs=songs;
        //update this.songsList
        this.songsList.push(song);
        this.songsList = this.orderBy(this.songsList);//order alpha
        return this.storage.set("local_songs", songs);
      }
      else {
        this.localSongs=[song];
        this.songsList.push(song);
        this.songsList = this.orderBy(this.songsList);//order alpha
        return this.storage.set("local_songs", [song]);
      }
    })
  }
  getLocalSongs(){
    if(this.localSongs) return Promise.resolve(this.localSongs);
    else return this.storage.get("local_songs").then(
      val => {
        if(val) {this.localSongs = val; return val}
        else return [];
      }
    );
  }
  deleteLocalSong(id){
    return this.storage.get("local_songs").then(songs => {
      if(songs){
        let key = songs[id].orgKey;
        songs.splice(id,1);this.localSongs = songs;
        //delete from list of songs
        let index = this.songsList.findIndex(element=>element.orgKey==key);
        this.songsList.splice(index,1);
        this.storage.set('local_songs',songs);
        return songs;
      }
      else return [];
    });
  }
  editLocalSong(id,song){
    return this.storage.get("local_songs").then(songs => {
      if(songs){
        let index = songs.findIndex(element=>element.orgKey==id);
        songs[index]=song;this.localSongs = songs;
        this.storage.set('local_songs',songs);
        return songs;
      }
    });
  }
  getLocalSongById(id){
    return this.storage.get("local_songs").then(songs => {
      if(songs){
        let index = songs.findIndex(element=>element.orgKey==id);
        return songs[index];
      }
    });
  }

}
