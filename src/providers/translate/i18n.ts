export const i18n = {
	// Rubrique and Themes have not been completely translated	
		//UTILITIES
		add:{
			FR:"Ajouter",
			EN:"Add",
			HU:"Hozzáadás",
			DE:"Hinzufügen"
		},
		all:{
			FR:"Tous",
			EN:"All",
			HU:"Összes",
			DE:"Alle"
		},
		apply:{
			FR:"Appliquer",
			EN:"Apply",
			HU:"Alkalmaz",
			DE:"Anwenden"
		},
	
		cancel:{
			FR:"Annuler",
			EN:"Cancel",
			HU:"Mégse",
			DE:"Abbrechen"
		},
		close:{
			FR:"Fermer",
			EN:"Close",
			HU:"Bezárás",
			DE:"Schließen"
		},
		copy:{
			FR:"Copier",
			EN:"Copy",
			HU:"Másolás",
			DE:"Kopieren"
		},
		create:{
			FR:"Créer",
			EN:"Create",
			HU:"Létrehozás",
			DE:"Erstellen"
		},
		delete:{
			FR:"Suppr.",
			EN:"Delete",
			HU:"Törlés",
			DE:"Löschen"
		},
		download:{
			FR:"Télécharger",
			EN:"Download",
			HU:"Letöltés",
			DE:"Herunterladen"
		},
		edit:{
			FR:"Modifier",
			EN:"Edit",
			HU:'Szerkesztés',
			DE:"Bearbeiten"
		},
		error:{
			FR:"Erreur",
			EN:"Error",
			HU:'Hiba',
			DE:"Fehler"
		},
		loading:{
			FR:"Chargement...",
			EN:"Loading...",
			HU:"Betöltés...",
			DE:"Loading..."
	
		},
		irreversible:{
			FR:"Cette action est irréversible.",
			EN:"Please note that this action is irreversible",
			DE:"Diese Aktion ist unwiderruflich"
		},
		no_internet:{
			FR:"Erreur de connexion internet",
			EN:"No internet connexion",
			HU:"Nincs internetkapcsolat",
			DE:"Keine Internetverbindung"
		},
		titre:{
			FR:"Titre",
			EN:"Title",
			HU:"Cím",
			DE:"Titel"
		},
		save:{
			FR:"Enregistrer",
			EN:"Save",
			HU:"Mentés",
			DE:"Speichern"
		},
	
		/*********** */
		//RUBRIQUES
		/*********** */
		ANIMATION:{
			EN:"Action Songs",
			HU:"Animáció"
		},
		LOUANGE:{
			EN:"Praise",
			HU:"Dicsőítés"
		},
		"ADORATION - CONFIANCE":{
			EN:"Adoration - Trust",
			HU:"Szentségimádás - Bizalom"
		},
		"ESPRIT-SAINT":{
			EN:"Holy Spirit",
			HU:"Szentlélek"
		},
		RÉCONCILIATION:{
			EN:"Reconciliation",
			HU:"Kiengesztelődés"
		},
		"REFRAINS ET LITANIES":{
			EN:"Choruses and litanies",
			HU:"Refrének és litániák"
		},
		"HYMNES ET CANTIQUES":{
			EN:"Hymns and Songs",
			HU:"Himnuszok és kantikumok"
		},
		PSAUMES:{
			EN:"Psalms",
			HU:"Zsoltárok"
		},
		"CHANTS LITURGIQUES":{
			EN:"Songs for the Eucharist",
			HU:"Liturgikus énekek"
		},
		"ORDINAIRES DE LA MESSE":{
			EN:"Ordinary of the Mass",
			HU:"A szentmise állandó részei"
		},
		DANSES:{
			EN:"Dances",
			HU:"Táncok"
		},
		BÉNÉDICITÉS:{
			EN:"Benedictions",
			HU:"Áldások"
		},
		PRIÈRES:{
			EN:"Prayers",
			HU:"Imák"
		},
		/*********** */
		//THEMES / TAGS
		/*********** */
	
		"Eucharistie":{
			EN:"Eucharist",
			HU:"Szentmise",
			DE:"Eucharistie"
		},
		"Procession":{
			EN:"Procession",
			HU:"Bevonulás"
		},
		"Entrée":{
			EN:"Entry",
			HU:"Bevezetés",
			DE:"Eintritt"
		},
		"Offrande":{
			EN:"Offering",
			HU:"Felajánlás",
			DE:"Zur Gabenvorbereitung"
		},
		"Communion":{
			EN:"Communion",
			HU:"Szentáldozás",
			DE:"Kommunion"
		},
		"Envoi":{
			EN:"Sending Out",
			HU:"Kivonulás",
			DE:"Ansendung"
		},
		"Temps liturgiques":{
			EN:"Liturgical Seasons",
			HU:"Liturgikus időszakok",
		},
		"Temps Ordinaire":{
			FR:"Temps Ordinaire",
			HU:"Évközi idő"
		},
		"Temps de l’Avent":{
			FR:"Temps de l’Avent",
			EN:"Advent",
			HU:"Ádvent",
			DE:"Advent"
		},
		"Noël":{
			EN:"Christmas",
			HU:"Karácsony",
			DE:"Weihnachten"
		},
		"Temps du Carême":{
			EN:"Lent",
			HU:"Nagyböjt",
			DE:"Fastenzeit"
		},
		"Temps Pascal":{
			EN:"Easter",
			HU:"Húsvét",
			DE:"Ostern"
		},
		"Pentecôte":{
			FR:"Pentecôte",
			EN:"Pentecost",
			HU:"Pünkösd",
			DE:"Pfingsten"
		},
		"Fêtes liturgiques":{
			EN:"Festivals",
			HU:"Liturgikus ünnepek"
		},
		"Toussaint":{
			EN:"All Saints' Day",
			HU:"Mindenszentek",
			DE:"Allerheilligentag"
		},
		"Épiphanie":{
			EN:"Epiphany",
			HU:"Vízkereszt"
		},
		"Rameaux":{
			EN:"Palm Sunday",
			HU:"Virágvasárnap"
		},
		"Trinité":{
			EN:"Trinity",
			HU:"Szentháromság"
		},
		"Marie":{
			EN:"Mary",
			HU:"Mária",
			DE:"Maria"
		},
		"Mémoires des saints":{
			EN:"Saints",
			HU:"Szentek",
			DE:"Heilige"
		},
		"Baptême":{
			EN:"Baptism",
			HU:"Keresztelő",
			DE:"Taufe"
		},
		"Aspersion":{
			FR:"Aspersion",
			HU:"Meghintés szenteltvízzel"
		},
		"Engagement":{
			EN:"Commitment",
			HU:"Elköteleződés"
		},
		"Obsèques":{
			EN:"Funeral",
			HU:"Temetés",
			DE:"Beerdigung"
		},
		"Office des heures":{
			EN:"Office",
			HU:"Imaórák liturgiája"
		},
		"Office du matin":{
			EN:"Morning Prayer",
			HU:"Laudes",
			DE:"Morgenlob"
		},
		"Office du soir":{
			EN:"Evening Prayer",
			HU:"Vesperás",
			DE:"Abend"
		},
		"Complies":{
			EN:"Night Prayer",
			HU:"Komplétórium"
		},
		"Autres":{
			EN:"Other",
			HU:"Egyéb",
			DE:"Sonstige"
		},
		"Jour de désert":{
			EN:"Desert Day",
			HU:"Sivatagi nap"
		},
		"Chant du vendredi":{
			EN:"Friday's",
			HU:"Pénteki liturgia",
			DE:"Freitags Lied"
		},
		"Croix":{
			EN:"Cross",
			HU:"Kereszt",
			DE:"Kreuz"
		},
		"Réconciliation":{
			EN:"Reconciliation",
			HU:"Kiengesztelődés",
			DE:"Versöhnung"
		},
		"Unité des chrétiens":{
			EN:"Christian Unity",
			HU:"Keresztények egysége",
			DE:"Einheit der Christen"
		},
		"Famille-Cana":{
			EN:"Family-Cana",
			HU:"Család",
			DE:"Familie-Cana"
		},
		"Paix":{
			FR:"Paix",
			EN:"Peace",
			DE:"Frieden",
			HU:"Béke"
		},
		"Danses":{
			EN:"Dances",
			HU:"Táncok",
			DE:"Tänze"
		},
		"Danse":{
			EN:"Dance",
			HU:"Tánc",
			DE:"Tanz"
		},
		"Danse d’Israël":{
			EN:"Israeli Dance",
			HU:"Izraeli táncok"
		},
		"Danse Folk":{
			EN:"Folk Dance",
			HU:"Népek táncai"
		},
		"Danse-Madison":{
			EN:"Madison",
			HU:"Madison tánc"
		},
		"Groupe de prière":{
			EN:"Prayer Group",
			HU:"Imacsoport"
		},
		"Chant gestué":{
			EN:"Action Song",
			HU:"Mutogatós ének"
		},
		"Louange":{
			EN:"Praise",
			HU:"Dicsőítés",
			DE:"Lobpreis"
		},
		"Louange-animation": {
			EN:"Praise-Action",
			HU:"Dicsőítés-animáció"
		},
		"Louange-ouverture":{
			EN:"Praise opening",
			HU:"Dicsőítés-kezdés"
		},
		"Louange rythmée":{
			EN:"Praise",
			HU:"Dicsőítés"
		},
		"Louange d’adoration":{
			EN:"Adoration Praise",
			HU:"Dicsőítés-hódolat"
		},
		"Adoration": {
			EN:"Adoration",
			HU:"Áhitat",
			DE:"Anbetung"
		},
		"Gloire":{
			EN:"Glory",
			HU:"Dicsőség"
		},
		"Confiance":{
			FR:"Confiance-Abandon",
			EN:"Trust",
			HU:"Bizalom",
			DE:"Vertrauen"
		},
		"Action de grâce": {
			FR:"Action de Grâce-Victoire",
			EN:"Thanksgiving-Victory",
			HU:"Hálaadás",
			DE:"Danksagung"
		},
		"Jubilation":{
			EN:"Jubilation",
			HU:"Ujjongó dicséret",
			DE:"Jubel"
		},
		"Esprit-Saint":{
			EN:"Holy Spirit",
			HU:"Szentlélek",
			DE:"Heiliger Geist"
		},
		/*********** */
		//MENU
		/*********** */
		all_songs:{
			EN:"Home",
			HU:"Kezdőlap",
			DE:"Startseite"
		},
		rubriques:{
			EN:"Sections",
			HU:"Sections",
			DE:"Themen"
		},
		chants_personnels:{
			EN:"Personal songs",
			HU:"Saját dalok",
			DE:"Persönliche Lieder"
		},
		playlists:{
			EN:"Playlists",
			HU:"Lejátszási lista",
			DE:"Playlists"
		},
		archives:{
			FR:"Archives",
			EN:"Archives",
			HU:"Archívum",
			DE:"Archiv"
		},
		priere_unite:{
			EN: "Prayer for unity",
			HU:"Ima a keresztények egységéért",
			DE:"Gebet für die Einheit der Christen"
		},
		messages:{
			EN:"Inbox",
			HU:"Beérkezett üzenetek",
			DE:"Posteingang"
		},
		contact:{
			EN:"Contact",
			HU:"Kapcsolat",
			DE:"Kontakt"
		},
		boutique:{
			EN:"Shop",
			HU:"Áruház",
			DE:"Geschäft"
		},
		donate:{
			EN:"Help us",
			HU:"Hogyan segíthetsz nekünk",
			DE:"Helfen Sie uns"
		},
		info:{
			EN:"About us",
			HU:"Rólunk",
			DE:"Über uns"
		},
		compte:{
			EN:"My account",
			HU:"Fiókom",
			DE:"Mein Konto"
		},
		login:{
			FR:"Se connecter",
			EN:"Log in",
			HU:"Bejelentkezés",
			DE:"Anmeldung"
	
		},
		params:{
			EN:"Settings",
			HU:"Beállítások",
			DE:"Einstellungen"
		},
	
		/*********** */
		// PAGES   //
		/*********** */
	
		//HOME PAGE
		get_all_songs:{
			EN: "To access all songs, create your account here",
			HU:"Az összes dal hozzáféréséhez készítsd el saját fiókod",
			DE:"Um auf alle Lieder zuzugreifen, erstellen Sie hier Ihr Konto."
		},
		filters:{
			FR:"Filtres",
			EN:"Filters",
			HU:"Szűrők",
			DE:"Filter"
		},
		sort_by:{
			EN:"Sort by",
			HU:"Szűrés",
			DE:"Sortieren nach"
		},
		nouveautes:{
			EN:"New",
			HU:"Új",
			DE:"Neu"
		},
		page:{
			EN:"Page",
			HU:"Oldal",
			DE:"Nummer"
		},
		filter_by_lang:{
			FR:"Langue",
			EN:"Language",
			HU:"Szűrés nyelv szerint",
			DE:"Sprache"
		},
		filter_by_author:{
			FR:"Auteur",
			EN:"Author",
			HU:"Szerző",
			DE:"Autor"
		},
		filter_audio:{
			FR:"Avec audio",
			EN:"With music audio",
			HU:"Hanggal",
			DE:"Mit der Musik Audio"
		},
		filter_midi:{
			FR:"Avec apprentissage des voix",
			EN:"With the different voices audio files",
			HU:"Szólamokkal",
			DE:"Mit den verschiedenen Stimmen Audio Files" 
		},
		reference:{
			FR:"Référence biblique",
			EN:"Biblical reference",
			HU:"Bibliai referencia",
			DE:"Biblische Referenz"
		},
		search_by_title:{
			EN:"Search by title",
			HU:"Cím szerinti keresés",
			DE:"Suche nach Titel"
		},
		quit:{
			EN:"Close app",
			HU:"Az alkalmazás bezárása",
			DE:"App schließen"
		},
		quit_confirm:{
			FR:"Voulez-vous vraiment quitter l'application ?",
			EN:"Do you really want to close the app?",
			HU:"Valóban ki szeretne lépni az alkalmazásból?",
			DE:"Wollen Sie die App wirklich schließen?"
		},
		updated_songs:{
			FR:"chants mis à jour",
			EN:"updated songs",
			HU:"frissített énekek",
			DE:"aktualisierte Lieder"
		},
	
		// SONG DETAIL
		admin_by:{
			FR:"Admin. par",
			EN:"Admin. by",
			HU:"Az énekek felelőse",
			DE:"verwaltet von"
		},
		traduction:{
			FR:"Traduction",
			EN:"Translation",
			HU:"Fordítás",
			DE:"Übersetzung"
		},
		key:{
			FR:"Tonalité",
			EN:"Key",
			HU:"Hangnem",
			DE:"Tonart"
		},
		paroles:{
			FR:"Paroles",
			EN:"Text",
			HU:"Szöveg",
			DE:"Text"
		},
		music:{
			FR:"Musique",
			EN:"Music",
			HU:"Dallam",
			DE:"Musik"
		},
		paroles_et_music:{
			FR:"Paroles et musique",
			HU:"Szöveg és dallam",
			EN:"Text and music",
			DE:"Text und Musik"
		},
		origine:{
			FR:"Origine",
			EN:"Origin",
			HU:"Eredet",
			DE:"Herkunft"
		},
		basse:{
			FR:"Basse",
			EN:"Bass",
			HU:"basszus",
			DE:"Bass"
		},
		tenor:{
			FR:"Ténor",
			EN:"Tenor",
			HU:"tenor",
			DE:"Tenor"
		},
		alto:{
			FR:"Alto",
			EN:"Alto",
			HU:"alt",
			DE:"Alto"
		},
		soprane:{
			FR:"Soprane",
			EN:"Soprano",
			HU:"szoprán",
			DE:"Soprano"
		},
		voix:{
			FR:"4 voix",
			EN:"4 voices",
			HU:"4 szólam",
			DE:"4 Stimmen"
		},
		first_note:{
			FR:"Première note",
			HU:"Kezdő hang",
			EN:"First note",
			DE:"Erste Note"
		},
		play_song:{
			FR:"Jouer",
			HU:"Lejátszás",
			EN:"Play",
			DE:"Hören"
		},
	
		//CREATE LOCAL SONG (AddPage)
		bridge:{
			FR:"Bridge",
			EN: "Bridge",
			HU:"Bridge",
			DE:"Brücke"
		},
		couplet:{
			FR:"Couplet",
			EN:"Verse",
			HU:"Versszak",
			DE:"Vers"
		},
		intro:{
			FR:"Intro",
			EN:"Intro",
			HU:"Bevezetés",
			DE:"Einleitung"
		},
		inter:{
			FR:"Inter",
			EN:"Inter",
			HU:"Közjáték",
			DE:"Inter"
		},
		preref:{
			FR:"Pré refrain",
			EN:"Pre-chorus",
			HU:"Pre-kórus",
			DE:"Überleitung"
		},
		ref:{
			FR:"Refrain",
			EN:"Chorus",
			HU:"Kórus",
			DE:"Refrain"
		},
		contrechant:{
			EN:"Contrechant",
			HU:"Ellenszólam",
			DE:"Contrechant"
		},
		coda:{
			EN:"Coda",
			HU:"Coda",
			DE:"Coda"
		},
		fin:{
			FR:"Fin",
			EN:"End",
			HU:"Vége",
			DE:"Ende"
		},
		antienne:{
			FR:"Antienne",
			EN:"Antiphon",
			HU:"Antifóna",
			DE:"Antiphon"
		},
		antiennes:{
			FR:"Antiennes",
			EN:"Antiphons",
			HU:"Antifónában",
			DE:"Antiphon"
		},
		verset:{
			FR:"Verset",
			EN:"Psalm verse",
			HU:"Zsoltár versszak",
			DE:"Psalmvers"
		},
		tags:{
			FR:"Mots-clés",
			EN:"Tags",
			HU:"Címkék",
			DE:"Stichworte"
		},
		new_song:{
			FR:"Nouveau chant",
			EN:"New song",
			HU:"Új dal",
			DE:"Neues Lied"
		},
		edition:{
			FR:"Édition",
			EN:"Edition",
			HU:"Szerkesztés",
			DE:"Bearbeitung"
		},
		rubrique:{
			FR:"Rubrique",
			EN:"Section",
			HU:"Section",
			DE:"Sektion"
		},
		photo:{
			FR:"Photo",
			EN:"Photo",
			HU:"Fénykép",
			DE:"Foto"
		},
	
		// ARCHIVES DE PLAYLISTS
		archived:{
			EN:"Archived on",
			HU:"Archivált",
			DE:"Archiviert"
		},
		playlist_restored:{
			EN:"Playlist restored",
			HU:"Lejátszási lista visszaállítva",
			DE:"Wiederhergestellt Playlists"
		},
		//BOUTIQUE
		acn_shop:{
			EN:"Online shop",
			HU:"Online áruház",
			DE:"Online shop"
		},
		//CONTACT ET BUGS
		contact_text:{
			EN:"Contact us here to give us your feedback when using the app!",
			HU:"Lépj kapcsolatba velünk, hogy visszajelezhess nekünk az alkalmazással kapcsolatban!",
			DE:"Schreiben Sie uns hier um Ihrer Feedback bei der Nutzung der App zu geben"
		},
		contact_type:{
			EN:"Type of comment",
			HU:"Megjegyzés típusa",
			DE:"Kommentartyp"
		},
		contact_improve:{
			EN:"Improve feature",
			HU:"A szolgáltatás javítása",
			DE:"Feature verbessern"
		},
		contact_new:{
			EN:"New feature",
			HU:"Új funkció",
			DE:"Neues Feature"
		},
		contact_erreur:{
			EN:"Mistake on a song",
			HU:"Hiba egy dalban",
			DE:"Lieder Fehlermeldung"
		},
		contact_autre:{
			EN:"Autre",
			HU:"Más",
			DE:"Sonstiges"
		},
		contact_send:{
			EN:"Send feeback",
			HU:"Visszajelzés küldése",
			DE:"Feedback senden"
		},
		contact_version:{
			EN:"App version:",
			HU:"Alkalmazás verziója:",
			DE:"App Version"
		},
		// LISTE DES EDITEURS
		editors_list:{
			FR:"Liste des éditeurs",
			EN:"Editors' list",
			HU:"Szerkesztői lista",
			DE:"Liste der Verlage"
		},
		editeurs_text:{
			EN:"We have implemented all means in our possessions to claim the rights of reproduction of the works contained in this work. We are available to authors and publishers that we have not managed to contact.",
			HU:"Mindent megtettünk annak érdekében, hogy a szerzői jogok minden műnél feltüntetésre kerüljenek. Mindazonáltal rendelkezésére állunk minden szerzőnek és kiadónak, akivel eddig még nem sikerült felvennünk a kapcsolatot.",
			DE:"Wir haben zur Verfügung alle stehenden Mittel eingesetzt, um die Vervielfältigungsrechte an den in diesem Werk enthaltenen Werken geltend zu machen. Wir stehen zur Verfügung für alle Autoren und Verlage die wir nicht erreichen konnten."
		},
		//DOSSIER
		delete_folder:{
			FR:"Supprimer le dossier ?",
			EN:"Delete folder?",
			HU:"Valóban törli a mappát?",
			DE:"Ordner löschen"
		},
		// AIDEZ NOUS
		donate_short:{
			FR:"Pour nous permettre de continuer à vous offrir l'appli gratuitement, aidez-nous ! ",
			EN:"To allow us to continue to offer you the app for free, please help us!",
			HU:"Segítsen, hogy továbbra is ingyenesen megoszthassuk ezt az alkalmazást.",
			DE:"So dass wir diese App kostenlos beibehalten, bitte helfen sie uns."  
		},
		donate_line1:{
			EN:"We are really happy to give you free access to this app.",
			HU:"Örömünkre szolgál, hogy ingyen hozzáférést biztosíthatunk ehhez az alkalmazáshoz.",
			DE:"Wir freuen uns sehr, ihnen diese App kostenlos zu vorschlagen."
		},
		donate_line2:{
			EN:"We still have plenty of new feature ideas to enhance your experience with this app. So that we continue to offer you the app for free, help us by making a small donation! Every contribution is useful!",
			HU:"Még számos ötletünk van, amivel jobbá tehetjük az alkalmazás használatát. Járulj hozzá egy csekély összeggel ahhoz, hogy továbbra is ingyen biztosíthassuk az alkalmazást! Minden hozzájárulás számít!",
			DE:"Wir haben noch viele Ideen, Ihrer nutzer Erfahrung zu verbessern durch neue Features. Um Ihnen den App noch kostenlos anzubieten, helfen sie uns mit einer kleinen Spende! Jeder Beitrag ist wertvoll"
		},
		give_website:{
			FR:"Sur notre site internet",
			EN:"On our website:",
			HU:"A honlapunkon:",
			DE:"Auf unserer Website"
		},
		give_app:{
			FR:"Ou directement dans l'application",
			EN:"Or directly on the app:",
			HU:"Vagy közvetlenül az alkalmazásban:",
			DE:"Oder direkt in der App"
		},
		amount:{
			FR:"Montant",
			EN:"Amount",
			HU:'Összeg',
			DE:"Geldbetrag"
		},
		donation:{
			FR:"Donner",
			EN:"Donate",
			HU:"Adományok",
			DE:"Spenden"
		},
		help_by_rating:{
			FR:"Ou en notant notre application",
			EN:"Or by rating our application:",
			HU:"Vagy osztályozza az alkalmazást.",
			DE:"Oder durch den Bewertung unserer App"
		},
		rate:{
			FR:"Noter",
			EN:"Rate",
			HU:"Értékel",
			DE:"Bewerten"
		},
		dons_effectues:{
			FR:"Mes dons",
			EN:"My donations",
			HU:"Adományaim",
			DE:"Meine Spende"
		},
		// PLAYLISTS
		create_playlist:{
			FR:"Créer une playlist",
			EN:"Create new playlist",
			HU:"Új lejátszási lista létrehozása",
			DE:"Neue Playlist erstellen"
		},
		playlist_new:{
			FR:"Nouvelle playlist",
			EN:"New playlist",
			HU:"Új lejátszási lista",
			DE:"Neue Playlist"                                                                                                                      
		},
		add_to_playlist:{
			FR:"Ajouter à la playlist",
			EN:"Add to playlist",
			HU:"Hozzáadás a lejátszási listához",
			DE:"Zum Playlist hinzufügen"		
		},
		added_playlist:{
			FR:"La playlist a été ajoutée",
			EN:"The playlist has been added to shared playlists",
			HU:"Lejátszási lista hozzáadva",
			DE:"Die Playlist wurde hinzugefügt"
		},
		added_to_playlist:{
			FR:"Ajoutés à la playlist",
			EN:"Added to the playlist",
			HU:"Hozzáadva a lejátszási listához",
			DE:"Zum Playlist hinzugefügt"
		},
		code:{
			FR:"Code",
			EN:"Code",
			HU:"Kód",
			DE:"Code"
		},
		create_code:{
			FR:"Générer un code de partage",
			EN:"Create sharing code",
			HU:"Új kód megosztáshoz",
			DE:"Freigabecode erstellen"
		},
		sharing_code:{
			FR:"Code de partage",
			EN:"Sharing code",
			HU:"Kód megosztáshoz",
			DE:"Freigabecode"
		},
		playlist_shared_code:{
			FR:"Cette playlist est partageable avec le code ",
			EN:"You can share this playlist with the following code",
			HU:"A lejátszási lista megosztható a következő kóddal",
			DE:"Diese Playlist kann mit dem folgende Code geteilt werden" 
		},
		enter_code:{
			FR:"Entrer le code de partage",
			EN:"Enter code",
			HU:"Megosztási kód:",
			DE:"Freigabecode einschreiben"
		},
		new_code_playlist:{
			FR:"Playlist partagée par code",
			EN:"Shared playlist with code",
			HU:"A lejátszási lista megosztva a következő kóddal",
			DE:"Playlist durch Code freigegeben"
		},
		playlist_note:{
			FR:"Ajouter une note",
			EN:"Add note",
			HU:"Megjegyzés hozzáadása",
			DE:"Note addieren"
		},
		delete_playlist:{
			FR:"Supprimer la playlist ?",
			EN:"Delete playlist?",
			HU:"Lejátszási lista törlése?",
			DE:"Playlist löschen ?"
		},
		share:{
			FR:"Partager la playlist",
			EN:"Share playlist",
			HU:"Megosztás",
			DE:"Playlist freigeben"
		},
		unshare:{
			FR:"Annuler le partage ?",
			EN:"Unshare playlist ?",
			HU:"Megosztás?",
			DE:"Playlist Freigabe aufheben"
		},
		folders:{
			FR:"Dossiers",
			EN:"Folders",
			HU:"Mappák",
			DE:"Ordner"
		},
		add_to_folder:{
			FR:"Ajouter au dossier",
			EN:"Add to folder",
			HU:"Hozzáadás mappához",
			DE:"Zum Ordner hinzufügen"
		},
		new_folder:{
			FR:"Nouveau dossier",
			EN:"New folder",
			HU:"Új mappa",
			DE:"Neuer Ordner"
		},
		add_playlist:{
			FR:"Nouvelle playlist personnelle",
			EN:"New local playlist",
			HU:"Új lejátszási lista",
			DE:"Neue privat Playlist"
		},
		local_playlist:{
			FR:"Playlists personnelles",
			EN:"Local playlists",
			HU:"Helyi lejátszási listák",
			DE:"Privat Playlist"
		},
		shared_playlist:{
			FR:"Playlists partagées",
			EN:"Shared playlists",
			HU:"Megosztott lejátszási listák",
			DE:"Freigegebene Playlists"
		},
		playlist_deleted:{
			FR:"La playlist a été supprimée",
			EN:"Playlist has been deleted",
			HU:"A lejátszási lista törölve",
			DE:"Playlist wurde gelöscht"
		},
		playlist_archived:{
			FR:"La playlist a été archivée",
			EN:"Playlist has been archived",
			HU:"A lejátszási lista archiválva",
			DE:"Playlist wurde archiviert"
		},
		created:{
			FR:"Créée le",
			EN:"Created on",
			HU:"Létrehozott",
			DE:"Erstellt am"
		},
		archiver:{
			FR:"Archiver",
			EN:"Archive",
			HU:"Archívum",
			DE:"Archivieren"
		},
		shared:{
			FR:"Partagée le",
			EN:"Shared on",
			HU:"Megosztott lejátszási listák",
			DE:"Freigegeben"
		},
		by:{
			FR:"par",
			EN:"by",
			HU:"által",
			DE:"Von"
		},
	
		create_playlist_error:{
			FR:"Vous devez être connecté pour pouvoir créer des playlists.",
			EN:"You must be connected to create playlists.",
			HU:"Ahhoz, hogy lejátszási listákat hozhass létre, kapcsolódnod kell az internethez.",
			DE:"Sie mussen verbunden sein, um Playlists zu erstellen"
		},
		//not used
		playlist_updated:{
			EN:'Playlist updated',
			HU:'Lejátszási lista frissítve',
			DE:"Playlist aktualisiert"
		},
		shared_error:{
			EN:"Shared playlists are limited to 20 songs.",
			HU:"A megosztott lejátszási listák maximum 20 dalt tartalmazhatnak.",
			DE:"Freigegebene Playlists sind auf 20 Lieder beschränkt"
		},
		playlist_shared_with:{
			EN:"This playlist is shared with",
			HU:"A lejátszási lista megosztása vele",
			DE:"Diese Playlist ist geteilt mit"
		},
		share_text:{
			EN:'Share the playlist in real time, during 48 hours, with other users.',
			HU:'A lejátszási lista valós idejű megosztása más felhasználókkal 48 órán keresztül.',
			DE:"Playlist mit anderen Nützern während 48 Stunden freigeben"
		},
		//
		
	
		// LOCAL SONGS
		import_title:{
			FR:"Importer un chant",
			EN:"Import a new song",
			HU:"Új dal importálása",
			DE:"Lied importieren"
		},
		import_desc:{
			FR:"Le format de chant requis est le format ChordPro.",
			EN: "The required song format is the ChordPro format.",
			HU:"A dalnak ChordPro formátumban kell lennie.",
			DE:"Das erforderlicche Songformat ist das ChordPro-Format."
		},
		import_button:{
			FR:"Import",
			EN:"Import",
			HU:"Importálás",
			DE:"Importieren"
		},
		import_error_file:{
			FR:"Erreur de lecture du fichier",
			EN:"File reading error",
			DE:"Fehler beim Lesen von Dateien"
		},
		import_error_file_text:{
			FR:"Le fichier est soit vide, soit dans un format non lisible.",
			EN:"File is empty or in a non-readable format.",
			HU:"Az állomány üres vagy nem olvasható",
			DE:"Datei ist leer oder der Format ist nicht lesbar"
		},
		import_file_error_title:{
			FR:"Titre manquant",
			EN:"Title is missing",
			HU:"Nincs cím",
			DE:"Fehlender Titel"
		},
		import_file_error_title_text:{
			FR:"Le chant doit au moins avoir un titre valide. Le fichier doit avoir une ligne : {t:Titre}",
			EN:"The song must have a valid title. The file must have a ligne : {t:Title}",
			HU:"Az éneknek legalább egy címe kell legyen. Az állománynak tartalmaznia kell a következő sort: {t: Cím}",
			DE:"Der Song muss einen gültigen Titel haben. Die Datei muss eine Linie haben: {t:Titel}"
		},
		// LOGIN PAGE
		welcome:{
			FR:"Bienvenue dans l'application 'Louange et Liturgie !'",
			EN:"Welcome to our app!",
			HU:"Üdvözlünk az alkalmazásunkban!",
			DE:"Willkommen in unserer App!"
		},
		next:{
			FR:"Suivant",
			EN:"Next",
			HU:"Következő",
			DE:"Nächste"
		},
		no_account:{
			FR:"Pas de compte ?",
			EN:"I don't have an account",
			HU:"Nincs fiókom",
			DE:"Ich habe kein Konto"
		},
		go_on:{
			FR:"Continuer sans se connecter",
			EN:"I don't want to create an account",
			HU:"Nem szeretnék fiókot létrehozni",
			DE:"Ich möchte kein Konto erstellen"
		},
		logout:{
			FR:"Se déconnecter",
			EN:"Log out",
			HU:"Kijelentkezés",
			DE:"Abmelden"
		},
		register:{
			FR:"S'inscrire",
			EN:"Register",
			HU:"Regisztráció",
			DE:"Registrieren"
		},
		register_text:{
			FR:"Votre compte vous permettra de créer des listes de chants, de les partager avec d'autres utilisateurs et d'accéder aux accords et aux partitions des chants.",
			EN:"Your account will allow you to create playlists, share them with other users, and access chords.",
			HU:"Fiókod létrehozásával lejátszási listákat készíthetsz, megoszthatod azokat a többi felhasználóval, és hozzáférhetsz az akkordokhoz.",
			DE:"Mit Ihrem Konto können Sie Playlists erstellen, sie mit anderen Benutzern teilen und auf Akkorde zugreifen."
		},
		email:{
			EN:"Email address",
			HU:"Email cím",
			DE:"Email Adresse"
		},
		email_error:{
			FR:"L'adresse mail n'est pas valide",
			EN:"The email address is invalid",
			HU:"Érvénytelen email cím",
			DE:"Die E-Mail Adresse ist ungültig"
		},
		password:{
			EN:"Password",
			HU:"Jelszó",
			DE:"Passwort"
		},
		password_error:{
			EN:"Password must be at least 6 characters long",
			HU:"A jelszónak minimum 6 karaktert kell tartalmaznia",
			DE:"Dass Passwort muss mindestens 6 Zeichen lang sein"
		},
		password_mail_sent:{
			FR:"Un email vous a été envoyé",
			EN:"You will soon receive an email with more info",
			DE:"Sie erhalten in Kürze eine E-Mail mit weiteren Informationen"
		},
		user_name:{
			EN:"User name",
			HU:"Felhasználónév",
			DE:"Benutzer-Name"
		},
		user_name_error:{
			EN:"User name must be at least 5 characters long",
			HU:"A felhasználónévnek minimum 5 karaktert kell tartalmaznia",
			DE:"Der Benutzername muss mindestens 5 Zeichen lang sein"
		},
		error_name:{
			FR:"Ce nom est déjà utilisé",
			EN:"This name is already used",
			HU:"Ez a név már foglalt",
			DE:"Dieser Name ist bereits vergeben"
		},
		forgot_password:{
			EN:"Forgot your password?",
			HU:"Elfelejtetted a jelszavad?",
			DE:"Haben Sie Ihr Passwort vergessen?"
		},
		cgu_text:{
			EN:"I have read and accept the Terms of Use",
			HU:"Elolvastam és elfogadtam a felhasználási feltételeket.",
			DE:"Ich habe die Nutzungsbedingungen gelesen, und akzeptiere sie" 
		},
		read_cgu:{
			EN:"Read Terms of Use",
			HU:"Elolvasom a felhasználási feltételeket",
			DE:"Nutzungsbedingungen lesen"
		},
		error_cgu:{
			FR:"Vous devez accepter les Conditions générales d'utilisation pour pouvoir utiliser l'application",
			EN:"You must accept the Terms of Use to use the app",
			HU:"Az alkalmazás használatához el kell fogadnod a felhasználási feltételeket.",
			DE:"Sie müssen die Nutzungsbedingungen akzeptieren, um die App nutzen zu können"
		},
		other_id:{
			FR:"ou s'identifier avec",
			EN:"or log in with",
			HU:"vagy bejelentkezés ezzel:",
			DE:"Oder Anmeldung mit"
		},
		create_account:{
			EN:"Create account",
			HU:"Fiók létrehozása",
			DE:"Konto erstellen"
		},
		create_error:{
			FR:"Problème lors de la création du compte",
			EN:"There has been an error",
			HU:"Hiba a felhasználó létrehozásakor",
			DE:"Es ist ein Fehler bei der Erstellung Ihres Kontos aufgetreten"
		},
		create_success:{
			FR:"Votre compte a bien été enregistré",
			EN:"Your account is registered",
			HU:"Felhasználó létrehozva",
			DE:"Ihr Konto wurde erfolgreich erstellt"  
		},
		login_no_account:{
			FR:"Ne pas créer de compte",
			EN:"Do not create an account",
			HU:"Ne hozzon létre fiókot",
			DE:"Kein Konto erstellen"
		},
		login_no_account_desc:{
			FR:"Sans créer de compte, vous n\'aurez accès qu\'aux chants libres de droits.",
			EN:"Without creating an account, you will only have access to public domain songs.",
			HU:"Fiókod létrehozásával lejátszási listákat készíthetsz, megoszthatod azokat a többi felhasználóval, és hozzáférhetsz az akkordokhoz.",
			DE:"Ohne ein Konto zu erstellen, haben Sie nur Zugriff auf gemeinfreie Lieder"
		},
		continuer:{
			FR:"Continuer",
			EN:"Go on",
			HU:"Tovább",
			DE:"Fortfahren"
		},
	
	
		// PARAMS
		general_params:{
			FR:"Paramètres généraux",
			EN:"Global settings",
			HU:"Általános beállítások",
			DE:"Allgemeine Einstellungen" 
		},
		app_lang:{
			FR:"Langue de l'application",
			EN:"Language",
			HU:"Nyelv",
			DE:"Sprache"
		},
		font_size:{
			FR:"Taille de police",
			EN:"Font size",
			HU:"Betűméret",
			DE:"Zeichengroße"
		},
		carnet:{
			FR:"Carnet",
			EN:"Songbook",
			HU:"Énekeskönyv",
			DE:"Liederbücher"
		},
		additional_books:{
			FR:"Carnets supplémentaires",
			EN:"Additional songbooks",
			HU:"További énekeskönyvek",
			DE:"Weitere Liederbücher"
		},
		search_mode:{
			FR:"Recherche des chants",
			EN:"Search mode",
			HU:"Keresési mód",
			DE:"Lieder Suchmodus" 
		},
		title_only:{
			FR:"Titre uniquement",
			EN:"Title only",
			HU:"Csak cím",
			DE:"Nur Titel"
		},
		title_lyrics:{
			FR:"Titre et paroles",
			EN:"Title and lyrics",
			HU:"Cím és szöveg",
			DE:"Titel und Text"
		},
		insomnia:{
			FR:"Mise en veille de l'écran",
			EN:"Keep app awake",
			HU:"Az alkalmazás futhat a háttérben",
			DE:"App wach halten"
		},
		never:{
			FR:"Jamais",
			EN:"Never",
			HU:"Soha",
			DE:"Niemals"
		},
		dark_mode:{
			FR:"Mode sombre",
			EN:"Dark mode",
			HU:"Sötét mód",
			DE:"Dunkelmodus"
		},
		chord_display:{
			FR:"Affichage des accords",
			EN:"Chords' display",
			HU:"Akkordok megjelenítése",
			DE:"Akkorde Darstellung"
		},
		show_chords:{
			FR:"Afficher les accords",
			EN:"Show chords",
			HU:"Akkordok megjelenítése",
			DE:"Akkorde anzeigen"
		},
		chord_format:{
			FR:"Format des accords",
			EN:"Chord format",
			HU:"Akkord formátum",
			DE:"Akkorde Format"
		},
		french_chords:{
			FR:"Français",
			EN:"Fixed Do key",
			HU:"Francia",
			DE:"Französische Musiknotation"
		},
		german_chords:{
			FR:"Notation allemande",
			EN:"German key",
			HU:"Német",
			DE:"Deutsche Musiknotation"
		},
		chord_embed:{
			FR:"Accords intégrés au texte",
			EN:"Embedded chords",
			HU:"Beágyazott akkordok",
			DE:"Integrierte Akkorde"
		},
		no_capo:{
			FR:"Mode sans capo",
			EN:"No capo mode",
			HU:"Capo nélküli mód",
			DE:"Kein Kapodaster Modus"
		},
		autoscroll:{
			FR:"Scenario complet",
			EN:"Allow autoscroll",
			HU:"Automatikus görgetés engedélyezése",
			DE:"Auto Scrollen zulassen"
		},
		links:{
			FR:"Liens utiles",
			EN:"Useful links",
			HU:"Hasznos linkek",
			DE:"Hilfreich Links"
		},
		cgu:{
			FR:"Conditions générales d'utilisation",
			EN:"Terms of service",
			HU:"A szolgáltatás feltételei",
			DE:"Nutzungsbedingungen"
		},
	
		
};