import { Injectable } from '@angular/core';
import { i18n } from './i18n';
import { ParamsProvider } from '../../providers/params/params';
/*
  Generated class for the TranslateProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TranslateProvider {
	lang:string;
	constructor(private paramsProvider: ParamsProvider) {
		this.paramsProvider.getLang().then(res=>this.lang=res)
	}
  translate(id:string, defaultString:string){
	
	if(this.lang && i18n[id] && i18n[id][this.lang]) {
			
		return i18n[id][this.lang];
	}
	else return defaultString;

  }
  updateLang(lang:string){
	this.lang=lang;
  }

}
