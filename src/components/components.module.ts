import { NgModule } from '@angular/core';
import { SongDetailComponent } from './song-detail/song-detail';
import { PipesModule } from '../pipes/pipes.module';
import {CommonModule} from '@angular/common';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [SongDetailComponent],
	imports: [PipesModule,CommonModule, IonicModule],
	exports: [SongDetailComponent]
})
export class ComponentsModule {}
