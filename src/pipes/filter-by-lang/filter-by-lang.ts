import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FilterByLangPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filterByLang',
})
export class FilterByLangPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any[], lang:string, carnet:string) {
    return value.filter((song)=>{
		return song.songbooks && song.songbooks[carnet] && song.songbooks[carnet].scenario && song.songbooks[carnet].scenario[lang];
	})
  }
}
