import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TranslatePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  lang:string="FR";
  
  transform(value: string, trads:any, carnet:string) {
	if(carnet) this.lang = carnet.split("_")[1];
	if(carnet && carnet.split("_")[1]=='IL') this.lang = "HE";
    if (trads && trads[this.lang] && trads[this.lang]!="") return trads[this.lang];
    else return value;
  }
}
