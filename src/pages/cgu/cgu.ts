import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the CguPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cgu',
  templateUrl: 'cgu.html',
})
export class CguPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,	private translateProvider:TranslateProvider
	) {
  }

  ionViewDidLoad() {
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
	}

}
