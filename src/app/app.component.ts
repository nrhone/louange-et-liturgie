import { Component , ViewChild , HostListener} from '@angular/core';
import { IonicApp , App , Platform , MenuController , Nav , AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HeaderColor } from '@ionic-native/header-color';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ParamsProvider } from '../providers/params/params';
import { Insomnia } from '@ionic-native/insomnia';
import { FCM } from '@ionic-native/fcm';
import { TranslateProvider } from '../providers/translate/translate'
import { MessagesProvider } from '../providers/messages/messages';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild('content') nav: Nav;
  rootPage:any = "LoginPage";
  //rootPage:string;
  user:any;
  system:string;
  darkMode:boolean;
  public pages : Array<{title: string, pages: Array<Page>, separator:boolean}> = [
    {
      title:"Groupe 1",
      pages:[
        { title: 'Tous les chants', component: "HomePage", icon:"home", id:"all_songs" },
        { title : 'Rubriques', component : 'RubriquesPage', icon:'ios-bookmarks', id:'rubriques'},
        { title: 'Chants personnels', component: "LocalSongsPage", icon:"list",id:'chants_personnels' },
        { title : 'Playlists', component : 'PlaylistsPage', icon:"folder-open", id:'playlists'},
        { title : 'Archives', component : 'ArchivePage', icon:"archive", id:"archives"},
        { title : 'Prière pour l\'Unité', component : 'SongPage', icon:"logo-rss", id:"priere_unite"},
       //{ title : 'Migration', component : 'MigrationPage', icon:'ios-bookmarks'}, 
      ], separator:true
    },
    {
      title:"Groupe 2",
      pages:[
		{ title : 'Messages', component : 'InboxPage', icon:'mail', id:"messages"},
		{ title : 'Retours et bugs', component : 'ContactPage', icon:'bug', id:"contact"},
        { title : 'Boutique', component : 'BoutiquePage', icon:'cart', id:"boutique"},
        //{ title : 'Projection', component : 'ProjectionPage', icon:'desktop'},
        { title : 'Aidez-nous', component : 'GivePage', icon:'heart-outline', id:"donate"},
        { title : 'À propos', component : 'AboutPage', icon:'information-circle', id:"info"}
      ], separator:true
    },
    {
      title:"Groupe 3",
      pages:[
        { title: 'Login', component: "LoginPage", icon:"log-in" , id:"login"},
        { title : 'Paramètres', component : 'ParamsPage', icon:'settings', id:"params"}
      ],
      separator:false
    }
    
    
  ];
  public timeToSleep : number = 20 * 60*1000; // temps avant de permettre la mise en veille en millisecondes
  constructor(
    public  app: App,
    public platform: Platform, 
    private statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public menu: MenuController,
    private afAuth: AngularFireAuth,
    headerColor: HeaderColor,
    private auth: AuthServiceProvider,
    private paramsProvider:ParamsProvider,
    private insomnia: Insomnia,
    private fcm: FCM,
    public alertCtrl: AlertController,
    private ionicApp: IonicApp,
	public translateProvider:TranslateProvider,
	private messagesProvider:MessagesProvider,
) 
  {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
	  //statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#e03512');
      statusBar.styleLightContent();
      
      //statusBar.hide();
      headerColor.tint('#e03512');
      //check if user and redirect
      this.auth.getUserInfo().then(user=>{
        if(user){
          this.user=user; 
          this.rootPage="HomePage";
          this.nav.setRoot("HomePage",{"updateSongs":true});
          splashScreen.hide();
          this.auth.getUserRights();
          this.pages[2].pages[0] = {title:"Mon compte", component:"LoginPage", icon:"person", id:'compte'};
          if(this.platform.is('cordova')){
            this.fcm.getToken().then(token => {
              this.auth.registerToken(token);
            });
          };
        }
        else {
          this.rootPage = "LoginPage";
          this.nav.setRoot("LoginPage");
          splashScreen.hide();
          this.user=null;
        }
      })
        //notifications
      if(this.platform.is('cordova')){
        this.insomnia.keepAwake().then(
          () => console.log('no sleep'),
          () => console.log('error')
        );
        this.fcm.getToken().then(token => {
          this.auth.registerToken(token);
        });
        this.fcm.onNotification().subscribe(data=>{
          if(data.listId) {
            let confirm = this.alertCtrl.create({
              title: data.title,
              message: data.body,
              buttons: [
                {
                  text: 'Fermer',
                  handler: () => {
                  }
                },
                {
                  text: 'Voir',
                  handler: () => {
                    this.nav.push("PlaylistPage", {
                      id: data.listId,
                      isShared:true
                    });
                  }
                }
              ]
            });
            
            
            if(data.wasTapped){
              //si reçu en background, aller directement à la playlist
              this.nav.push("PlaylistPage", {
                id: data.listId,
                isShared:true
              });
            } else {
              // si reçu avec l'appli ouverte, on affiche le pop up
              if(this.user && this.user.userId==data.createUserID) {
                //si playlist créée par l'utilisateur, ne rien faire
              }
              else confirm.present();
            };
          }
          else {
            let confirm = this.alertCtrl.create({
              title: data.title,
              message: data.body,
              buttons: [
                {
                  text: 'Fermer',
                  handler: () => {
                  }
                },
              ]
            });
            confirm.present();
            
          }
          

        });
        this.fcm.onTokenRefresh().subscribe(token => {
          this.auth.registerToken(token);
        });
        if(this.platform.is('ios')) this.system='ios'
        else if(this.platform.is('android')) this.system='android';
      }
	  this.paramsProvider.getParams().then(params=>{
		if(params && params.veille) {this.timeToSleep=params.veille* 60*1000; }
	  })
	  this.paramsProvider.getInit().then(res=>{
		if(!res) res={showDonation:true, viewNumber:0}; 
		this.paramsProvider.setInit(res.showDonation, res.viewNumber+1); 
		if(res.viewNumber%20==5 && res.showDonation)  {
		  let confirm = this.alertCtrl.create({
			title: "Aidez-nous",
			message: "Pour nous permettre de continuer à vous offrir l'appli gratuitement, aidez-nous ! ",
			buttons: [
			  {
				text: 'Fermer',
				handler: () => {
				}
			  },
			  {
				text: 'Donner',
				handler: () => {
				  this.nav.push("GivePage");
				}
			  }
			]
		  });
		  confirm.present();
		}
	  })
	  //get last messages
	  this.messagesProvider.getLastMessages();
	  this.messagesProvider.getUnread.subscribe(val=>this.pages[1].pages[0].badge = val);	
	  this.paramsProvider.getMode.subscribe(val=>{
		this.darkMode = val;
		if(this.darkMode) this.statusBar.backgroundColorByHexString('#000000');
		else this.statusBar.backgroundColorByHexString('#e03512');
	  })
    });
    
    
    //pour empêcher de quitter l'appli sur un retour arrière du bouton
    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNavs()[0];
      let activeView = nav.getActive();console.log(activeView);
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
          this.ionicApp._modalPortal.getActive() ||
          this.ionicApp._toastPortal.getActive() ||
          this.ionicApp._overlayPortal.getActive();
		  console.log(this.nav.getActive().component);
		  console.log(this.nav.getActive());
      if(activePortal){
		activePortal.dismiss();
	  }
      else if(this.nav.getActive().id === "HomePage") {
		let confirm = this.alertCtrl.create({
			title: 'Quitter',
			message: 'Voulez-vous vraiment quitter l\'application ?', 
			buttons: [
			  {
				text: 'Annuler',
				handler: () => {
				}
			  },
			  {
				text: 'Quitter',
				handler: () => {
					this.platform.exitApp();
				}
			  },
			]
		  });
		if(this.platform.is('android')) confirm.present();      }
      else if (nav.canGoBack()){ //Can we go back?``
        nav.pop();
      } else {
        nav.setRoot("HomePage");
		this.rootPage="HomePage";		
      }
    });
    
  }
  
  ngAfterViewInit() {
    //check auth change to update nav
    this.afAuth.auth.onAuthStateChanged(user=>{
		if(user){
			this.rootPage="HomePage";
			this.pages[2].pages[0] = {title:"Mon compte", component:"LoginPage", icon:"person", id:"compte"};
			this.auth.getUserInfo().then(localUser=>{this.user=localUser;})
		}
		else {
			//user not logged in Firebase 
			this.auth.getUserInfo().then(localUser=>{
				if(localUser && localUser!='no_user') {
					// si l'utilisateur a déjà son compte créé
					let confirm = this.alertCtrl.create({
						title: 'Erreur',
						message: 'Vous avez été déconnecté. Pour continuer à utiliser toutes les fonctionnalités de l\'application, merci de vous reconnecter', 
						buttons: [
						  {
							text: 'Annuler',
							handler: () => {
							}
						  },
						  {
							text: 'OK',
							handler: () => {
								this.auth.logout().subscribe(
									()=>{},
									()=>{},
									()=>{
										this.rootPage="LoginPage";
										this.nav.setRoot("LoginPage");
									}
								);
							}
						  },
						]
					  });
					confirm.present();
				}
			})
			return false;
		}
    })
  }
  
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    if(page.component !="SongPage") {
      this.nav.setRoot(page.component);
      this.rootPage = page.component;
    }
    else {
      this.nav.push("SongPage", {
        id: 743,
        order:"alpha"
      });
    }    
  }
  @HostListener('click') onClick() {
    // Your click functionality
    setTimeout(()=>{
      this.insomnia.allowSleepAgain()
    },this.timeToSleep* 60*1000)
  }
  closeApp(){
    this.platform.exitApp();
  }
  translate(id:string, defaultString:string){
	  return this.translateProvider.translate(id, defaultString);
  }
  

  
}


export interface Page {
  title: string, 
  component: any, 
  icon : string ,
  id:string,
  badge?:number
}