import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GivePage } from './give';
//import { PayPal } from '@ionic-native/paypal';

@NgModule({
  declarations: [
    GivePage,
  ],
  imports: [
    IonicPageModule.forChild(GivePage),
  ],
  //providers:[PayPal]
})
export class GivePageModule {}
