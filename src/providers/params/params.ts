import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';

/*
  Generated class for the ParamsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ParamsProvider {
	public getMode = new Subject<boolean>();
	constructor(public storage: Storage) {
    
  }
  updateParams(params){
	this.getMode.next(params.darkMode?params.darkMode:false);
	return this.storage.set('params',params);
  }
  getParams() {
    return this.storage.get('params').then(res=>{
		if(res) this.getMode.next(res.darkMode?res.darkMode:false);
		return res;
	});
  }
  async getLang(){
	  let params = await this.getParams();
	  if(params && params.lang) return params.lang;
	  else return "FR";
  }
  getInit(){
    return this.storage.get('sb_init');
  }
  setInit(show,init){
    return this.storage.set("sb_init", {showDonation:show, viewNumber:init});
  }
  async stopDonationsPopup(){
		let init = await this.getInit();
		init.showDonation = false;
    	return this.storage.set("sb_init", init);
  }
  //{showChords:true, capoMode:false, position:false, fontSize:16, veille:10}
}
