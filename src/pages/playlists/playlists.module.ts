import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaylistsPage } from './playlists';
import { NewPlaylistPageModule } from '../new-playlist/new-playlist.module';

@NgModule({
  declarations: [
    PlaylistsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaylistsPage),
    NewPlaylistPageModule
  ],
})
export class PlaylistsPageModule {}
