import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Facebook } from '@ionic-native/facebook'; //Added Facebook
import { GooglePlus } from '@ionic-native/google-plus';
//import * as firebase from 'firebase/app'; //Changed firebase/app
import firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from 'angularfire2/firestore';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {
  currentUser: any;
  localUser:any;
  public fireDb: any;
  public usersCollection:any;
  public usersInfoCollection:any;
  constructor(
    public storage: Storage,
    private afAuth: AngularFireAuth, 
    private fb: Facebook, 
    private googlePlus:GooglePlus, 
    private platform: Platform,
    afs: AngularFirestore
  ) {
    afAuth.authState.subscribe(user => {
      if (!user) {
        this.currentUser = null;        
        return;
      }
      this.currentUser = user;
    });
    this.fireDb =  firebase.firestore();
    this.usersCollection = this.fireDb.collection("users");
    this.usersInfoCollection = this.fireDb.collection("usersInfo");
	//console.log('Hello AuthServiceProvider Provider');
  
  }
  async getUserRights(){
	let user = await this.getUserInfo();
	if(user && user.userId) {
		 this.usersCollection.doc(user.userId).get().then(snap=>{
			if(snap.exists && snap.data().email){
				if(!snap.data().userId) {
					snap.data().userId = user.userId;
					this.usersCollection.doc(this.currentUser.uid).set(snap.data());
				}
				let packages = snap.data().packages;
				let songbooks = snap.data().songbooks;
				user.packages = packages;
				user.songbooks = songbooks;
				user.userId=this.currentUser.uid;
				this.storage.set("userDetails",user);
			}
			else {
				 this.usersCollection.doc(this.currentUser.uid).set(user);
				 this.usersInfoCollection.doc(this.currentUser.uid).set({
					 username:user.username,
					 email:user.email
				 });
			 }	
		 })
 
	 }
   }
  
  public updateRights(packages){
    //
    return this.getUserInfo().then(user=>{
      if(user) {
        for(var key in packages) {
          user.packages[key] = packages[key];
        }
        this.storage.set("userDetails",user);
        this.usersCollection.doc(this.currentUser.uid).set({
          packages: user.packages
        }, { merge: true });
      }
      return user;
    })
  }
  public registerToken(token){
    return this.getUserInfo().then(user=>{
      if(user && user!="no_user" && token ) {
        user.fcmToken = token;
        this.storage.set("userDetails",user);
        if(this.currentUser) this.usersCollection.doc(this.currentUser.uid).set({
          fcmToken: token
        }, { merge: true });
      }
      return user;
    })
  }
  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {
        // At this point make a request to your backend to make a real check!
        this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password).then(
          res => {
            let response = res;
            this.currentUser = res;
            observer.next(response); 
            // au login, récupérer les droits de l'utilisateur 
            // pour si ils auraient changé par action dans Firebase
            // ou pour si l'utilisateur s'est déconnecté
            this.usersCollection.doc(res.uid).get().then(
              user=>{
                if(user.exists) {
                  this.localUser = user.data();
                  this.storage.set("userDetails",user.data()).then(()=>{
                    observer.next("user"); observer.complete(); 
                  }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
                }
                else{
                  console.log("user does not exist");
                  observer.error("Ce compte n'existe pas...");
                }
              }
            );
            
          }
        ).catch((error) => { 
          console.log(error);
          observer.error(error); 
        });
        
      });
    }
  }
  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password).then(
          createdUser => {
            //initialisation avec droits publics
            this.currentUser = createdUser;
            let userInfos = {
              username:credentials.name,
              email:credentials.email,
              provider:"email",
              packages:{"public":1, "open":1},
              platforms:this.platform.platforms(),
              createTime:  Date.now(),
              userId:createdUser.uid
            }
            //firebase.database().ref('users/' + createdUser.uid).set(userInfos); 
            this.usersCollection.doc(createdUser.uid).set(userInfos);
            this.usersInfoCollection.doc(createdUser.uid).set({
              username:userInfos.username,
              email:userInfos.email
            });
            this.storage.set("userDetails",userInfos).then(()=> {
              observer.next(createdUser); observer.complete();
            }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));          
          }
        ).catch((error) => {          
          observer.error(error);
        });
      });
    }
  }
  public checkUserName(username){
    return this.usersInfoCollection.where("username", "==", username).get().then(res=>{
      if(res.docs && res.docs.length>0) return false;
      else return true;
    })
    .catch(error=>{return true;})
  }
  public loginWithFacebook() {
    return Observable.create(observer => {
      if (this.platform.is('cordova')) {
        this.fb.login(['email', 'public_profile']).then(res => {
          if(res) {
            const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
            this.afAuth.app.auth().signInWithCredential(facebookCredential).then((response)=>{
              if(response) {
                this.currentUser = response;console.log(response);
                let userId = response.uid;
                observer.next(response); 
                //check in Firebase if user exists
                this.usersCollection.doc(userId).get().then(
                  user=>{
                    if(user.exists) {
                      this.localUser = user.data();
                      this.storage.set("userDetails",user.data()).then(()=>{
                        observer.next("user"); observer.complete(); 
                      }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
                    }
                    else{
                      // first login with Facebook -> get details
                      console.log("user does not exist");
                      observer.next("get user from Facebook"); 
                      this.fb.api("/me?fields=email,name,gender,picture", []).then((user)=> {
                        console.log(user);
                        let userInfos = { 
                          username: user.name, 
                          email: user.email,
                          gender:user.gender?user.gender:"",
                          img:user.picture.data.url,
                          provider:"facebook",
                          //providerData: createdUser.providerData,
                          packages:{"public":1, "open":1},
                          platforms:this.platform.platforms(),
                          createTime:  Date.now(),
                          userId:userId
                        }
                        this.usersCollection.doc(userId).set(userInfos);
                        this.usersInfoCollection.doc(userId).set({
                          username:userInfos.username,
                          email:userInfos.email,
                          img:user.picture.data.url,
                        });
                        userInfos["userId"] = userId;
                        this.storage.set("userDetails", userInfos).then(()=>{
                          observer.next("user"); observer.complete();
                        }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
                      });
                    }
                  }
                )
              }
              else {observer.next("error"); observer.error({message:"Problème d'authentification"});}        
              
              observer.next(res);            
            }).catch(error => {
      //console.log(error);
              observer.error(error);
            });
          }
          else observer.error({message:"Pas de réponse de Facebook"});
        });
      } else {
        return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((response)=>{
          this.currentUser = response;
          console.log(response);
          //get user details
          let userId = response.user.uid;
          observer.next(response); 
          //check in Firebase if user exists
          //firebase.database().ref('users/'+userId).once("value").then(
          this.usersCollection.doc(userId).get().then(
            user=>{
              if(user.exists) {
                console.log("user exists");
                this.localUser = user.data();
                this.storage.set("userDetails",user.data()).then(()=>{
                  observer.next("user"); observer.complete(); 
                }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
              }
              else{
                // first login with Facebook -> get details
                console.log("user does not exist");
                observer.next("get user from Facebook"); 
                let userInfos = { 
                  username: response.user.displayName, 
                  email: response.user.email,
                  provider:"facebook",
                  //providerData: createdUser.providerData,
                  packages:{"public":1, "open":1}
                }
                //firebase.database().ref('users/' + userId).set(userInfos); 
                this.usersCollection.doc(userId).set(userInfos);
                this.storage.set("userDetails", userInfos).then(()=>{
                  observer.next("user"); observer.complete();
                }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
              }
            }); 
        }).catch(error => {
          //console.log(error);
          observer.error(error);
        });
      }
    });
  }
  public loginWithGoogle() {
    return Observable.create(observer => {
      if (this.platform.is('cordova')) {
       this.googlePlus.login({
        'webClientId':'197868433292-qdd3cgk8fp4u660ttd6rtia6mgm5hb9n.apps.googleusercontent.com'
        }).then((userData) => {
          observer.next("Got Google account");console.log(userData);
          const googleCredential = firebase.auth.GoogleAuthProvider.credential(userData.idToken);
          this.afAuth.app.auth().signInWithCredential(googleCredential).then((res)=>{
            observer.next("Log in Firebase");
            this.currentUser = res;
            let userId = res.uid;
            observer.next(res); 
            //check in Firebase if user exists
            //firebase.database().ref('users/'+userId).once("value").then(
            this.usersCollection.doc(userId).get().then(
              user=>{
                if(user.exists) {
                  this.localUser = user.data();
                  this.storage.set("userDetails",user.data()).then(()=>{
                    observer.next("user"); observer.complete(); 
                  }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
                }
                else{
                  // first login with Google -> get details
                  console.log("user does not exist");
                  observer.next("get user from Google");
                  let userInfos = {
                    username:userData.displayName,
                    surname:userData.givenName?userData.givenName:"",
                    name: userData.familyName?userData.familyName:"",
                    email:userData.email,
                    img:userData.imageUrl,
                    provider:"google",
                    packages:{"public":1, "open":1},
                    platforms:this.platform.platforms(),
                    createTime:  Date.now(),
                    userId:userId
                  };
                  this.usersCollection.doc(userId).set(userInfos);
                  this.usersInfoCollection.doc(userId).set({
                    username:userInfos.username,
                    email:userInfos.email,
                    img:userInfos.img
                  });
                  this.storage.set("userDetails", userInfos).then(()=>{
                    observer.next("user"); observer.complete();
                  }).catch(error=>observer.error("Pas assez de mémoire disponible sur l'appareil"));
                }
              }
            )           
          }).catch(error => {
            //console.log(error);
            observer.error(error);
          });
        }).catch(error => {
            //console.log(error);
            observer.error(error);
        });
      } else {
        return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(()=>{
          observer.next();
        }).catch(error => {
          //console.log(error);
          observer.error(error);
        });
      }
    });
  }
  async getUserInfo() {
    if(this.localUser) return Promise.resolve(this.localUser);
    else {
		let hasMigrated = await this.storage.get('hasMigrated');
		if(!hasMigrated) {
			//do migration to sql
			await this.migrateToSql();
			this.storage.set('hasMigrated',true);
		}
		return this.storage.get('userDetails').then((user)=>{
			this.localUser=user; 
			return user;
		});
	}
  }
 public initializePassword(email){
   return this.afAuth.auth.sendPasswordResetEmail(email).then(function() {
    // Email sent.
    return {success:true, error:null};
  }).catch(function(error) {
    // An error happened.
    return {error:error, success:false};
  });
 }
  public logout() {
    return Observable.create(observer => {
      this.storage.remove("songslist").then(()=>{
        observer.next('list');
        this.storage.remove("userDetails").then( ()=>
          firebase.auth().signOut().then(function() {
            observer.next('auth'); observer.complete();
            console.log('signed out')
          }).catch(function(error) {
            observer.error(error);observer.complete();
          })
        );
      });
      
    })
    
  }
  public setUser(user){
    this.localUser = user;
    return this.storage.set('userDetails',user);
  }
  async editUserName(name:string){
	let user = await this.getUserInfo();
	user.username = name;
	this.storage.set('userDetails',user);
	this.usersInfoCollection.doc(user.userId).set({username:name},{merge:true});
	return this.usersCollection.doc(user.userId).set({username:name},{merge:true});
  }
  	async migrateToSql(){
		let oldDb = new Storage({
			driverOrder: ['indexeddb','sqlite',  'websql', 'localstorage']
		});
		return oldDb.forEach((value, key)=>{
			this.storage.set(key, value).then(res=>{
				oldDb.remove(key);
			});
		});
		
	}
}