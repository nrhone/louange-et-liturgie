import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
import { SongPage } from './song';

@NgModule({
  declarations: [
    SongPage
  ],
  imports: [
    IonicPageModule.forChild(SongPage),
    PipesModule,
	ComponentsModule
  ],
})
export class SongPageModule {}
