import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaylistPage } from './playlist';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    PlaylistPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaylistPage),
	PipesModule
  ],
})
export class PlaylistPageModule {}
