import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SpotifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-spotify',
  templateUrl: 'spotify.html',
})
export class SpotifyPage {
  public url:String;
  public type:string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
    this.type = this.navParams.get("type");
    if(this.type=='spotify') this.url = "https://open.spotify.com/embed?uri=spotify:track:"+this.navParams.get("id");
    else this.url = "https://www.youtube.com/embed/"+this.navParams.get("id");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpotifyPage');
  }

}
