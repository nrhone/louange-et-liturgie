import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ModalController} from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { TranslateProvider } from '../../providers/translate/translate'
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { ParamsProvider } from '../../providers/params/params'

/**
 * Generated class for the LocalSongsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-local-songs',
  templateUrl: 'local-songs.html',
})
export class LocalSongsPage {
  songs:any[];params:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private songsProvider:SongsProvider,
    public modalCtrl: ModalController,
	private translateProvider:TranslateProvider,
	private analyticsProvider:AnalyticsProvider,
	private paramsProvider:ParamsProvider
	) 
  {
    this.songsProvider.getLocalSongs().then(res=>{
	  if(res && res.length>0) this.songs=res.slice();
	  else this.songs=[];
	})
	this.paramsProvider.getParams().then(res=>this.params=res)
  }
  ionViewDidEnter(){
  }
  ionViewDidLoad() {
	this.analyticsProvider.pageEvent("Chants locaux");
  }
  goToSong(event, song) {
    this.navCtrl.push("SongPage", {
      id: song.orgKey,
      order:"alpha",
      local:true
    });    
  }
  deleteSong(index){
    this.songsProvider.deleteLocalSong(index).then(res=>this.songs=res);
  }
  createSong(){
    this.navCtrl.push("AddPage");
  }
  import(){
    let modal = this.modalCtrl.create("ImportPage",{},{cssClass:this.params.darkMode?'dark':''});
    modal.present();
    modal.onDidDismiss(
      (songs)=>{
        if(songs) {
			this.songsProvider.getLocalSongs().then(res=>{
				if(res && res.length>0) this.songs=res.slice();
			})
		}
		else{
		}
      }
    );
  }
	translate(id:string, defaultString:string){
		return this.translateProvider.translate(id, defaultString);
	}
  

}