import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewFolderPage } from './new-folder';

@NgModule({
  declarations: [
    NewFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(NewFolderPage),
  ],
})
export class NewFolderPageModule {}
