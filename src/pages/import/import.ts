import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController , AlertController} from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the ImportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-import',
  templateUrl: 'import.html',
})
export class ImportPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private songsProvider:SongsProvider,
    private alertCtrl:AlertController,
    private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
	) {
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Import de chant");
  }

	fileSelected(ev) {
		let files = ev.target.files;
		for (var i = 0; i < files.length; i++) {
			if(files[i] && files[i].size>0) {
				let reader = new FileReader();
				if(files[i])
				reader.onload = (ev) => {
					let res = this.readSong(reader.result);
				  	this.songsProvider.addLocalSong(res).then(res=>{
					//this.navCtrl.setRoot("LocalSongsPage");
						this.analyticsProvider.clickEvent("Import de chant",res.title)
					})
				}
				reader.readAsText(files[i]);
			}
			else {
				let alert=this.alertCtrl.create({
				  title: 'Erreur de lecture du fichier',
				  subTitle: 'Le fichier est soit vide, soit dans un format non lisible.',
				  buttons: ['Fermer']
				});
				alert.present();
			}		
		}
		setTimeout(()=>{
			this.viewCtrl.dismiss();
		},200)
		
   
  }
  readSong(text){
    let id = 'local-' + Math.random().toString(36).substr(2, 16);
    let song:any={
      //title:"Nouveau chant",
      orgKey:id,
      createTime:(new Date()).getTime(),
      lastUpdateTime:(new Date()).getTime(),
      songbooks:{
        "local":{scenario:{"FR":[]}}
      },
      text_parts:{"FR":{}}
	};
	//split by line ends
	let array = text.split("\n");
	if(array.length<=1) array = text.split("\r\n");
	// init vars
	let verseId = "C1"; let verseNb=1, commentNb=1, chorusNb=1, bridgeNb =1; let texteArray=[];let isMeta = true; let continueTillEnd=false;
	array.forEach(line => {
		if(line.charAt(0)=="{"){
			line=line.replace(/[{}\r\n]/g,"");
			//first read meta
			if(isMeta) switch(line.split(":")[0]) {
				case 't':
				case 'title':
					song.title = line.split(":")[1];break;
				case 'st':
				case 'subtitle':
					song.l_copyright = line.split(":")[1];break;
					//song.alternance = line.split(":")[1];break;
				case 'copyright':
					song.l_copyright = line.split(":")[1];break;
				case 'lyricist':
					song.author = line.split(":")[1];break;
				case 'composer':
					song.composer = line.split(":")[1];break;
				case 'capo':
					song.capo = +(line.split(":")[1]);break;
				case 'tonality':
						song.tonality = +(line.split(":")[1]);break;
				default:
					isMeta = false; // end of metadata
			}
			if(!isMeta) {
				switch(line.split(":")[0]) {
					//start of balise --> close last verse and init new
					case 'start_of_chorus':
					case 'soc':
					case 'start_of_verse':
					case 'sov':
					case 'start_of_bridge':
					case 'sob':
						if(texteArray && texteArray.length>0){
							song.songbooks.local.scenario.FR.push(verseId);
							song.text_parts.FR[verseId]={lines:texteArray};
						}
						texteArray=[];continueTillEnd=true;
						break;	
					case 'end_of_chorus':
					case 'eoc': 
						verseId = "R"+chorusNb;
						song.songbooks.local.scenario.FR.push(verseId);
						song.text_parts.FR[verseId]={lines:texteArray};
						texteArray=null; chorusNb++;continueTillEnd=false;
						break;
					case 'end_of_bridge':
					case 'eob': 
						verseId = "B"+bridgeNb;
						song.songbooks.local.scenario.FR.push(verseId);
						song.text_parts.FR[verseId]={lines:texteArray};
						texteArray=null; bridgeNb++; continueTillEnd=false;
						break;
					case 'eov':
					case 'end_of_verse':
						verseId = "C"+verseNb;
						song.songbooks.local.scenario.FR.push(verseId);
						song.text_parts.FR[verseId]={lines:texteArray};
						texteArray=null; verseNb++;continueTillEnd=false;
						break;
					case 'comment':
					case 'c':
						if(texteArray && texteArray.length>0){
							console.log("pushing");
							song.songbooks.local.scenario.FR.push(verseId);
							song.text_parts.FR[verseId]={lines:texteArray};
						}
						texteArray=null;
						var commentId = "K" + commentNb;
						song.songbooks.local.scenario.FR.push(commentId);
						song.text_parts.FR[commentId]={lines:[line.split(":")[1]]};
						commentNb++;continueTillEnd=false;
						break;
				}
			}
		}
		else {
			//texte
			if(continueTillEnd)
				texteArray.push(line); 
			else if(line.replace(/[\r\n]/g,"") == "" ) {
				if(texteArray && texteArray.length>0){//close verse
					verseId = "C"+verseNb;
					song.songbooks.local.scenario.FR.push(verseId);
					song.text_parts.FR[verseId]={lines:texteArray};
					texteArray=null; verseNb++;
				}
				else {
				}
			}
			else {				
				if(texteArray && texteArray.length>0) {
					texteArray.push(line); 
				}
				else {
					//begin new verse
					verseId = "C"+verseNb;
					texteArray=null; 
					texteArray=[line];
				}
			}		
		}
	});
	if(texteArray && texteArray.length>0) {
		//add last verse
		song.songbooks.local.scenario.FR.push(verseId);
		song.text_parts.FR[verseId]={lines:texteArray};
	}
	
    if(!song.title) {
      let alert = this.alertCtrl.create({
        title: 'Titre manquant',
        subTitle: 'Le chant doit au moins avoir un titre valide. Le fichier doit avoir une ligne : {t:Titre}',
        buttons: ['Fermer']
      });
      alert.present();
    }
    else {
		return song;
    }
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}

  


}
