import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RubriquesPage } from './rubriques';
@NgModule({
  declarations: [
	RubriquesPage,
  ],
  imports: [
    IonicPageModule.forChild(RubriquesPage),
  ],
})
export class RubriquesPageModule {}
