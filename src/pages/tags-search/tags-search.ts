import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { deepCopy } from 'ionic-angular/util/util';
import { AnalyticsProvider } from '../../providers/analytics/analytics'
import { TranslateProvider } from '../../providers/translate/translate'

/**
 * Generated class for the TagsSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tags-search',
  templateUrl: 'tags-search.html',
})
export class TagsSearchPage {
  @ViewChild('search') searchInput;
  public db: any;
  public themes:any;
  public allThemes:any;
  public selectedTheme;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    public tagsProvider:SongsProvider,
    private analyticsProvider:AnalyticsProvider,
	private translateProvider:TranslateProvider
	) {
    this.tagsProvider.getTags().then((themes) => {this.themes=themes;this.allThemes=themes;});  
    
  }

  ionViewDidLoad() {
    this.analyticsProvider.pageEvent("Mots clés");
  }
  ionViewWillLeave(){
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  
  getAll(){
    this.themes = deepCopy(this.allThemes);
    // deepCopy(this.allThemes);
  }
  onInput(ev){
    //this.themes = this.allThemes;
    var val = ev.target.value;
    this.getAll();
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.themes = this.themes.map((theme) => {
        
        theme.themes = theme.themes.filter((item) => {
          return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        return theme;
        
      })
    }
  }
  select(theme){
    this.analyticsProvider.clickEvent("Sélection de thème",theme);
    this.viewCtrl.dismiss(theme);
  }
  translate(id:string, defaultString:string){
	return this.translateProvider.translate(id, defaultString);
}
}
