import { Component , ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , reorderArray , ModalController, ToastController, AlertController , LoadingController, ActionSheetController} from 'ionic-angular';
import { FavorsProvider } from '../../providers/favors/favors';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ParamsProvider } from '../../providers/params/params';
import firebase from 'firebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { SongsProvider } from '../../providers/songs/songs';

/**
 * Generated class for the PlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playlist',
  templateUrl: 'playlist.html',
})
export class PlaylistPage {
  @ViewChild('titleInput') input;
  playlist:any;
  playlistId:number;
  editMode:boolean=false;
  isSharedPlaylist: boolean=false; // true si playlist partagée par un autre utilisateur
  canEdit: boolean=false; // true si l'utilisateur a le droit de modifier (propri)
  isOwnPlaylist:boolean = false;
  public db: any;
  carnet:string="APPLI_FR";
  public fireDb: any;
  user:any;
  newSong:any={title:"",notes:""};
  showNote:boolean=false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public favorsProvider:FavorsProvider,
    public modalCtrl: ModalController,
    private auth: AuthServiceProvider,
    public toastCtrl: ToastController,
    private paramsProvider:ParamsProvider,
    afs: AngularFirestore,
    public alertCtrl: AlertController,
    private songsProvider:SongsProvider,
	public loadingCtrl: LoadingController,
	private actionCtrl:ActionSheetController,
  ) 
  {
    this.fireDb = firebase.firestore();
    this.playlistId = navParams.get('id');
    this.isSharedPlaylist = navParams.get('isShared');
    this.canEdit = navParams.get('canEdit');
    this.auth.getUserInfo().then(res=>{
        this.user=res;
	});
    // si playlist partagée par un autre utilisateur, récupérer les détails sur Firebase
    //TODO : loader + si playlist personnelle, chercher en local si pas de connxion.
    if(this.isSharedPlaylist) {
      let loading = this.loadingCtrl.create({
        content: 'Getting playlist...'
      });
      loading.present();
      this.fireDb.collection("playlists").doc(this.playlistId).onSnapshot(
        snap => {
          if(snap.exists) {

            loading.dismiss();
            this.playlist = snap.data();
            this.auth.getUserInfo().then(res=>{
              this.user=res;
              if(this.playlist.createUserId==res.userId) this.isOwnPlaylist=true;
            });
          }
          else {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: 'La playlist n\'a pas été trouvée',
              duration: 2000,
              position: 'middle'
            });
            toast.present();
          }
          
        },
        err=>{
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: 'Erreur de connexion...',
            duration: 2000,
            position: 'middle'
          });
          toast.present();
        }
      )
    }
    // sinon, playlist local -> récupérer les détails en local storage
    else this.favorsProvider.getPlaylist(this.playlistId).then(result =>{
      this.isOwnPlaylist=true;
	  this.playlist = result;
	  
    })
    this.paramsProvider.getParams().then(params=>{
      if(params) {this.carnet=params.carnet;}
    })
  }
  ionViewWillEnter() {
    if(!this.isSharedPlaylist){
      this.favorsProvider.getPlaylist(this.playlistId).then(result =>{
        this.playlist = result;
      })

    }
  }
  ionViewDidLoad() {
  }
  deleteSong(id){
    this.playlist.songs.splice(id,1);
    if(this.playlist.isShared && this.playlist.sharedKey) {
      this.fireDb.collection("playlists").doc(this.playlist.sharedKey).set({songs:this.playlist.songs},{merge:true})
    }
    else this.favorsProvider.updatePlaylist(this.playlistId, this.playlist);
  }
  delete(){
    let confirm = this.alertCtrl.create({
      title: 'Supprimer la playlist ?',
      message: 'Cette action est irréversible.',
      buttons: [
        {
          text: 'Annuler',
          handler: () => {
          }
        },
        {
          text: 'Supprimer',
          handler: () => {
            this.favorsProvider.deletePlaylistById(this.playlistId).then(()=>{
              this.navCtrl.pop();
            })
          }
        }
      ]
    });
    confirm.present();
    
  }
  goToSong(index) {
	  if(this.playlist.songs[index].orgKey) {
		this.navCtrl.push("PlaylistSongsPage", {
			id: index,
			playlist:this.playlist
		  });
	  }
	  else{
		  //for songs that are not in the songbook
	  }
      
  }
  reorderItems(ev){
    this.playlist.songs = reorderArray(this.playlist.songs, ev);
    if(this.playlist.isShared && this.playlist.sharedKey) {
      this.fireDb.collection("playlists").doc(this.playlist.sharedKey).set({songs:this.playlist.songs},{merge:true})
    }
    else this.favorsProvider.updatePlaylist(this.playlistId, this.playlist);
  }
  editTitle(input){
    setTimeout(()=>{
      this.input.setFocus();
    },200)
    this.editMode=true;
  }
  save(){
    this.favorsProvider.updatePlaylist(this.playlistId, this.playlist);
    this.editMode=false;    
  }
  selectUsers(){
    if(this.playlist.songs && this.playlist.songs.length<=20) {
      let modal = this.modalCtrl.create("SelectUsersPage",{users:this.playlist.users?this.playlist.users.slice():[]});
      modal.present();
      modal.onDidDismiss(
        (data)=>{
          if(data) { // des utilisateurs ont été sélectionnés
            if(!this.playlist.isShared) {		
              // création de la playlist partagée
              this.playlist.isShared=true;
              this.auth.getUserInfo().then(res=>{
                if(res && res != null) {this.playlist.createUser = res.username}
                let playlistKey = this.fireDb.collection("playlists").doc().id;
                this.playlist.sharedKey = playlistKey;
                this.playlist.sharedTime=(new Date()).getTime();
				this.playlist.createUserId = res.userId?res.userId:"";
                this.fireDb.collection("playlists").doc(playlistKey).set(this.playlist);
                
                //add song details
                let songs=[];
                this.playlist.songs.forEach(song => {
                  if(!song.text_parts) this.songsProvider.getSongById(song.orgKey).then(res=>{
                    songs.push(res);
                    if(songs.length==this.playlist.songs.length){
                      this.playlist.songs = songs;
                      //create playlist details in firebase
                      this.fireDb.collection("playlists").doc(playlistKey).set(this.playlist);
                    }
                  })
                  else {
                    songs.push(song);
                    if(songs.length==this.playlist.songs.length){
                      this.playlist.songs = songs;
                      //create playlist details in firebase
                      this.fireDb.collection("playlists").doc(playlistKey).set(this.playlist);
                    }
                  }
                });                
                //add playlist summary in user's playlists
                this.playlist.users = data;
                this.playlist.users.forEach(user => {
                  var userSummary =  {
                    "title":this.playlist.title,
                    "createdTime":this.playlist.createdTime,
                    "createUser":this.playlist.createUser ? this.playlist.createUser : "",
                    "createUserId":res.userId ? res.userId : "",
                    "sharedTime":this.playlist.sharedTime,
                    "id":this.playlist.sharedKey,
                    "canEdit":false
                  }
                  if(user.canEdit) userSummary.canEdit=true;
                  else userSummary.canEdit=false;
                  this.fireDb.collection("users").doc(user.userId).collection("playlists").doc(playlistKey).set(userSummary);
                })
                var userSummary =  {
                  "title":this.playlist.title,
                  "createdTime":this.playlist.createdTime,
                  "createUser":this.playlist.createUser ? this.playlist.createUser : "",
                  "createUserId":res.userId ? res.userId : "",
                  "sharedTime":this.playlist.sharedTime,
                  "id":this.playlist.sharedKey,
                  "canEdit":true
                };
                this.fireDb.collection("users").doc(res.userId).collection("playlists").doc(playlistKey).set(userSummary);
              });
              this.favorsProvider.updatePlaylist(this.playlistId, this.playlist).then(()=>{
                let toast = this.toastCtrl.create({
                  message: 'Playlist partagée',
                  duration: 3000
                });
                toast.present();
              });

            }
            else {
              //mise à jour de la playlist partagée
              if(this.playlist.users) this.playlist.users.forEach(user =>{
                if(data.indexOf(user)<0){
                  //l'utilisateur n'est pas dans la nouvelle liste -> supprimer la playlist de sa liste de playlists
                  this.fireDb.collection("users").doc(user.userId).collection("playlists").doc(this.playlist.sharedKey).delete();          
                }
              })
              data.forEach(newUser =>{
                var summary = {
                  "title":this.playlist.title,
                  "createdTime":this.playlist.createdTime,
                  "createUser":this.playlist.createUser ? this.playlist.createUser : "",
                  "sharedTime":this.playlist.sharedTime,
                  "id":this.playlist.sharedKey,
                  "canEdit":false
                };
                if(newUser.canEdit) summary.canEdit=true; 
                // le nouvel utilisateur n'est pas présent dans la liste actuelle -> lui ajouter la playlist
                this.fireDb.collection("users").doc(newUser.userId).collection("playlists").doc(this.playlist.sharedKey).set(summary);
              });
              this.playlist.users = data;
              this.fireDb.collection("playlists").doc(this.playlistId).set({users:data},{ merge: true });
              this.favorsProvider.updatePlaylist(this.playlistId, this.playlist).then(()=>{
                let toast = this.toastCtrl.create({
                  message: 'Playlist mise à jour',
                  duration: 3000
                });
                toast.present();
              });
            }

          }
          else{}
        }
      );
    }
    else {
      //limit shared playlist to 20 songs
      let alert = this.alertCtrl.create({
        title: 'Erreur',
        subTitle: 'Les playlists partagées sont limitées à 20 chants.',
        buttons: ['OK']
      });
      alert.present();
      }
  }
  addSong(){
	const modal = this.modalCtrl.create("AddSongPage",{ listId: this.playlistId, isShared:this.isSharedPlaylist },{cssClass:"pop-left"});
	modal.present();
	modal.onDidDismiss(
		(res)=>{
		  	if(res && !this.isSharedPlaylist) {
				//update songs
				this.favorsProvider.getPlaylist(this.playlistId).then(result =>{
					this.playlist = result;
				})		
			}
		}
	  );
  }
	async createGlobalPlaylist(){
		let confirm = this.alertCtrl.create({
			title: 'Partager la playlist ?',
			message: 'Si cette playlist est achevée, il est possible de la partager à tous les utilisateurs. Il faudra leur envoyer un message pour leur permettre de la télécharger.',
			buttons: [
				{
				text: 'Annuler',
				handler: () => {
				}
				},
				{
				text: 'Partager',
				handler: () => {
					if(this.user && this.user != null) {
						this.playlist.createUser = this.user.username;
						this.playlist.createUserId = this.user.userId?this.user.userId:"";
					}
					let playlistKey = this.fireDb.collection("globalPlaylists").doc().id;
					this.playlist.sharedKey = playlistKey;
					this.playlist.sharedTime=(new Date()).getTime();
					this.fireDb.collection("globalPlaylists").doc(playlistKey).set(this.playlist).then(res=>{
						let alert = this.alertCtrl.create({
							title: 'Playlist créée',
							subTitle: "L'identifiant de la liste est : " + playlistKey,
							buttons: ['OK']
						});
						alert.present();
					});
				}
				}
			]
		});
		confirm.present();
		
	}
	async shareOptions(){
		let actionSheet = this.actionCtrl.create({
			title: 'Partager la playlist',
			buttons: [
				// {
				// 	text: 'À une liste d\'utilisateurs',
				// 	icon:'people',
				// 	handler: () => {
				// 		this.selectUsers();
				// 	}
				// },
				{
					text: 'Générer un code de partage',
					icon:'code-working',
					handler: () => {
						this.generateCode();
					}
				},
				{
					text: 'Annuler',
					role: 'cancel',
					handler: () => {
					}
				  }
			]
		});
		actionSheet.present();
	}
	async generateCode(){
		//generer un code hexa à envoyer
		var letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var code = '';
		for (var i = 0; i < 4; i++) {
			code += letters[Math.floor(Math.random() * 36)];
		}
		let user = await this.auth.getUserInfo();
		let id = this.playlistId;
		if(user && user != null) {this.playlist.createUser = user.username}
		if(!this.playlist.isShared) {
			this.playlist.isShared=true; this.isSharedPlaylist=true;
			let playlistKey = this.fireDb.collection("playlists").doc().id;
			this.playlist.sharedKey = playlistKey;	
			this.playlistId = playlistKey;
		}
		this.playlist.sharedTime=(new Date()).getTime();
		this.playlist.createUserId = user.userId?user.userId:"";
		this.playlist.shareCode=code;
		//add song details
		let songs=[];
		this.playlist.songs.forEach(song => {
			if(!song.text_parts && song.orgKey !='external_song') this.songsProvider.getSongById(song.orgKey).then(res=>{
				songs.push(res);
				if(song.copyright && song.copyright.international && (typeof(song.copyright.international)=='object' && song.copyright.international[0]==null)) {
					delete(song.copyright.international);
				}
				if(songs.length==this.playlist.songs.length){
					this.playlist.songs = songs;
					//create playlist details in firebase
					this.fireDb.collection("playlists").doc(this.playlist.sharedKey).set(this.playlist);
				}
			})
			else {
				songs.push(song);
				if(songs.length==this.playlist.songs.length){
					this.playlist.songs = songs;
					//create playlist details in firebase
					this.fireDb.collection("playlists").doc(this.playlist.sharedKey).set(this.playlist);
				}
			}
		});      

		var userSummary =  {
			"title":this.playlist.title,
			"createdTime":this.playlist.createdTime,
			"createUser":this.playlist.createUser ? this.playlist.createUser : "",
			"createUserId":user.userId ? user.userId : "",
			"sharedTime":this.playlist.sharedTime,
			"id":this.playlist.sharedKey,
			"canEdit":true
		};
		this.fireDb.collection("users").doc(user.userId).collection("playlists").doc(this.playlist.sharedKey).set(userSummary);
		this.favorsProvider.updatePlaylist(id, this.playlist).then(()=>{
			let alert = this.alertCtrl.create({
				title: 'Code de partage',
				subTitle: 'Voici le code de téléchargement pour cette playlist : <br/><p ion-text text-center color="primary">'+code+'</p>',
				buttons: [{
					text: 'Fermer',
					role: 'close',
						handler: () => {
						}
					}
				]
			});
			alert.present();
		});
		
	}

	addExternalSong(){
		if(!this.showNote) this.showNote=true;
		// to add to playlist a song that is not in the app
		else {
			let song = {
				title:this.newSong.title,
				orgKey:'external_song',
				songbooks:{},
				notes:this.newSong.notes
			}
			this.favorsProvider.addSongToPlaylist(song, this.playlistId, this.isSharedPlaylist);
			this.playlist.songs.push(song);
			this.showNote = false;
			this.newSong={};
		}
	}
	isExternal(id){
		return (this.playlist.songs[id].orgKey&&this.playlist.songs[id].orgKey=='external_song')?true:false;
	}
  


}
